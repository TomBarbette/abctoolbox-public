/*
 * TIndependentMeasuresDataSet.h
 *
 *  Created on: May 6, 2009
 *      Author: wegmannd
 */
/*
#ifndef TINDEPENDENTMEASURESDATASET_H_
#define TINDEPENDENTMEASURESDATASET_H_

#include "TObsData.h"
#include "TSimData.h"
#include <stdlib.h>
#include <time.h>
//---------------------------------------------------------------------------
class TIMDataSet {
   public:
	  TObsData* myObsData;
	  TSimData* mySimData;
	  TObsData* myObsDataForDist;
	  TSimData* mySimDataForDistForDist;
	  int myObsNum;

	  int numSimsUsed;
	  ColumnVector obsValues;
	  Matrix paramMatrix,statMatrix;
	  Matrix C;
	  ColumnVector c_zero;
	  Matrix CHat;
	  Matrix RHat;
	  Matrix SigmaS;
	  Matrix SigmaSInv;
	  Matrix SigmaTheta;
	  Matrix SigmaThetaInv;
	  Matrix T_jInv;
	  double* c_j;
	  ColumnVector* t_j;
	  double meanExpLikelihood;



	  TIMDataSet(TObsData* ObsData, TSimData* SimData, int obsNum);
	  ~TIMDataSet(){}

	  void fillRetainedMatrices(TObsData* ObsData, TSimData* SimData, bool thresholdDefined, bool retainedDefined, float threshold, int numToRetain, bool writeRetainedSimulations, my_string outputPrefix);
	  void performLinearRegression();
	  void prepareLikelihoodCalculation(Matrix& SigmaThetaInv);
	  double getLikelihood(ColumnVector& theat);

};



#endif /* TINDEPENDANTMEASURESDATASET_H_ */
