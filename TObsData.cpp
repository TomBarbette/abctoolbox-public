//---------------------------------------------------------------------------
#include "TObsData.h"
//---------------------------------------------------------------------------
TObsDataSet::TObsDataSet(int gotNumStats){
   internalCounter=0;
   numStats=gotNumStats;
   myObsValues=new double[numStats];
   myObsValuesUsed = NULL;
   myStandardizedObsValues=NULL;
   standardized=false;
   myStandardizedObsValuesUsed = NULL;
   useSubset = false;
   numUsedStats = 0;
   numObsDataSets=-1;
};
void TObsDataSet::subsetStats(vector<int> & usedStats){
	if(useSubset){
		delete[] myObsValuesUsed;
		if(standardized) delete[] myStandardizedObsValuesUsed;
	}
	myObsValuesUsed = new double[usedStats.size()];
	for(vector<int>::size_type i=0; i<usedStats.size(); ++i)
		myObsValuesUsed[i] = myObsValues[usedStats[i]];

	if(standardized){
		myStandardizedObsValuesUsed = new double[usedStats.size()];
		for(vector<int>::size_type i=0; i<usedStats.size(); ++i)
			myStandardizedObsValuesUsed[i] = myStandardizedObsValues[usedStats[i]];
	}
	useSubset = true;
	numUsedStats = usedStats.size();
}
void TObsDataSet::add(double value){
   if(internalCounter==numStats) throw "Too many values added to an observed data set!";
   myObsValues[internalCounter]=value;
   ++internalCounter;
};
void TObsDataSet::standardize(double* means, double* sds, vector<int> & usedStats){
   myStandardizedObsValues=new double[numStats];
   for(int i=0; i<numStats;++i){
	  myStandardizedObsValues[i]=(myObsValues[i]-means[i])/sds[i];
   }
   standardized=true;

   if(useSubset){
	   delete[] myObsValuesUsed;
	   useSubset = false;
	   subsetStats(usedStats);
   }
}
double* TObsDataSet::getPointerToObsData(){
	if(useSubset){
		if(standardized) return myStandardizedObsValuesUsed;
		return myObsValuesUsed;
	} else {
		if(standardized) return myStandardizedObsValues;
		return myObsValues;
	}
}
double* TObsDataSet::getPointerToObsDataAllStats(){
	if(standardized) return myStandardizedObsValues;
	return myObsValues;
}
void TObsDataSet::getStandardizedObsValuesIntoColumnVector(ColumnVector& colVector){
   if(!standardized) throw "Observed values have not been standardized!";
   if(useSubset){
	   colVector.resize(numUsedStats);
	   for(int i=0; i<numUsedStats; ++i)
	   colVector(i+1)= myStandardizedObsValuesUsed[i];
   } else {
	   colVector.resize(numStats);
	   for(int i=0; i<numStats; ++i)
	   colVector(i+1)= myStandardizedObsValues[i];
   }
}
void TObsDataSet::getObsValuesIntoColumnVector(ColumnVector& colVector){
	if(useSubset){
	   colVector.resize(numUsedStats);
	   for(int i=0; i<numUsedStats; ++i)
	   colVector(i+1)= myObsValuesUsed[i];
	} else {
	   colVector.resize(numStats);
	   for(int i=0; i<numStats; ++i)
	   colVector(i+1)= myObsValues[i];
	}
}

//---------------------------------------------------------------------------
//TObsData
//---------------------------------------------------------------------------
TObsData::TObsData(TLog* logFile){
   obsFileName = "";
   hasBeenStandardized = false;
   logfile = logFile;
   numObsDataSets = 0;
   numStats = 0;
   numUsedStats = 0;
}
TObsData::TObsData(std::string ObsFileName, TLog* logFile){
   //this constructor reads the files
   obsFileName=ObsFileName;
   hasBeenStandardized=false;
   logfile=logFile;
   numUsedStats = 0;
   readObsFile();
}
//---------------------------------------------------------------------------
void TObsData::update(std::string ObsFileName){
	//clear old data
	clear();
	//read new data
	obsFileName=ObsFileName;
	readObsFile();
	numUsedStats = 0;
}
//---------------------------------------------------------------------------
void TObsData::clear(){
	for(vector<TObsDataSet*>::iterator it=obsDataSets.begin(); it!=obsDataSets.end(); ++it){
		delete *it;
	}
    obsDataSets.clear();
    obsNameVector.clear();
    usedStats.clear();
}
//---------------------------------------------------------------------------
void TObsData::readObsFile(){
   logfile->listFlush("Reading observed data file '" + obsFileName + "' ...");
   ifstream is (obsFileName.c_str()); // opening the file for reading
   if(!is) throw "File with observed statistics '" + obsFileName + "' could not be opened!";

   //read names
   fillVectorFromLineWhiteSpace(is, obsNameVector);
   if(obsNameVector.empty()) throw "No statistics in observed file!";
   std::string s=getFirstNonUniqueString(obsNameVector);
   if(!s.empty()) throw "Statistic '"+s+"' is listed multiple times in header of file '"+ obsFileName +"'!";

   //read values into an array
   endObsNameVector = obsNameVector.end();
   numStats = obsNameVector.size();
   //every line is a new dataset
   int dataSetCounter=0;
   std::string line;
   vector<double> values;
   int lineNum = 1;
   while(is.good() && !is.eof()){
	   ++lineNum;
	   fillVectorFromLineWhiteSpace(is, values);
	   if(values.size()>0){
		   if(values.size() > obsNameVector.size()) throw "More values than names in the .obs-file '" + obsFileName + "' on line " + toString(lineNum) + "!";
		   if(values.size() < obsNameVector.size()) throw "More names than values in the .obs-file '" + obsFileName + "' on line " + toString(lineNum) + "!";
		   obsDataSets.push_back(new TObsDataSet(numStats));
		   for(vector<double>::iterator it=values.begin(); it!=values.end(); ++it)
			   obsDataSets.at(dataSetCounter)->add(*it);
		 ++dataSetCounter;
	  }
   }

   curObsNameVector=obsNameVector.begin();
   numObsDataSets=obsDataSets.size();
   useAllStats();

   is.close();
   logfile->write(" done!");
   logfile->conclude(numObsDataSets, " data sets with ", numStats, " statistics each.");
}
//---------------------------------------------------------------------------
int TObsData::getStatColNumberFromName(std::string name){
   int i=0;
   curObsNameVector=obsNameVector.begin();
   for(;curObsNameVector!=endObsNameVector;++curObsNameVector, ++i){
	  if(*curObsNameVector==name) return i;
   }
   return -1;
}
std::string TObsData::getStatNameFromColNumber(int num){
   return obsNameVector[num];
}
//---------------------------------------------------------------------------
void TObsData::fillNamesVector(vector<std::string> & namesVec){
	for(curObsNameVector=obsNameVector.begin();curObsNameVector!=endObsNameVector;++curObsNameVector){
		namesVec.push_back(*curObsNameVector);
	}
}
//---------------------------------------------------------------------------
void TObsData::fillUsedNamesVector(vector<std::string> & namesVec){
	for(vector<int>::iterator it=usedStats.begin(); it!=usedStats.end(); ++it)
		namesVec.push_back(obsNameVector[*it]);
}
//---------------------------------------------------------------------------
void TObsData::fillObsDataMatrix(int dataSet){
   if(dataSet<numObsDataSets){
	   obsValuesMatrix=Matrix(usedStats.size(),1); //prepare for later..
	   int i=1;
	   for(vector<int>::iterator it=usedStats.begin(); it!=usedStats.end(); ++it, ++i)
		   obsValuesMatrix(i,1)=obsDataSets[dataSet]->myStandardizedObsValues[*it];
   } else {
	  throw "There are less data sets in the obsfile than requested!";
   }
}
//---------------------------------------------------------------------------
void TObsData::standardizeObservedValues(double* means, double*sds){
	for(int i=0; i<numObsDataSets;++i){
		obsDataSets[i]->standardize(means, sds, usedStats);
	  }
	  hasBeenStandardized=true;
}
//---------------------------------------------------------------------------
double* TObsData::getObservedValues(int num){
	return obsDataSets[num]->getPointerToObsData();
}
double* TObsData::getObservedValuesAllStats(int num){
	return obsDataSets[num]->getPointerToObsDataAllStats();
}
//---------------------------------------------------------------------------
void TObsData::getObsValuesIntoColumnVector(int & num, ColumnVector& colVector){
   if(hasBeenStandardized) obsDataSets[num]->getStandardizedObsValuesIntoColumnVector(colVector);
   else obsDataSets[num]->getObsValuesIntoColumnVector(colVector);
}
//---------------------------------------------------------------------------
bool TObsData::statIsUsed(int & num){
	for(vector<int>::iterator it=usedStats.begin(); it!=usedStats.end(); ++it){
		if(*it == num) return true;
	}
	return false;
}
//---------------------------------------------------------------------------
void TObsData::useOnlyTheseStats(vector<int> & theseStats){
	if(theseStats.empty()) throw "No statistics left!";
	usedStats = theseStats;
	numUsedStats = usedStats.size();
	subsetStatsInObjects();
}
void TObsData::excludeTheseStats(vector<int> & excludeTheseStats){
	vector<int> old=usedStats;
	usedStats.clear();
	vector<int>::iterator itEx;
	for(vector<int>::iterator it=old.begin(); it!=old.end(); ++it){
		itEx=excludeTheseStats.begin();
		for(; itEx!=excludeTheseStats.end(); ++itEx){
			if(*it == *itEx) break;
		}
		if(itEx==excludeTheseStats.end())
			usedStats.push_back(*it);
	}

	if(usedStats.empty()) throw "No statistics left!";
	numUsedStats=usedStats.size();
	subsetStatsInObjects();
}

void TObsData::useAllStats(){
	for(int i=0; i<numStats; ++i) usedStats.push_back(i);
	numUsedStats = numStats;
	subsetStatsInObjects();
}

void TObsData::subsetStatsInObjects(){
	for(vector<TObsDataSet*>::iterator it=obsDataSets.begin(); it!=obsDataSets.end(); ++it)
		(*it)->subsetStats(usedStats);
}


