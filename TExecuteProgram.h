//---------------------------------------------------------------------------

#ifndef TExecuteProgramH
#define TExecuteProgramH

#include <fstream>
#include <iostream>
#include <vector>
#include "newmat.h"
#include "newmatap.h"
#include "newmatio.h"
#include "TRandomGenerator.h"
#include "stringFunctions.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

//---------------------------------------------------------------------------
//TExecuteProgram
//---------------------------------------------------------------------------
class TExecuteProgram{
private:
	  char** myParameter;
	  int myPassedParamIdentifier;
	  bool paramsInitialized;

public:
	 std::string myExe;
	int outputFileDescriptor;
	bool outputfileOpen;
	bool redirectOutput;
	ofstream* redirect;
	std::string redirectName;
	int redirectDescriptor;
	bool writeOutput;

	TExecuteProgram(){
		paramsInitialized=false;
		myPassedParamIdentifier=-1;
		myParameter=NULL;
		outputFileDescriptor=-1;
		outputfileOpen=false;
		redirectOutput=false;
		redirect=NULL;
		redirectDescriptor=-1;
		writeOutput = true;
	};
	TExecuteProgram(std::string exe, std::string* parameter, int numParameter, int passedParamIdentifier=-1);
	TExecuteProgram(std::string exe);
	virtual ~TExecuteProgram(){
		if(paramsInitialized) delete[] myParameter;
	};
	void setOutputFile(int OutputFileDescriptor);
	void setRedirectionFile(std::string & filename);
	virtual int execute();
	virtual int execute(std::string passedParam);
	virtual int executeAndWrite(std::string* passedParams, int numPassedParams){
		//base calss does not write itself -> just calle xecute function;
		return execute(passedParams, numPassedParams);
	};
	virtual int execute(std::string* passedParams, int numPassedParams);
	virtual void fillHeaderNames(std::vector<std::string> & nameContainer){throw "void fillHeaderNames(std::vector<std::string> & nameContainer) not implemented for base class TExecuteProgram!";};
	virtual void fillValues(std::vector<double> & valueContainer){throw "void fillValues(std::vector<double> & valueContainer) not implemented for base class TExecuteProgram!";};
	virtual void fillValues(double* valueContainer){throw "void fillValues(double* valueContainer) not implemented for base class TExecuteProgram!";};

	std::string getMyExe(){ return myExe;}
};

//TODO: implement TGLM without need of newmat
class TGLM:public TExecuteProgram{
private:
	Matrix C;
	ColumnVector c0;
	SymmetricMatrix Sigma;
	Matrix A;
	ColumnVector e;
	ColumnVector P;
	int numStats;
	int numParams;
	TRandomGenerator* randomGenerator;
	std::string outputname;

public:
	ColumnVector s;

	~TGLM(){};
	TGLM(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator);
	void simulate(std::string* passedParams, int & numPassedParams);
	int execute(std::string* passedParams, int numPassedParams);
	void writeStatesToFile();
	void fillHeaderNames(std::vector<std::string> & nameContainer);
	void fillValues(std::vector<double> & valueContainer);
	void fillValues(double* valueContainer);
};

class TNormdist:public TExecuteProgram{
private:
	std::string outputFilename;
	TRandomGenerator* randomGenerator;
	double sampleMean, sampleSD;
	std::string outputname;

public:


	~TNormdist(){};
	TNormdist(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator);
	void fillHeaderNames(std::vector<std::string> & nameContainer);
	void fillValues(std::vector<double> & valueContainer);
	void fillValues(double* valueContainer);
	void simulate(std::string* passedParams, int & numPassedParams);
	int execute(std::string* passedParams, int numPassedParams);
	void writeStatsToFile();
};



#endif
