//---------------------------------------------------------------------------
#include "TOutput.h"
//---------------------------------------------------------------------------
TOutput::TOutput(int gotSampling, std::string outputFilename, bool WriteStats, bool WriteDistance, bool OverWrite){
	sampling=gotSampling;
	writeDistance=WriteDistance;
	writeStats = WriteStats;
	overWrite = OverWrite;
	filename=outputFilename+"_sampling"+toString(sampling)+".txt";
	CheckIfFileExists();
	myFile.open(filename.c_str());
}
TOutputOneObs::TOutputOneObs(int gotSampling, int gotObsFileNumber, std::string outputFilename, bool WriteStats, bool WriteDistance, bool OverWrite){
	sampling=gotSampling;
	obsFileNumber=gotObsFileNumber;
	writeDistance=WriteDistance;
	writeStats = WriteStats;
	overWrite = OverWrite;
	filename=outputFilename+"_Obs"+ toString(obsFileNumber) +"_sampling"+toString(sampling)+".txt";
	CheckIfFileExists();
	myFile.open(filename.c_str());
}

void TOutput::CheckIfFileExists(){
	if(!overWrite){
		ifstream f(filename.c_str());
		if (f.good()){
	        f.close();
	        throw "Output file '" + filename + "' already exists! Use 'overWrite' to force replacement.";
		} else f.close();
	}
}

//------------------------------------------------------------------------------
void TOutput::writeHeader(TInputFileVector* InputFiles, TDataVector* Data){
   myFile << "Sim";
   InputFiles->priors->writeHeader(myFile);
   if(writeStats) Data->writeHeader(myFile);
   if(writeDistance) myFile << "\t" << "Distance";
   myFile << std::endl;
}
void TOutput::writeHeader(TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject){
   myFile << "Sim";
   InputFiles->priors->writeHeader(myFile);
   if(writeStats){
	   Data->writeHeader(myFile);
	   pcaObject->writeHeader(myFile);
   }
   if(writeDistance) myFile << "\t" << "Distance";
   myFile << std::endl;
}
void TOutputOneObs::writeHeader(TInputFileVector* InputFiles, TDataVector* Data){
   myFile << "Sim";
   InputFiles->priors->writeHeader(myFile);
   if(writeStats) Data->writeHeaderOnlyOneDataObject(obsFileNumber, myFile);
   if(writeDistance) myFile << "\t" << "Distance";
   myFile << std::endl;
}
//------------------------------------------------------------------------------
void TOutput::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance){
   if(run%sampling==0){
	  myFile << run;
	  InputFiles->priors->writeParameters(myFile);
	  if(writeStats) Data->writeData(myFile);
	  if(writeDistance) myFile << "\t" << distance;
	  myFile << std::endl;
   }
}
void TOutput::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject, double distance){
   if(run%sampling==0){
	  myFile << run;
	  InputFiles->priors->writeParameters(myFile);
	  if(writeStats){
		  Data->writeData(myFile);
		  pcaObject->writeSimPCA(myFile);
	  }
	  if(writeDistance) myFile << "\t" << distance;
	  myFile << std::endl;
   }
}
void TOutputOneObs::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance){
   if(run%sampling==0){
	  myFile << run;
	  InputFiles->priors->writeParameters(myFile);
	  if(writeStats) Data->writeDataOnlyOneDataObject(obsFileNumber, myFile);
	  if(writeDistance) myFile << "\t" << distance;
	  myFile << std::endl;
   }
}
//------------------------------------------------------------------------------
TOutputVector::TOutputVector(std::string mcmcsampling, std::string outName, TDataVector* Data, TInputFileVector* InputFile, bool WriteStats, bool gotWriteDistance, bool gotSeparateOutputFiles, bool forceOverWrite){
	separateOutputFiles=gotSeparateOutputFiles;
	data=Data;
	inputFiles=InputFile;
	inputFileAndDataPointerSet=true;

	std::vector<int> samplingVec;
	fillVectorFromString(mcmcsampling, samplingVec, ';');
	if(separateOutputFiles){
		for(std::vector<int>::iterator it=samplingVec.begin(); it!=samplingVec.end(); ++it){
			for(int i=0; i<Data->numDataObjects;++i){
				TOutput* newObject = new TOutputOneObs(*it, i, outName, WriteStats, gotWriteDistance, forceOverWrite);
				//this is done because the Vector wants a TOutput object and produces an error if the TOutputOneObs is directly created on the next line
				vecOutputFiles.push_back(newObject);
				//vecOutputFiles.push_back(new TOutput(mcmcsampling.extract_sub_str(*";").toInt(), outName, gotWriteDistance, Data, InputFile));
		  }
	   }
	} else {
		for(std::vector<int>::iterator it=samplingVec.begin(); it!=samplingVec.end(); ++it){
			vecOutputFiles.push_back(new TOutput(*it, outName, WriteStats, gotWriteDistance, forceOverWrite));
		}
    }
	endOutput=vecOutputFiles.end();
}
//------------------------------------------------------------------------------
void TOutputVector::writeHeader(){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
		vecOutputFiles[i]->writeHeader(inputFiles, data);
	}
}
void TOutputVector::writeHeader(TLinearComb* pcaObject){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
		vecOutputFiles[i]->writeHeader(inputFiles, data, pcaObject);
	}
}
//------------------------------------------------------------------------------
void TOutputVector::writeHeader(TInputFileVector* InputFiles, TDataVector* Data){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
		vecOutputFiles[i]->writeHeader(InputFiles, Data);
	}
}
void TOutputVector::writeHeader(TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
		vecOutputFiles[i]->writeHeader(InputFiles, Data, pcaObject);
	}
}
//------------------------------------------------------------------------------
void TOutputVector::writeSimulations(int run){
   writeSimulations(run, 0.0);
}
void TOutputVector::writeSimulations(int run, double distance){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
	   vecOutputFiles[i]->writeSimulations(run, inputFiles, data, distance);
	}
}
void TOutputVector::writeSimulations(int run, TLinearComb* pcaObject){
   writeSimulations(run, pcaObject, 0.0);
}
void TOutputVector::writeSimulations(int run, TLinearComb* pcaObject, double distance){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
	   vecOutputFiles[i]->writeSimulations(run, inputFiles, data, pcaObject, distance);
	}
}
//------------------------------------------------------------------------------
void TOutputVector::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data){
   writeSimulations(run, InputFiles, Data, 0.0);
}
void TOutputVector::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
	   vecOutputFiles[i]->writeSimulations(run, InputFiles, Data, distance);
	}
}
void TOutputVector::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject){
   writeSimulations(run, InputFiles, Data,  pcaObject, 0.0);
}
void TOutputVector::writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject, double distance){
	for(unsigned int i=0; i< vecOutputFiles.size();++i){
	   vecOutputFiles[i]->writeSimulations(run, InputFiles, Data,  pcaObject, distance);
	}
}
