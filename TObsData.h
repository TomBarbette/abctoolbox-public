//---------------------------------------------------------------------------

#ifndef TObsDataH
#define TObsDataH
//---------------------------------------------------------------------------
#include <sstream>
#include <fstream>
#include <iostream>
#include "newmat.h"
#include "newmatio.h"
#include "TParameters.h"
#include <vector>
#include "TLog.h"
#include "TObsData.h"
//---------------------------------------------------------------------------
class TObsDataSet{
	public:
	   double* myObsValues;
	   double* myStandardizedObsValues;
	   int numUsedStats;
	   double* myObsValuesUsed;
	   double* myStandardizedObsValuesUsed;
	   int internalCounter;
	   int numObsDataSets;
	   int numStats;
	   bool standardized;
	   bool useSubset;

	   TObsDataSet(int gotNumStats);
	   TObsDataSet(){
		   myObsValues=NULL;
		   myObsValuesUsed=NULL;
		   numUsedStats = 0;
		   myStandardizedObsValues = NULL;
		   myStandardizedObsValuesUsed = NULL;
		   internalCounter=0;
		   numObsDataSets=0;
		   numStats=0;
		   standardized=false;
		   useSubset = false;
	   }

	   ~TObsDataSet(){
		  delete[] myObsValues;
		  if(standardized) delete[] myStandardizedObsValues;
		  if(useSubset){
			  delete[] myObsValuesUsed;
			  if(standardized) delete[] myStandardizedObsValuesUsed;
		  }

	   }
	   void add(double value);
	   void subsetStats(vector<int> & usedStats);
	   void standardize(double* means, double*sds, vector<int> & usedStats);
	   double* getPointerToObsData();
	   double* getPointerToObsDataAllStats();
	   void getStandardizedObsValuesIntoColumnVector(ColumnVector& colVector);
	   void getObsValuesIntoColumnVector(ColumnVector& colVector);
};

class TObsData{
   public:
	  std::string obsFileName;
	  vector<std::string> obsNameVector;
	  vector<std::string>::iterator curObsNameVector, endObsNameVector;
	  int numStats, numUsedStats;
	  int numObsDataSets;
	  vector<TObsDataSet*> obsDataSets;
	  vector<int> usedStats;

	  Matrix obsValuesMatrix;
	  bool hasBeenStandardized;
	  TLog* logfile;

	  TObsData(TLog* logFile);
      TObsData(std::string ObsFileName, TLog* logFile);
      ~TObsData(){
    	  clear();
      }
      void update(std::string ObsFileName);
      void clear();
	  void readObsFile();
	  int getStatColNumberFromName(std::string name);
	  void fillNamesVector(vector<std::string> & namesVec);
	  void fillUsedNamesVector(vector<std::string> & namesVec);
	  void fillObsDataMatrix(int dataSet);
	  void standardizeObservedValues(double* means, double*sds);
	  double* getObservedValues(int num);
	  double* getObservedValuesAllStats(int num);
	  std::string getStatNameFromColNumber(int num);
	  void getObsValuesIntoColumnVector(int & num, ColumnVector& colVector);
	  bool statIsUsed(int & num);
	  void useOnlyTheseStats(vector<int> & theseStats);
	  void excludeTheseStats(vector<int> & excludeTheseStats);
	  void useAllStats();
	  void subsetStatsInObjects();
};

#endif
