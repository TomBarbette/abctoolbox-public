//---------------------------------------------------------------------------

#ifndef TTransformerH
#define TTransformerH
#include <map>
#include <vector>
#include <fstream>
#include "TParameters.h"
#include "stringFunctions.h"
#include "TLog.h"
#include "TLinearComb.h"
#include "TLinearCombBoxCox.h"


//---------------------------------------------------------------------------

class TTransformer{
	private:
		bool boxcox;
		TLog* logFile;
		std::string LinearComb_fileName;
		std::string inputFileName;
		std::string outputFileName;
		int numLinearComb;

	public:
		TTransformer(TParameters* gotParameters, TLog* gotLogFile);
		void transform();
};
#endif
