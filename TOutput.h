//---------------------------------------------------------------------------

#ifndef TOutputH
#define TOutputH

#include "TDataVector.h"
#include "TInputFileVector.h"
#include "TLinearComb.h"
#include <vector>
//---------------------------------------------------------------------------

class TOutput{
	public:
		int sampling; //every ith simulation is recorded
		std::string filename;
		ofstream myFile;
		bool writeDistance;
		bool writeStats;
		bool overWrite;

		TOutput(){
			sampling = 1;
			writeDistance = false;
			writeStats = false;
			overWrite = false;
		};
		TOutput(int gotSampling, std::string outputFilename, bool WriteStats, bool WriteDistance, bool OverWrite);
		virtual ~TOutput(){myFile.close();};
		void CheckIfFileExists();
		virtual void writeHeader(TInputFileVector* InputFiles, TDataVector* Data);
        void writeHeader(TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject);
		virtual void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance=-1);
		void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject, double distance=-1);
};
class TOutputOneObs:public TOutput{
	public:
	   int obsFileNumber;

	   TOutputOneObs(int gotSampling, int gotObsFileNumber, std::string outputFilename, bool WriteStats, bool WriteDistance, bool OverWrite);
	   ~TOutputOneObs(){myFile.close();};
	   virtual void writeHeader(TInputFileVector* InputFiles, TDataVector* Data);
	   virtual void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance=-1);
};

class TOutputVector{
	public:
		vector<TOutput*> vecOutputFiles;
		vector<TOutput*>::iterator curOutput, endOutput;
		TDataVector* data;
		TInputFileVector* inputFiles;
		bool separateOutputFiles;
		bool inputFileAndDataPointerSet;

		TOutputVector(std::string mcmcsampling, std::string outName, TDataVector* Data, TInputFileVector* InputFiles, bool WriteStats, bool gotWriteDistance=false, bool gotSeparetOutputFiles=false, bool forceOverWrite=false);
		~TOutputVector(){
			for(unsigned int i=0; i< vecOutputFiles.size();++i){
				delete vecOutputFiles[i];
			}
			vecOutputFiles.clear();
		};                                    
		void reateOutputFiles(bool writeDistance);
		void writeHeader();
		void writeHeader(TLinearComb* pcaObject);
		void writeHeader(TInputFileVector* InputFiles, TDataVector* Data);
		void writeHeader(TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject);
		void writeSimulations(int run);
		void writeSimulations(int run, double distance);
		void writeSimulations(int run, TLinearComb* pcaObject);
		void writeSimulations(int run, TLinearComb* pcaObject, double distance);
		void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data);
		void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, double distance);
		void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject);
		void writeSimulations(int run, TInputFileVector* InputFiles, TDataVector* Data, TLinearComb* pcaObject, double distance);

};






#endif
