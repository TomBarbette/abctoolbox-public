//---------------------------------------------------------------------------


#include "TLinearCombBoxCox.h"
//---------------------------------------------------------------------------
TLinearCombBoxCox::TLinearCombBoxCox(std::string gotLinearCombFileName, std::vector<std::string> NamesInInputFiles, int maxLinearCombToUse){
	LinearCombFileName=gotLinearCombFileName;
	namesInInputFiles=NamesInInputFiles;
	numStats=namesInInputFiles.size();
	statIsUsed = new int[numStats];
	transformedData = new double[numStats];
	readLinearCombFile();
	maxNumCombToUse=maxLinearCombToUse;
	if(maxNumCombToUse<0) maxNumCombToUse=numLinearComb;
	simPCA = new double[maxNumCombToUse];
	oldSimPCA = new double[maxNumCombToUse];
}
//------------------------------------------------------------------------------
void TLinearCombBoxCox::readLinearCombFile(){
	numLinearComb=0;
	std::string line;
	std::vector<std::string> vecLine;
	std::vector<double> tempPCAVector;
	std::vector<double*> pcaVector;
	std::vector<double*>::iterator curPCA, endPCA;
	std::vector<double> statMeanVector;
	std::vector<double> statSdVector;
	std::vector<double> statMaxVector;
	std::vector<double> statMinVector;
	std::vector<double> lambdaVector;
	std::vector<double> statGMVector;

	//open PCA_file
	std::ifstream linearCombFile (LinearCombFileName.c_str()); // opening the file for reading
	if(!linearCombFile)
	   throw "The Linear-Combination-File '" + LinearCombFileName + "' can not be read!";

	//read PCA File
	double temp;
	int lineNum=0;
	while(linearCombFile.good() && !linearCombFile.eof()){
		++lineNum;
		getline(linearCombFile, line);
		line=extractBeforeDoubleSlash(line);
		trimString(line);
		if(!line.empty()){
			//split by white space
			fillVectorFromStringWhiteSpace(line, vecLine);
			if(vecLine.size() < 4) throw "Less than four entries on line " +toString(lineNum) + "!";
			//read name
			statNamesVector.push_back(vecLine[0]);

			//read max, min, lambda, GM
			statMaxVector.push_back(stringToDouble(vecLine[1]));
			temp=stringToDouble(vecLine[2]);
			if(temp>=(*statMaxVector.rbegin())) throw "Min >= Max in the Linear-Combination-File '" + LinearCombFileName + "' on line " + toString(lineNum) + "! ";
			statMinVector.push_back(temp);
			lambdaVector.push_back(stringToDouble(vecLine[3]));
			temp=stringToDouble(vecLine[4]);
			if(temp<=0) throw "Geometric mean <=0 in the Linear-Combination-File '" + LinearCombFileName + "' on line " + toString(lineNum) + "! ";
			statGMVector.push_back(stringToDouble(vecLine[4]));

			//read Mean and sd after Boxcox
			statMeanVector.push_back(stringToDouble(vecLine[5]));
			temp=stringToDouble(vecLine[6]);
			if(temp<=0.0) throw "SD of zero in the Linear-Combination-File '" + LinearCombFileName + "' on line " + toString(lineNum) + "! Has a Box-Cox transformation been used?";
			statSdVector.push_back(temp);

			//read linear combination into temp vector
			tempPCAVector.clear();
			for(std::vector<std::string>::iterator it=vecLine.begin()+7; it!=vecLine.end(); ++it){
				if(it->empty()) break;
				tempPCAVector.push_back(stringToDouble(*it));
			}

			//create linear combination array
			if((int)tempPCAVector.size()==numLinearComb || (numLinearComb==0 && pcaVector.empty())){
			   numLinearComb=tempPCAVector.size();
			   //put into array for fast calculations
			   double* tempArray=new double[numLinearComb];
			   for(int i=0; i<numLinearComb; ++i) tempArray[i]=tempPCAVector[i];
			   pcaVector.push_back(tempArray);
			} else
			   throw "Unequal number of columns among the lines in the Linear-Combination-File '" + LinearCombFileName + "'!";
		}
	}
	linearCombFile.close();


	//check stats
	std::string missingStat=getFirstMissingStat();
	if(!missingStat.empty())
	   throw "The summary statistic '"+ missingStat +"' is missing in the simulated File, but required by the Linear-Combination-File '"+ LinearCombFileName + "'!";

	//write pls into an array.
	//The order of the array is similar to the order in the inputFile
	linearCombFileRead=true;
	endPCA=pcaVector.end();
	pcaVectorSize=pcaVector.size();
	pca=new double*[pcaVector.size()];
	max=new double[pcaVector.size()];
	min=new double[pcaVector.size()];
	lambda=new double[pcaVector.size()];
	gm=new double[pcaVector.size()];
	mean=new double[pcaVector.size()];
	sd=new double[pcaVector.size()];
	int k=0;
	int j=0;
	for(curNameInInputFiles=namesInInputFiles.begin();curNameInInputFiles!=namesInInputFiles.end(); ++curNameInInputFiles, ++j){
		curPCA=pcaVector.begin();
		statIsUsed[j]=-1;
		for(int i=0;curPCA!=endPCA; ++curPCA, ++i){
			if(statNamesVector[i]==*curNameInInputFiles){
				pca[k]=*curPCA;
				max[k]=statMaxVector[i];
				min[k]=statMinVector[i];
				lambda[k]=lambdaVector[i];
				gm[k]=statGMVector[i];
				mean[k]=statMeanVector[i];
				sd[k]=statSdVector[i];
				statIsUsed[j]=k;
				++k;
			}
		}
	}
	linearCombVariances = new double[numLinearComb];
}
//------------------------------------------------------------------------------
void TLinearCombBoxCox::calcObsLinearComb(std::vector<double> obsData){
	//order has to be the same as the names passed to the constructor!!!
	obs = new double[numStats];
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			obs[i] = getBoxCox(obsData[i], statIsUsed[i]);
		}
	}

	//calculate pca for obsData
	obsPCA = new double[maxNumCombToUse];
	for(int i=0; i<maxNumCombToUse; ++i){
		obsPCA[i]=getPCA(i, obs);
	}
	obsLinearCombComputed=true;
}
//------------------------------------------------------------------------------
double TLinearCombBoxCox::getBoxCox(double & simData, int & stat){
	double tmp = 1+(simData-min[stat])/(max[stat]-min[stat]);
	return (pow(tmp, lambda[stat])-1)/(lambda[stat]*pow(gm[stat], (lambda[stat]-1)));
}

void TLinearCombBoxCox::calcSimDataPCA(double** simData){
	//transform stats via boxcox
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			transformedData[i] = getBoxCox(*simData[i], statIsUsed[i]);
		}
	}
	for(int i=0; i<maxNumCombToUse; ++i){
		simPCA[i]=getPCA(i, transformedData);
	}
}
void TLinearCombBoxCox::calcSimDataPCA(double* simData){
	//transform stats via boxcox
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			transformedData[i] = getBoxCox(simData[i], statIsUsed[i]);
		}
	}

	for(int i=0; i<maxNumCombToUse; ++i){
		simPCA[i] = getPCA(i, transformedData);
	}
}
void TLinearCombBoxCox::calcSimDataPCA(std::vector<double> & simData){
	//transform stats via boxcox
	std::vector<double>::iterator it=simData.begin();
	for(int i=0; i<numStats; ++i, ++it){
		if(statIsUsed[i]>=0){
			transformedData[i] = getBoxCox(*it, statIsUsed[i]);
		}
	}
	for(int i=0; i<maxNumCombToUse; ++i){
		simPCA[i] = getPCA(i, transformedData);
	}
}
//------------------------------------------------------------------------------



