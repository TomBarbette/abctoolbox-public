CFLAGS=-O3
SOURCES=$(shell find . -name "*.cpp")
OBJECTS=$(SOURCES:%.cpp=%.o)
TARGET=ABCtoolbox

.PHONY: all
	all: $(TARGET)

$(TARGET): $(OBJECTS)
	        $(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) $(CFLAGS) -o $@

.cpp.o:
	    $(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(TARGET) $(OBJECTS)

