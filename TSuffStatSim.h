//---------------------------------------------------------------------------

#ifndef TSuffStatSimH
#define TSuffStatSimH

#include "TMcmcSim.h"
#include "TParameters.h"
#include "TDataVector.h"
#include "TOutput.h"
#include "TSuffStatBlock.h"
#include "TLog.h"
#include <fstream>
#include <iostream>
#include "stringFunctions.h"

//---------------------------------------------------------------------------
class TSuffStatSim:public TMcmcSim{
   public:
	  TSuffStatBlockVector* myBlocks;
	  std::string suffStatFileName;

	  TSuffStatSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
	  ~TSuffStatSim(){
		  delete myBlocks;
	  };

	  void runSimulations();
	  void initialCalibration();
	  void performMcmcSimulation(int s);
	  void checkAcceptance();
	  void writeSimulations(int & s);
	  bool startupPassed();
	  void resetStartingPoints();
};
/*
//---------------------------------------------------------------------------
class TSuffStatSimIndependentMeasures:public TMcmcSim{
public:
	  TSuffStatBlockVector* myBlocks;
	  std::string suffStatFileName;

	  TSuffStatSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
	  ~TSuffStatSim(){
		  delete myBlocks;
	  };

	  void runSimulations();
	  void initialCalibration();
	  void performMcmcSimulation(int s);
	  void checkAcceptance();
	  void writeSimulations(int & s);

}
*/

#endif
