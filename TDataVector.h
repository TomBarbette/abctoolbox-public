//---------------------------------------------------------------------------
#ifndef TDataVectorH
#define TDataVectorH
#include "TParameters.h"
#include "TExecuteProgram.h"
#include "TDataVector.h"
#include "TLinearComb.h"
#include "TInputFileVector.h"
#include <vector>
#include <sstream>
#include "TLog.h"
#include "TObsData.h"
#include <sys/stat.h>
#include <dirent.h>
#include "TProgram.h"

//---------------------------------------------------------------------------
class TData{
   public:
	  int numObsData, numObsDataOrig, numObsDataBoosting; //orig is before boosting
	  double* obsData;
	  double* oldData;
	  double* obsDataVariance; //assed during calibration step
	  std::string* obsDataName;
	  double** simDataPointers;
	  std::vector<double> simDataInput;
	  double* simDataInput_Array;
	  std::vector<std::string> simDataFileName;
	  int numSimData;
	  TLog* logFile;
	  std::string sumStatFile;
	  //bool doPCA;
	  bool storageInitialized;
	  bool simDataInitialized;
	  bool getSimDataFromProgram;
	  bool simDataInputArrayInitialized;
	  TProgram* simulationProgram;
	  TProgram sumStatProgram, scriptBeforeSSCalc, scriptAfterSSCalc;
	  bool tagsToInitializeProgramsPrepared;
	  std::map<std::string, std::string> initializationTags, executingTags;
	  bool doBoosting;

	  TData(){
			getSimDataFromProgram=false;
			storageInitialized=false;
			simDataInitialized=false;
			//other variables
			numObsData = -1;
			numObsDataOrig = -1;
			numObsDataBoosting = -1;
			oldData=NULL;
			obsDataVariance=NULL;
			simDataPointers=NULL;
			numSimData=-1;
			logFile=NULL;
			simulationProgram = NULL;
			obsDataName = NULL;
			obsData = NULL;
			tagsToInitializeProgramsPrepared = false;
			doBoosting = false;
			simDataInput_Array = NULL;
			simDataInputArrayInitialized = false;
	  };
	  TData(std::vector<std::string> & statNames, double* statValues, TLog* gotlogfile, bool DoBoosting);
	  void init(TLog* gotlogfile);
	  ~TData(){
		  if(storageInitialized){
			  delete[] obsData;
			  delete[] obsDataName;
			  delete[] oldData;
			  delete[] obsDataVariance;
			  delete[] simDataPointers;
			  if(simDataInputArrayInitialized) delete[] simDataInput_Array;
		  }
	  };

	  void setSumStatFile(std::string & name);
	  void setProgramToGetSimDataFrom(TProgram* prog);
	  void initializeWithoutSumStatProgram();
	  std::string locateFileRecursively(std::string file, std::string dir);
	  void fillTagsToInitializePrograms();
	  void updateExecutingTags(int & simnum, int & index);
	  void initializeSumstatProgram(std::string & gotSumstatprogram, std::vector<std::string> & gotParameters, std::vector<std::string> & SimDataNames, TPriorVector* priors);
	  void initializeScriptBeforeSSCalc(std::string gotScript, std::vector<std::string> gotParameter);
	  void initializeScriptAfterSSCalc(std::string gotScript, std::vector<std::string> gotParameter);
	  void setOutputFile(int OutputFileDescriptor);
	  void setExternalErrorhandling(std::string & externalErrorHandling);
	  void calculateSumStats(int & simnum, int & index);
	  void readInitialSumStats();
	  void readSumStats();
	  void writeData(std::ofstream& ofs);
	  void writeHeader(std::ofstream& ofs);
	  void writeObsData(std::ofstream& ofs);
	  int getObsDataIndexFromName(std::string name);
	  void resetOldValues();
	  void saveOldValues();
};


//TODO: Split into two: one with and one wothout lineraComb ???
class TDataVector{
   private:
		//double calculateDistanceLinearComb(TLinearComb* pcaObject);

   public:
		std::vector<TData*> vecDataObjects;
		std::vector<TData*>::iterator curDataObject;
		int numDataObjects;
		TLog* logFile;
		int numObsData;
		bool stdLinearCombForDist;
		std::string arpDirectory;
		std::string sumStatsTempFileName;
		std::string tempSuffix;
		double** pointersToObsData;
		double** pointersToSimData;
		double** pointersToVariances;
		std::string* obsDataNamesWithPrefix;
		TInputFileVector* inputFiles;
		bool useSumstatProgram;
		bool launchScriptBeforeSS;
		bool launchScriptAfterSS;
		bool ignoreExecutionError;
		bool suppressExecutionErrorWaring;

		TDataVector(TParameters* GotParameters, TInputFileVector* gotInputFiles, TLog* gotlogfile, std::string & externalErrorHandling, int & childOutputfileDescriptor, std::string Suffix="_testsuffix");
		~TDataVector(){
			delete[] pointersToObsData;
			delete[] pointersToSimData;
			delete[] pointersToVariances;
			delete[] obsDataNamesWithPrefix;
			for(curDataObject=vecDataObjects.begin();curDataObject!=vecDataObjects.end(); ++curDataObject)
				delete *curDataObject;
		};

		void setOutputFile(int OutputFileDescriptor);
		double calculateDistance();
		double calculateDistance(TLinearComb* pcaObject);
		double calculateDistance(double* pointerToData);
		double calculateDistance(double* pointerToData, TLinearComb* pcaObject);
		void summarizeDataObjects();
		void calculateSumStats(int simnum);
		void saveOldValues();
		void resetOldValues();
		void writeHeader(std::ofstream& ofs);
		void writeHeaderOnlyOneDataObject(int thisObs, std::ofstream& ofs);
		void writeData(std::ofstream& ofs);
		void writeDataOnlyOneDataObject(int thisObsofstream, std::ofstream& ofs);
		bool matchColumns(std::vector<std::string> & colNames, int numColNames, int** pointerToArrayToFill);
		std::vector<std::string> getNamesVector();
		std::vector<double> getObsDataVector();
		void fillObsDataArray(double* & obsData);
};

#endif
