/*
 * matrixFunctions.cpp
 *
 *  Created on: May 10, 2012
 *      Author: wegmannd
 */

#include "matrixFunctions.h"

void makeSymmetric(Matrix & m){
	m=0.5*( m + m.t() );
}

void makeInvertable(SymmetricMatrix & m){
	//Trick from Leu: strech the matrix a little bit...
	DiagonalMatrix D;
	Matrix V;
	eigenvalues(m,D,V);

	//make all values on the diagonal to be at least 1/1000 of the largest value
	double newVal=D(D.Ncols())/10000;
	if(newVal < 0.00000000000000000000001) newVal = 0.00000000000000000000001;
	for(int i=1; i<=D.Ncols(); ++i){
		if(D(i)<newVal) D(i)=newVal;
		else break;
	}

	//transform back
	m << V * D * V.t();
}

void fillInvertable(Matrix & m, SymmetricMatrix & m_invertable){
	//make sure the result is symmetric (avoid numeric noise)
	makeSymmetric(m);
	m_invertable << m;
	makeInvertable(m_invertable);
}

void invertMatrix(Matrix m, SymmetricMatrix & inverted){
	fillInvertable(m, inverted);
	inverted=inverted.i();
}

void invertMatrix(SymmetricMatrix m, SymmetricMatrix & inverted){
	makeInvertable(m);
	inverted=m.i();
}




