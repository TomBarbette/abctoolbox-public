//---------------------------------------------------------------------------
#include "TSuffStatBlock.h"
//---------------------------------------------------------------------------
TSuffStatBlock::TSuffStatBlock(const std::string& filename, std::string & Weight, TPriorVector* Priors){
   myLinearCombFile=filename;
   priors = Priors;
   caliDistCalculated=false;
   priorsInitialized=false;
   priorNumbers=NULL;
   startingValuesInitialized=false;
   startingValues=NULL;
   numStartingValues=-1.0;
   caliData=NULL;
   myThreshold=-1.0;
   numPrior=-1.0;
   linearCombCreated = false;
   myLinearComb=NULL;
   numAccepted = 0;
   //parse weight
   weight = stringToDoubleCheck(Weight);
   if(weight<=0) throw "Weight for suff stat block '" + myLinearCombFile + "' is <= 0!";
   cumulWeight = 0;
}
//---------------------------------------------------------------------------
bool TSuffStatBlock::add(std::string & priorName){
	//check if it is a prior
	if(!priors->simplePriorExists(priorName)){
		if(priors->combinedPriorExists(priorName)) throw "The model parameter '" + priorName + "' requested in Suff Stat Block '" + myLinearCombFile + "' is a complex parameter!";
		else throw "The model parameter '" + priorName + "' requested in a Suff Stat Block does not exist!";
	}
   //check if priorName is already in the vector
   if(checkPriorNameExistence(priorName)) return false;
   //check if prior is constant
   if(priors->priorIsConstant(priorName)) throw "The model parameter '" + priorName + "' requested in Suff Stat Block '" + myLinearCombFile + "' is constant!";
   priorNames.push_back(priorName);
   return true;
}
//---------------------------------------------------------------------------
void TSuffStatBlock::createLinearCombObject(const std::vector<std::string>& statNames, const std::vector<double>& obsData, bool boxcox){
   if(boxcox) myLinearComb=new TLinearCombBoxCox(myLinearCombFile, statNames);
   else myLinearComb=new TLinearComb(myLinearCombFile, statNames);
   linearCombCreated=true;
   myLinearComb->calcObsLinearComb(obsData);
}
//---------------------------------------------------------------------------
std::vector<std::string> TSuffStatBlock::getPriorNames(){
   return priorNames;
}
std::string TSuffStatBlock::getPriorNames(std::string delim){
	std::string res;
	concatenateString(priorNames, res, delim);
	return res;
}
//---------------------------------------------------------------------------
bool TSuffStatBlock::checkPriorNameExistence(const std::string& name){
   for(curPriorName=priorNames.begin();curPriorName!=priorNames.end();++curPriorName){
	  if(*curPriorName==name) return true;
   }
   return false;
}
//---------------------------------------------------------------------------
void TSuffStatBlock::initializePriorNumbers(){
	if(!priorsInitialized){
		numPrior=priorNames.size();
		priorNumbers = new int[numPrior];
		priorsInitialized=true;
		//get the number of each prior in the list     !! the order here is different from the one in the est file!!!
		int i=0;
		for(curPriorName=priorNames.begin();curPriorName!=priorNames.end();++curPriorName, ++i){
			priorNumbers[i]=caliData->inputFiles->getNumberOfSimplePriorFromName(*curPriorName);
		}
	}
}
void TSuffStatBlock::calculateCaliDistances(){
	if(!caliDistCalculated){
		caliData->reset();
		caliData->calculateDistances(myLinearComb);
		caliDistCalculated=true;
	}
}

void TSuffStatBlock::setThreshold(float thresholdProportion){
	calculateCaliDistances();
    myThreshold=caliData->getThreshold(thresholdProportion);
}
void TSuffStatBlock::setThresholdFixed(float threshold){
    myThreshold=threshold;
}
void TSuffStatBlock::setMcmcRanges(float mcmcRangeProportion, TLog* logFile){
	calculateCaliDistances();
	if(myThreshold<0) throw "Can not set MCMC ranges: threshold has not been set!";
	initializePriorNumbers();
	float range=0;
	for(int i=0;i<numPrior;++i){
		  range = caliData->setMcmcRangesOnePrior(priorNumbers[i], mcmcRangeProportion);
		  logFile->list("Range for parameter " + priorNames[i] + " set to " + toString(range));
	}
}
void TSuffStatBlock::setMcmcRangesFixed(float mcmcRange){
	initializePriorNumbers();
	for(int i=0;i<numPrior;++i){
		  caliData->setFixedMcmcRangesOnePrior(priorNumbers[i], mcmcRange);
	}
}
void TSuffStatBlock::setMcmcRangesRelativeToRange(float mcmcRange){
	initializePriorNumbers();
	for(int i=0;i<numPrior;++i){
		  caliData->setMcmcRangesRelativeToRangeOnePrior(priorNumbers[i], mcmcRange);
	}
}
void TSuffStatBlock::fillStartingValues(){
	if(myThreshold<0) throw "Can not fill starting values: threshold has not been set!";
	numStartingValues = caliData->num_sims_below_threshold;
	startingValues=new double*[numPrior];
	for(int i=0; i<numPrior; ++i) startingValues[i]=new double[numStartingValues];
	startingValuesInitialized=true;
	for(int i=0;i<numPrior;++i){
		startingValues[i][0]=caliData->priors[caliData->smallestDistCaliSim][priorNumbers[i]];
	  for(int j=0; j<numStartingValues; ++j){
		  if(j!=caliData->smallestDistCaliSim)
		  startingValues[i][j]=caliData->priors[caliData->simsBelowThreshold[j]][priorNumbers[i]];
	  }
	}
}
void TSuffStatBlock::fillStartingValuesRandom(int numValues){
	numStartingValues = numValues;
	startingValues = new double*[numPrior];
	for(int i=0; i<numPrior; ++i) startingValues[i]=new double[numStartingValues];
	startingValuesInitialized=true;
	for(int i=0;i<numPrior;++i){
		//just pick first few from cali sims -> they are supposed to be random
	  for(int j=0; j<numStartingValues; ++j){
		  startingValues[i][j]=caliData->priors[j][priorNumbers[i]];
	  }
	}
}

void TSuffStatBlock::fillStartingValuesFixed(){
	//they are fixed and can be read from cali data -> not a very nice solution....
	numStartingValues=1;
	startingValues=new double*[numPrior];
	for(int i=0; i<numPrior; ++i) startingValues[i]=new double[1];
	startingValuesInitialized=true;
	for(int i=0;i<numPrior;++i){
		 startingValues[i][0]=caliData->inputFiles->priors->simplePriors[priorNumbers[i]]->curValue;
	}
}
//---------------------------------------------------------------------------
void TSuffStatBlock::setStartingValues(double rand){
	if(linearCombCreated){
		if(numAccepted<1){
			int startingValue=floor(rand*(numStartingValues-0.000001));
			for(int i=0; i<numPrior; ++i){
				priors->setSimplePriorValue(priorNumbers[i], startingValues[i][startingValue]);
			}
		}
	} else {
		//in case the block has no LC: start from prior or, in case of hyperprior, start from hypo parameters
		for(int i=0; i<numPrior; ++i){
			if(priors->isHyperPrior(priorNumbers[i])){

			} else {
				if(numAccepted<1) priors->chooseStartingValueFromPrior(priorNumbers[i]);
			}
		}

	}
}
void TSuffStatBlock::updateParametersMcmc(){
  for(int i=0; i<numPrior; ++i) priors->getNewValuesMcmc(priorNumbers[i]);
}
//---------------------------------------------------------------------------
//TSuffStatBlockVector
//---------------------------------------------------------------------------
TSuffStatBlockVector::TSuffStatBlockVector(std::string filename, bool DoBoxCox, TDataVector* myData, TPriorVector* Priors, TRandomGenerator* RandomGenerator, TLog* gotLogFile){
	randomGenerator=RandomGenerator;
	logFile=gotLogFile;
	doBoxCox=DoBoxCox;
	iterationStarted = false;
	logFile->startNumbering("Reading blocks and linear transformations from file '" +  filename + "':");

	//read this file
	//structure: LC file, weight, Param 1, Param 2, ....
	std::ifstream suffStatFile (filename.c_str());
   if(!suffStatFile) throw "The SuffStatFile '" + filename + "' can not be read!";
   std::string line;
   std::vector<std::string> vec;
   int lineNum=0;
   std::vector<std::string>::iterator it;
   double cumulWeight = 0.0;
   blocksHaveEqualWeight = true;
   double firstWeight = -1;
   std::vector<std::string> statNames = myData->getNamesVector();
   std::vector<double> obsData=myData->getObsDataVector();
   while(suffStatFile.good() && !suffStatFile.eof()){
	   ++lineNum;
	   getline(suffStatFile, line);
	   line=extractBeforeDoubleSlash(line);
	   if(!line.empty()){
		  fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
		  if(vec.size()<3) throw "Wrong number of entries on line " + toString(lineNum) + "!";
		   //first check if linearComb file already exists
		  it = vec.begin();
		  if(*it != "-"){
			   for(curSuffStatBlock=suffStatBlocks.begin(); curSuffStatBlock!=suffStatBlocks.end();++curSuffStatBlock){
				  if((*curSuffStatBlock)->myLinearCombFile==*it){
					 throw "The file with linear transformation '" + *it + "' is listed multiple times!";
				  }
			   }
		  }
		  //push back object and add priors
		  suffStatBlocks.push_back(new TSuffStatBlock(*it, vec[1], Priors));
		  ++it; ++it;
		  for(; it!=vec.end(); ++it){
			  suffStatBlocks.back()->add(*it);
		  }

		  //initialize
		  logFile->number("Initializing block from file '" + suffStatBlocks.back()->myLinearCombFile + "':");
		  logFile->addIndent();
		  logFile->list("Parameters: " + suffStatBlocks.back()->getPriorNames(", "));
		  if(suffStatBlocks.back()->myLinearCombFile == "-"){
			  logFile->list("WARNING: no linear combinations defined! Updates to these parameters will be accepted based on the prior densities only!");
		  } else {
			  suffStatBlocks.back()->createLinearCombObject(statNames, obsData, doBoxCox);
			  logFile->list("Initialized " + toString(suffStatBlocks.back()->myLinearComb->numLinearComb) + " linear combinations");
			  if(doBoxCox) logFile->list("All statistics are Box-Cox transformed prior to linear combination computation");
		  }
		  //save cumulative weights
		  logFile->list("Relative weight = " + toString(suffStatBlocks.back()->weight));
		  cumulWeight += suffStatBlocks.back()->weight;
		  suffStatBlocks.back()->cumulWeight = cumulWeight;
		  if(firstWeight < 0) firstWeight = suffStatBlocks.back()->weight;
		  else {
			  if(suffStatBlocks.back()->weight != firstWeight) blocksHaveEqualWeight = false;
		  }
		  logFile->endIndent();
	   }
   }
   suffStatFile.close();
   oneOverNumBlocks = 1.0 / (double) suffStatBlocks.size();
   logFile->endNumbering();

   //check if a prior name is listed several times or not at all
   std::vector<TSuffStatBlock*>::iterator secondIterator;
   std::vector<std::string> priorNames;
   std::vector<std::string>::iterator priorNamesIterator;
   bool* priorFound = new bool[Priors->numSimplePrior];
   for(unsigned int i=0; i<Priors->numSimplePrior; ++i) priorFound[i] = false;
   for(curSuffStatBlock=suffStatBlocks.begin(); curSuffStatBlock!=suffStatBlocks.end();++curSuffStatBlock){
	  priorNames=(*curSuffStatBlock)->getPriorNames();
	  for(priorNamesIterator=priorNames.begin();priorNamesIterator!=priorNames.end();++priorNamesIterator){
		 priorFound[Priors->getNumberOfSimplePriorFromName(*priorNamesIterator)] = true;
		 for(secondIterator=suffStatBlocks.begin(); secondIterator!=suffStatBlocks.end();++secondIterator){
			if(secondIterator!=secondIterator){
			   if((*secondIterator)->checkPriorNameExistence(*priorNamesIterator))
				  throw "The prior name '" + *priorNamesIterator + "' is listed in several suff stat blocks!";
			}
		 }
	  }
   }
   for(unsigned int i=0; i<Priors->numSimplePrior; ++i){
	   if(!priorFound[i] && !Priors->simplePriorIsConstant(i))
		   throw "Parameter '" + Priors->getSimplePriorName(i) + "' is not listed in any suff stat block!";
   }

   //standardize cumul weight
   for(curSuffStatBlock=suffStatBlocks.begin(); curSuffStatBlock!=suffStatBlocks.end();++curSuffStatBlock)
	   (*curSuffStatBlock)->cumulWeight /= cumulWeight;

   //set to first
   curSuffStatBlock = suffStatBlocks.begin();
}
//---------------------------------------------------------------------------
void TSuffStatBlockVector::setThresholdAndMcmcRanges(TSimDatabase* caliData, std::vector<double> & thresholds, bool thresholdFixed, std::vector<double> & mcmcRanges, bool rangesFixed, bool startingPointGiven){
	//go block by block to avoid recalculating the distances!
	std::vector<double>::iterator thresholdIt=thresholds.begin();
	std::vector<double>::iterator rangeIt=mcmcRanges.begin();
	for(curSuffStatBlock=suffStatBlocks.begin(); curSuffStatBlock!=suffStatBlocks.end();++curSuffStatBlock, ++thresholdIt, ++rangeIt){
		logFile->number("Block from file '" + (*curSuffStatBlock)->myLinearCombFile + "':");
		logFile->addIndent();
		(*curSuffStatBlock)->setCaliData(caliData);

		if((*curSuffStatBlock)->linearCombCreated){
			//threshold
			if(thresholdFixed){
				caliData->setFixedThreshold(*thresholdIt);
				(*curSuffStatBlock)->setThresholdFixed(*thresholdIt);
			} else {
				(*curSuffStatBlock)->setThreshold(*thresholdIt);
			}
			logFile->list("Threshold set to " + toString((*curSuffStatBlock)->myThreshold));
			//ranges
			if(rangesFixed){
				(*curSuffStatBlock)->setMcmcRangesFixed(*rangeIt);
				logFile->list("Proposal ranges set to " + toString(*rangeIt) + " of the prior range");
			} else (*curSuffStatBlock)->setMcmcRanges(*rangeIt, logFile);
			//starting values
			if(startingPointGiven) (*curSuffStatBlock)->fillStartingValuesFixed();
			else (*curSuffStatBlock)->fillStartingValues();
			logFile->endIndent();
		} else {
			//this block has no LC: distance will always be zero
			//by setting threshold > 0 (e.g. 1.0), acceptance will be based on prior densities only
			(*curSuffStatBlock)->setThresholdFixed(1000000000000.0);
			//MCMC ranges are set as fixed
			(*curSuffStatBlock)->setMcmcRangesRelativeToRange(*rangeIt);
			logFile->list("Proposal ranges set to " + toString(*rangeIt) + " of the prior range");
			int numStartingVals = 200;
			if(caliData->num_sims < 200) numStartingVals = caliData->num_sims;
			logFile->list("Starting values are set to first " + toString(numStartingVals) + " values from the calibration simulations.");
			(*curSuffStatBlock)->fillStartingValuesRandom(numStartingVals);
		}
   }
	//set to first
	curSuffStatBlock = suffStatBlocks.begin();
}
//---------------------------------------------------------------------------
void TSuffStatBlockVector::setStartingValues(){
   for(std::vector<TSuffStatBlock*>::iterator it=suffStatBlocks.begin(); it!=suffStatBlocks.end();++it){
	   (*it)->setStartingValues(randomGenerator->getRand());
   }
}
//---------------------------------------------------------------------------
void TSuffStatBlockVector::setBestStartingValues(){
   for(std::vector<TSuffStatBlock*>::iterator it=suffStatBlocks.begin(); it!=suffStatBlocks.end();++it){
	   (*it)->setStartingValues(0);
   }
}

//---------------------------------------------------------------------------
void TSuffStatBlockVector::next(){
	++curSuffStatBlock;
	if(curSuffStatBlock==suffStatBlocks.end())
		curSuffStatBlock=suffStatBlocks.begin();
}

void TSuffStatBlockVector::chooseBlock(){
	if(blocksHaveEqualWeight) next();
	else {
		//chose at random
		double tmp = randomGenerator->getRand();
		curSuffStatBlock = suffStatBlocks.begin();
		while((*curSuffStatBlock)->cumulWeight < tmp) ++curSuffStatBlock;
	}
}

void TSuffStatBlockVector::createNewInputFiles(TInputFileVector* inputFiles, int simNum){
    //update the priors of this block mcmc like
    (*curSuffStatBlock)->updateParametersMcmc();
	//now we need to update all combined parameters
	inputFiles->updateCombinedParameters();
	inputFiles->writeNewInputFiles(simNum);
}
//---------------------------------------------------------------------------
double TSuffStatBlockVector::calculateDistance(TDataVector* dataVector){
	//return 0 if there are no linear combinations defined -> acceptance will be based on prior densities only
	if((*curSuffStatBlock)->linearCombCreated) return dataVector->calculateDistance((*curSuffStatBlock)->myLinearComb);
	else return 0.0;
}
//---------------------------------------------------------------------------
double TSuffStatBlockVector::getThreshold(){
   return (*curSuffStatBlock)->myThreshold;
}
//---------------------------------------------------------------------------
void TSuffStatBlockVector::updateWasAccepted(){
	++(*curSuffStatBlock)->numAccepted;
}
//---------------------------------------------------------------------------
bool TSuffStatBlockVector::allMoved(){
	for(std::vector<TSuffStatBlock*>::iterator it=suffStatBlocks.begin(); it!=suffStatBlocks.end();++it){
		if((*it)->numAccepted < 1) return false;
	}
	return true;
}
//---------------------------------------------------------------------------
