//---------------------------------------------------------------------------
#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "TLog.h"
//includes for estimator
#include "TEstimator.h"
//includes for sampler
#include "TMcmcSim.h"
#include "TStandardSim.h"
#include "TPmcSim.h"
#include "TSuffStatSim.h"
//includes for other tools
#include "TTransformer.h"
#include <sys/time.h>

using namespace std;

//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" ABCtoolbox 2.0 ");
	logfile.write("****************");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose=myParameters.parameterExists("verbose");
		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);


		//open log file that handles the output
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" ABCtoolbox 2.0 ");
			logfile.writeFileOnly("****************");
		}

		if(myParameters.parameterExists("suppressWarnings"))
			logfile.suppressWarings();

		//initialize random generator
		logfile.listFlush("Initializing random generator ...");
		TRandomGenerator* randomGenerator;
		if(myParameters.parameterExists("fixedSeed")){
			randomGenerator=new TRandomGenerator(myParameters.getParameterLong("fixedSeed"), true);
		} else if(myParameters.parameterExists("addToSeed")){
			randomGenerator=new TRandomGenerator(myParameters.getParameterLong("addToSeed"), false);
		} else randomGenerator=new TRandomGenerator();
		logfile.flush(" done with seed ");
		logfile.flush(randomGenerator->usedSeed);
		logfile.write("!");

		//switch among tasks
		std::string task=myParameters.getParameterString("task");
		//ESTIMATION
		if(task=="estimate"){
			//create estimation object -> switch between types
			TEstimator myEstimation(&myParameters, randomGenerator, &logfile);
			myEstimation.performEstimations(&myParameters);
		}
		//find stats for model choice
		else if(task=="findStatsModelChoice"){
			TEstimator myEstimation(&myParameters, randomGenerator, &logfile);
			myEstimation.findStatsModelChoice(&myParameters);
		}
		//SIMULATION
		else if(task=="simulate"){
			//set directory where this program is launched
			std::string exeDir=argv[0];
			exeDir=extractPath(exeDir);

			//check which type of simulation to perform
			TSim* mySim=NULL;
			bool typeExists=false;
			std::string type=myParameters.getParameterString("samplerType", false);
			if(type.empty()) type="standard";
			if(type=="standard"){
				mySim=new TStandardSim(&myParameters, exeDir, &logfile, randomGenerator);
				typeExists=true;
			}
			if(type=="MCMC"){
				mySim=new TMcmcSim(&myParameters, exeDir, &logfile, randomGenerator);
				typeExists=true;
			}
			if(type=="PMC"){
				mySim=new TPmcSim(&myParameters, exeDir, &logfile, randomGenerator);
				typeExists=true;
			}
			if(type=="PaSS"){
				mySim=new TSuffStatSim(&myParameters, exeDir, &logfile, randomGenerator);
				typeExists=true;
			}
			if(!typeExists) throw "Unknown samplerType '"+type+"'!";

			//Launch simulation
			mySim->runSimulations();

			//cleaning up
		    logfile.listFlush("Cleaning up ...");
			delete mySim;
			logfile.write(" done!");
			logfile.list("Simulations were performed successfully!");
			logfile.endIndent();
		}
		//TRANSFORM SIMS
		else if(task=="transform"){
			logfile.list("Transforming Statistics");
			TTransformer myTransformer(&myParameters, &logfile);
			myTransformer.transform();
		}
		//UNKNOWN TASK
		else throw "Unknown task! Use either 'simulate', 'estimate', 'transform' or 'findStatsModelChoice'.";


		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!="") logfile.warning("The following parameters were not used: " + unusedParams + "!");

		//clean
		delete randomGenerator;
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	// catch exceptions thrown by newmat11
	catch(BaseException&) {
		logfile.error(BaseException::what());
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
    return 0;
}
//---------------------------------------------------------------------------
