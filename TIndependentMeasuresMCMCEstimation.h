//---------------------------------------------------------------------------
/*
#ifndef TIndependentMeasuresMCMCEstimationH
#define TIndependentMeasuresMCMCEstimationH
#include "TBaseEstimation.h"
#include "TIMDataSet.h"
#include "TPriorVector.h"
#include <sstream>

//---------------------------------------------------------------------------
class TIndependentMeasuresMCMCEstimation:public TBaseEstimation {
   public:
	  TIMDataSet** myDataSets;
	  TPriorVector* myPriors;
	  ColumnVector curTheta;
	  ColumnVector oldTheta;

	  TIndependentMeasuresMCMCEstimation(TParameters* parameters, TRandomGenerator* randomGenerator, TLog* logFile);
	  void performEstimations();
	  void printMarginalDensity(int thisObsDataSet);
	  double getCurrentHastingTerm();
};


#endif
*/
