//---------------------------------------------------------------------------
#include "TPmcSim.h"
//---------------------------------------------------------------------------
TPmcSim::TPmcSim(TParameters* GotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator):TSim(GotParameters, gotexedir, gotLogFile, RandomGenerator){
	calibrationFile=gotParameters->getParameterString("calName", 0);
	logFile->startIndent("Parameters for PMC run:");
	numInterations=gotParameters->getParameterDouble("numInterations");
	logFile->list("Number of iterations is set to ", numInterations);
	particleSize=gotParameters->getParameterDouble("sampleSize");
	logFile->list("Sample size is set to ", particleSize);
	if(particleSize<2*inputFiles->priors->numSimplePrior) throw "Sample size is < 2 * number of parameters!";
	lastParticleSize=(int)gotParameters->getParameterDouble("lastSampleSize", false);
	if(lastParticleSize<=0) lastParticleSize=particleSize;
	logFile->list("Sample size of last iteration is set to ", lastParticleSize);
	tolerance=gotParameters->getParameterDouble("tolerance");
	numRetained=tolerance*particleSize;
	if(numRetained<1) throw "Given the defined threshold, no simulations is retained!!";
	if(numRetained>particleSize) throw "Given the defined threshold, all simulations are retained!";
	logFile->list("Tolerance is set to ", tolerance);
	logFile->list(numRetained, " simulations will be used to generate the next population");
	pmc_range_proportion=gotParameters->getParameterDouble("rangeProp", false);
	if(pmc_range_proportion<=0) pmc_range_proportion=2;
	logFile->list("Range proportion is set to ", pmc_range_proportion);
	numCaliSims=(int)gotParameters->getParameterDouble("numCaliSims");
	if(numCaliSims<numRetained) throw "Less calibration simulations than simulations to retain in the first iteration!";
	repeatsPerParameterVector=gotParameters->getParameterDouble("runsPerParameterVector",0);
	if(!repeatsPerParameterVector) repeatsPerParameterVector=1;
	logFile->list(repeatsPerParameterVector, " repeats per parameter vector");
	logFile->endIndent();

	//initialize other variables
	std::string calibrationFile;
	mySimData=NULL;
	nextDb=-1;
	currentDb=-1;
	oldDb=-1;
	weights=NULL;
	paramMin=NULL;
	paramMax=NULL;

	//prepare the two sim databases
	mySimData=new TSimDatabase*[3];
	weights=new double*[3];
	weights[0]=new double[numRetained];
	weights[1]=new double[numRetained];
	weights[2]=new double[numRetained];
};
//---------------------------------------------------------------------------
void TPmcSim::performCalibratingSimulations(){
	struct timeval start, end;
	gettimeofday(&start, NULL);

	int prog, oldProg;
	oldProg=0;
	logFile->listFlush("Performing ", mySimData[0]->num_sims, " calibration simulations ... (0%)");
	//This function performs a given number of simulations and calculates for
	//all summary statistics mean and variance (used to normalize the distances).
	mySimData[0]=new TSimDatabase(numCaliSims, myData, inputFiles, logFile);
	//Run simulations to fill the arrays
	for(int i=0; i< mySimData[0]->num_sims ; ++i){
		inputFiles->createNewInputFiles(-1);
		inputFiles->performSimulations(-1);
		myData->calculateSumStats(-1);
		mySimData[0]->addSimToDB(myData, inputFiles);
		//report progress
		prog=floor(100*(float)i/(float)mySimData[0]->num_sims);
		if(prog>oldProg){
			oldProg=prog;
			logFile->listOverFlush("Performing ", mySimData[0]->num_sims, " calibration simulations ... (", prog, "%)");
		}
	}
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logFile->overList("Performing ", mySimData[0]->num_sims, " calibration simulations ... done in ", runtime, "min!");
}
//---------------------------------------------------------------------------
void TPmcSim::initialCalibration(){
	logFile->startIndent("Calibration:");
	mySimData[0]=new TSimDatabase(0, myData, inputFiles, logFile);
	  if(!calibrationFile.empty()){
		  mySimData[0]->addSimsFromFile(calibrationFile, numCaliSims);
	  } else {
		  performCalibratingSimulations();
		  mySimData[0]->writeToFile("calibration_file.txt");
	  }
	  //calc things for calibration set
	  logFile->startIndent("Calculating means and variances:");
	  mySimData[0]->calculateMeanVariances();
	  logFile->endIndent();
	  if(doLinearComb) mySimData[0]->calculateDistances(myLinearComb);
	  else mySimData[0]->calculateDistances();
}
//---------------------------------------------------------------------------
void TPmcSim::runSimulations(){
	logFile->startIndent("Performing PMC iteration:");
	 initialCalibration();

	 //set up the second database
	 mySimData[1]=new TSimDatabase(particleSize, myData, inputFiles, logFile);
	 mySimData[2]=new TSimDatabase(particleSize, myData, inputFiles, logFile);
	 nextDb=1;
	 currentDb=0;
	 oldDb=2;

	 ColumnVector vij(inputFiles->priors->numSimplePrior);
	 ColumnVector z(inputFiles->priors->numSimplePrior);
	 double* cumulativeWeights=new double[numRetained];
	 double* newParams=new double[inputFiles->priors->numSimplePrior];

	 for(int iteration=0; iteration<numInterations; ++iteration){
		 logFile->startIndent("Iteration ", iteration, ":");
		 //make rejection
		 mySimData[currentDb]->setThreshold(numRetained);
		 mySimData[currentDb]->fillArrayOfSimsBelowThreshold();
		 logFile->list("Threshold is ", mySimData[currentDb]->threshold);


		 //standardize the retained parameters
		 logFile->listFlush("Calculating weights ...");
		 if(iteration==0) mySimData[currentDb]->calculateMinMaxofParameters(paramMin, paramMax);
		 mySimData[currentDb]->standardizeRetainedParameters(paramMin, paramMax);

		 //set weights
		 if(iteration==0){
			 for(unsigned int i=0;i<numRetained;++i) weights[nextDb][i]=(double)1/(double) numRetained;
		 } else {
			 double weightSum=0;
			 for(unsigned int i=0;i<numRetained;++i){
				 double sum=0;
				 for(unsigned int j=0;j<numRetained;++j){
					 //calculate vij, which is the column vector of the differences between the particle i and j
					 for(unsigned int n=0; n<inputFiles->priors->numSimplePrior; ++n){
					 	vij.element(n)=mySimData[currentDb]->standardizedParameters[i][n]-mySimData[oldDb]->standardizedParameters[j][n];
					 }
					 Matrix temp=-0.5*vij.t()*Sigma_inv*vij;
					 sum+=weights[currentDb][j]*exp(temp.element(0,0));
				 }
				 weights[nextDb][i]=mySimData[currentDb]->getPriorDensityOneRetainedSim(i)/sum;
				 weightSum+=weights[nextDb][i];
			 }
			 for(unsigned int i=0;i<numRetained;++i) weights[nextDb][i]=weights[nextDb][i]/weightSum;
		 }
		 cumulativeWeights[0]=weights[nextDb][0];
		 for(unsigned int i=1;i<numRetained;++i) cumulativeWeights[i]=cumulativeWeights[i-1]+weights[nextDb][i];

		 //calculate Sigma
		 mySimData[currentDb]->calculateWeightedSigmaOfRetainedParameters(Sigma, weights[nextDb]);
		 Sigma=pmc_range_proportion*Sigma;
		 invertMatrix(Sigma, Sigma_inv);

		 try{
			 A=Cholesky(Sigma);
		 } catch (...){
			std::cout << Sigma << std::endl;

		 	throw "Problems solving the Cholesky decomposition of Sigma!";
		 }

		 //generate new population
		 logFile->write(" done!");
		 if(iteration==numInterations-1) particleSize=lastParticleSize;
		 mySimData[nextDb]->empty(particleSize);
		 myOutputFiles=new TOutputVector("1", outName+"_iteration_"+toString(iteration), myData, inputFiles, writeStatsToOutputFile, false, writeSeparateOutputFiles, forceOverWrite);
		 if(!gotParameters->parameterExists("noHeader")){
			 if(doLinearComb) myOutputFiles->writeHeader(myLinearComb);
			 else myOutputFiles->writeHeader();
		 }

		 struct timeval start, end;
		 gettimeofday(&start, NULL);
		 int prog, oldProg;
		 oldProg=0;
		 logFile->listFlush("Generating ", particleSize, " particles ... (0%)");
		 for(unsigned int j=0; j<particleSize;++j){
			 double distance=0;
			 for(int r=0; r<repeatsPerParameterVector;++r){
				 bool parametersAccepted=false;
				 while(!parametersAccepted){
					 double rand=randomGenerator->getRand();
					 //find sim according to weights....
					 int thisSim=0;
					 while(cumulativeWeights[thisSim]<rand) ++thisSim;
					 ColumnVector mu(inputFiles->priors->numSimplePrior);
					 for(unsigned int i=0;i<inputFiles->priors->numSimplePrior;++i) mu.element(i)=mySimData[currentDb]->standardizedParameters[thisSim][i];
					 //get new multivariate normal values
					 for(unsigned int i=0;i<inputFiles->priors->numSimplePrior;++i) z.element(i)=randomGenerator->getNormalRandom(0.0,1.0);
					 //get new parameters
					 z=mu+A*z;
					 //now destandardize them....
					 for(unsigned int i=0;i<inputFiles->priors->numSimplePrior;++i) newParams[i]=(z.element(i)*(paramMax[i]-paramMin[i]))+paramMin[i];
					 parametersAccepted=inputFiles->getLogPriorDensity(newParams);
					 if(parametersAccepted) parametersAccepted=inputFiles->creatNewInputFilesPMC(newParams, j);
				 }
				 inputFiles->performSimulations(j);
				 myData->calculateSumStats(j);
				 distance+=calcDistance();
			 }

			 distance=distance/repeatsPerParameterVector;
			 if(addDistanceToOutputfile) myOutputFiles->writeSimulations(j, distance);
			 else myOutputFiles->writeSimulations(j);
			 mySimData[nextDb]->addSimToDB(myData, inputFiles, distance);

			 //report progress
			 prog=floor(100*(float)j/(float)particleSize);
			 if(prog>oldProg){
				 oldProg=prog;
				 logFile->listOverFlush("Generating ", particleSize, " particles ... (", prog, "%)");
			 }
		 }
		 gettimeofday(&end, NULL);
		 float runtime=(end.tv_sec  - start.tv_sec)/60.0;
		 logFile->overList("Generating ", particleSize, " particles ... done in ", runtime, "min!");


		 delete myOutputFiles;
		 int temp=currentDb;
		 currentDb=nextDb;
		 nextDb=oldDb;
		 oldDb=temp;
		 logFile->endIndent();
	 }
	 logFile->endIndent();
};
