//---------------------------------------------------------------------------
#include "TInputFileVector.h"

//---------------------------------------------------------------------------
//TSimulationProgramInputFileLine
//------------------------------------------------------------------------------
TSimulationProgramInputFileLine::TSimulationProgramInputFileLine(std::string line, TPriorVector* gotPriors){
	std::string temp, delim, buf;
	while(!line.empty()){
		temp=extractBeforeWhiteSpace(line);
		delim=line.substr(0,1);
		line.erase(0,1);
		if(!temp.empty()){
			//check if it is a prior tag
			if(gotPriors->isPriorTag(temp)){
				if(!buf.empty()){
					fragments.push_back(buf.c_str());
					isParameterTag.push_back(false);
				}
				buf=delim;
				fragments.push_back(temp);
				isParameterTag.push_back(true);
				parameterPointer.push_back(gotPriors->getPriorFromName(temp));
			} else {
				buf+=temp+delim;
			}
		}
	}
	if(!buf.empty()){
		fragments.push_back(buf);
		isParameterTag.push_back(false);
	}
}
void TSimulationProgramInputFileLine::writeLine(std::ofstream& out){
	paramTagIt = isParameterTag.begin();
	paramPointerIt = parameterPointer.begin();
	for(std::vector<std::string>::iterator it=fragments.begin(); it!=fragments.end(); ++it, ++paramTagIt){
		if(*paramTagIt){
			(*paramPointerIt)->writeCurValue(out);
			++paramPointerIt;
		}
		else out << *it;
	}
	out << std::endl;
}

void TSimulationProgramInputFileLine::addRequiredParameters(std::vector<TPrior*> & RequiredParams){
	std::vector<TPrior*>::iterator it;
	bool found;
	for(paramPointerIt = parameterPointer.begin(); paramPointerIt != parameterPointer.end(); ++paramPointerIt){
		//check if parameter has already been added
		found = false;
		for(it = RequiredParams.begin(); it != RequiredParams.end(); ++it){
			if(*it == *paramPointerIt){
				found = true;
				break;
			}
		}
		if(!found) RequiredParams.push_back(*paramPointerIt);
	}
}
//---------------------------------------------------------------------------
//TSimulationProgramInputFile
//------------------------------------------------------------------------------
TSimulationProgramInputFile::TSimulationProgramInputFile(std::string Name, TPriorVector* gotPriors, std::string & suffix){
	//open input file
	simulationProgramInputFilename=Name;
	std::ifstream spif(Name.c_str());
	if (!spif) throw "The simulation program input file '" + Name + "' could not be read!";

	//read input file and store it
	std::string line;
	while(spif.good() && !spif.eof()){
		//read line by line
		getline(spif, line);
		if(!line.empty())
			lines.push_back(TSimulationProgramInputFileLine(line, gotPriors));
	}
	spif.close();

	//create and save output file name
	std::string::size_type l=Name.find_last_of('.');
	if(l!=std::string::npos)
		newSimulationProgramInputFilename=Name.substr(0,l)+suffix+Name.substr(l);
	else newSimulationProgramInputFilename=Name+suffix;
}
void TSimulationProgramInputFile::createNewSimulationProgramInputFile(){
	std::ofstream newSpif(newSimulationProgramInputFilename.c_str());
	if (!newSpif) throw "Unable to open '" + newSimulationProgramInputFilename + "' for writing!";
	for(std::vector<TSimulationProgramInputFileLine>::iterator it=lines.begin(); it!=lines.end(); ++it){
		it->writeLine(newSpif);
	}
	newSpif.close();
}

void TSimulationProgramInputFile::addRequiredParameters(std::vector<TPrior*> & RequiredParams){
	for(std::vector<TSimulationProgramInputFileLine>::iterator it=lines.begin(); it!=lines.end(); ++it){
		it->addRequiredParameters(RequiredParams);
	}
}
//---------------------------------------------------------------------------
//TInputFile
//---------------------------------------------------------------------------
TInputFile::TInputFile(std::vector<std::string> Siminputnames, TPriorVector* gotPriors, TLog* Logfile, std::string suffix, TRandomGenerator* RandomGenerator){
	priors = gotPriors;
	logfile = Logfile;
	randomGenerator = RandomGenerator;

	for(std::vector<std::string>::iterator it=Siminputnames.begin(); it!=Siminputnames.end(); ++it)
		simulationProgramInputFiles.push_back(TSimulationProgramInputFile(*it, priors, suffix));

	//record required parameters
	for(std::vector<TSimulationProgramInputFile>::iterator it=simulationProgramInputFiles.begin(); it!=simulationProgramInputFiles.end(); ++it)
		it->addRequiredParameters(requiredParameters);

	//initialize things
	hasChanged = false;
	tagsToInitializeProgramsPrepared = false;
}
//---------------------------------------------------------------------------
void TInputFile::writeNamesToLogfile(){
	if(simulationProgramInputFiles.size()>0){
		logfile->startIndent("Input file(s) and corresponding name(s) after parsing:");
		for(std::vector<TSimulationProgramInputFile>::iterator it=simulationProgramInputFiles.begin(); it!=simulationProgramInputFiles.end(); ++it){
			logfile->list("'" +  it->simulationProgramInputFilename  + "' -> '" + it->newSimulationProgramInputFilename + "'");
		}
		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
void TInputFile::setOutputFile(int OutputFileDescriptor){
	if(simulationProgram.initialized) simulationProgram.setOutputFile(OutputFileDescriptor);
	if(scriptBeforeSimulations.initialized) scriptBeforeSimulations.setOutputFile(OutputFileDescriptor);
	if(scriptAfterSimulations.initialized) scriptAfterSimulations.setOutputFile(OutputFileDescriptor);
}
void TInputFile::setSimulationProgramRedirection(std::string & filename){
	if(simulationProgram.initialized){
		simulationProgram.setRedirection(filename);
		logfile->list("Redirecting std::out output to '" + filename + "'");
	}
}
void TInputFile::setExternalErrorhandling(std::string & externalErrorHandling){
	if(simulationProgram.initialized) simulationProgram.setExternalErrorhandling(externalErrorHandling);
	if(scriptBeforeSimulations.initialized) scriptBeforeSimulations.setExternalErrorhandling(externalErrorHandling);
	if(scriptAfterSimulations.initialized) scriptAfterSimulations.setExternalErrorhandling(externalErrorHandling);
}
//---------------------------------------------------------------------------
void TInputFile::createNewInputFiles(){
	//check if the parameter values required changed
	hasChanged = false;
	for(requiredParametersIt=requiredParameters.begin(); requiredParametersIt!=requiredParameters.end(); ++requiredParametersIt){
		if((*requiredParametersIt)->changed){
			hasChanged = true;
			break;
		}
	}
	if(hasChanged){
		//if one parameter has changed, update all files
		//if several files are in the list parse all of them
		writeNewInputFiles();
	}
}

void TInputFile::writeNewInputFiles(){
	for(std::vector<TSimulationProgramInputFile>::iterator it=simulationProgramInputFiles.begin(); it!=simulationProgramInputFiles.end(); ++it){
		it->createNewSimulationProgramInputFile();
	}
}
//------------------------------------------------------------------------------
std::string TInputFile::getFirstSimulationProgramInputFile(){
	if(simulationProgramInputFiles.size()<1) throw "No simulation program input file specified!";
	return simulationProgramInputFiles[0].newSimulationProgramInputFilename;
}
//------------------------------------------------------------------------------
void TInputFile::fillTagsToInitializePrograms(){
	if(!tagsToInitializeProgramsPrepared){
		initializationTags.insert(std::pair<std::string, std::string>("SIMNUM", "SIMNUM"));
		initializationTags.insert(std::pair<std::string, std::string>("INDEX", "INDEX"));

		if(simulationProgramInputFiles.size()>0){
			int i=1;
			for(std::vector<TSimulationProgramInputFile>::iterator it=simulationProgramInputFiles.begin(); it!=simulationProgramInputFiles.end(); ++it, ++i){
				initializationTags.insert(std::pair<std::string, std::string>("SIMINPUTNAME" + toString(i), it->newSimulationProgramInputFilename));
			}
			if(simulationProgramInputFiles.size()==1) initializationTags.insert(std::pair<std::string, std::string>("SIMINPUTNAME", simulationProgramInputFiles.begin()->newSimulationProgramInputFilename));
		}
		tagsToInitializeProgramsPrepared = true;

		//prepare tags to be parsed when executed
		//Note: order is hard-coded in alphabetical order (due to map sorting...)
		executingTags.insert(std::pair<std::string, std::string>("INDEX", ""));
		executingTags.insert(std::pair<std::string, std::string>("SIMNUM", ""));
	}
}
void TInputFile::updateExecutingTags(int simnum, int & index){
	//first index
	std::map<std::string, std::string>::iterator tagIt=executingTags.begin();
	tagIt->second = toString(index);
	//then simnum
	++tagIt;
	tagIt->second = toString(simnum);
}
//------------------------------------------------------------------------------
void TInputFile::initializeProgram(TProgram & progPointer, std::string & gotSimulationprogram, std::vector<std::string> & gotParameter){
	fillTagsToInitializePrograms();
	progPointer.initialize(gotSimulationprogram, gotParameter, initializationTags, priors, logfile, randomGenerator);
	progPointer.addRequiredParameters(requiredParameters);
	logfile->list("Command line to be used: " + progPointer.getCommandLine());
	progPointer.writefoundPriorNames(logfile);
	logfile->endIndent();
}
void TInputFile::initializeSimulationProgramm(std::string & gotSimulationprogram, std::vector<std::string> & gotParameter){
	logfile->startIndent("Using simulation program '" + gotSimulationprogram + "'");
	initializeProgram(simulationProgram, gotSimulationprogram, gotParameter);
}
void TInputFile::initializeScriptAfterSimulation(std::string & gotScript, std::vector<std::string> & gotParameter){
	logfile->startIndent("Launching script '" + gotScript + "' after simulation program.");
	initializeProgram(scriptAfterSimulations, gotScript, gotParameter);
}
void TInputFile::initializeScriptBeforeSimulation(std::string & gotScript, std::vector<std::string> & gotParameter){
	logfile->startIndent("Launching script '" + gotScript + "' before simulation program.");
	initializeProgram(scriptBeforeSimulations, gotScript, gotParameter);
}
//------------------------------------------------------------------------------
void TInputFile::writeCommandLines(int & simnum, int & index){
	logfile->startIndent("Command lines to be used:");
	updateExecutingTags(simnum, index);
	if(scriptBeforeSimulations.initialized) logfile->list("Script before: " + scriptBeforeSimulations.getCommandLine(executingTags));
	logfile->list("Simulation program: " + simulationProgram.getCommandLine(executingTags));
	if(scriptAfterSimulations.initialized) logfile->list("Script after: " + scriptAfterSimulations.getCommandLine(executingTags));
	logfile->endIndent();
}
//------------------------------------------------------------------------------
void TInputFile::performSimulation(int simnum, int & index){
	updateExecutingTags(simnum, index);
	if(scriptBeforeSimulations.initialized) scriptBeforeSimulations.execute(executingTags);
	simulationProgram.execute(executingTags);
	if(scriptAfterSimulations.initialized) scriptAfterSimulations.execute(executingTags);
}
//---------------------------------------------------------------------------
//TInputFileVector
//---------------------------------------------------------------------------
TInputFileVector::TInputFileVector(TParameters* gotParameters, TLog* gotLogFile, TRandomGenerator* RandomGenerator, std::string & externalErrorHandling, int & childOutputfileDescriptor, std::string Suffix){
	tempSuffix=Suffix;
	logFile=gotLogFile;
	randomGenerator=RandomGenerator;

	logFile->startIndent("Initializing simulations:");

	//est file=file with prior definitions
	estFile=gotParameters->getParameterString("estName");
	priors = new TPriorVector(estFile, logFile, randomGenerator);

	alsoLaunchedWhenNoParamChanged = gotParameters->parameterExists("launchUnchanged");
	if(alsoLaunchedWhenNoParamChanged) logFile->list("Also rerunning simulation programs if required model parameters remain unchanged.");
	else logFile->list("Only executing simulation programs if required model parameters changed.");


	//------------------------------------------------
	//Simulation program
	//------------------------------------------------
	logFile->startNumbering("Initializing simulation programs:");
	//read simulation programs...
	std::vector<std::string> tmp, simProgList;
	gotParameters->fillParameterIntoVector("simProgram", tmp, ';');
	if(tmp.empty()) throw "No simulation program specified!";
	//some could be vectors -> expand those!
	repeatAndExpandIndexes(tmp, simProgList);

	//read arguments for the simulation program
	std::vector< std::vector<std::string> > simParamList;
	if(gotParameters->parameterExists("simArgs")){
		gotParameters->fillParameterIntoVector("simArgs", tmp, ';');
		//some could be vectors -> expand those!
		repeatAndExpandIndexesOfSubs(tmp, simParamList, " \t\f\v\n\r");
		if(simParamList.size() !=  simProgList.size()) throw "Number of argument lists does not match number of simulation programs!";
	} else {
		for(unsigned int i = 0; i<simProgList.size(); ++i)
			simParamList.push_back(std::vector<std::string>());
	}

	//read input files
	std::vector< std::vector<std::string> > inputFileVec;
	if(gotParameters->parameterExists("simInputName")){
		gotParameters->fillParameterIntoVector("simInputName", tmp, ';');
		//some could be vectors -> expand those!
		repeatAndExpandIndexesOfSubs(tmp, inputFileVec, ",");
		if(inputFileVec.size() !=  simProgList.size()) throw "Number of input files does not match number of simulation programs!";
	} else {
		for(unsigned int i = 0; i<simProgList.size(); ++i)
			inputFileVec.push_back(std::vector<std::string>());
	}

	//read redirection
	std::vector<std::string> redirectionNames;
	if(gotParameters->parameterExists("simOutputRedirection")){
		gotParameters->fillParameterIntoVector("simOutputRedirection", tmp, ';');
		//some could be vectors -> expand those!
		repeatAndExpandIndexes(tmp, redirectionNames);
		if(redirectionNames.size() !=  simProgList.size()) throw "Number of output redirections does not match number of simulation programs!";
	} else {
		for(unsigned int i = 0; i<simProgList.size(); ++i)
			redirectionNames.push_back("");
	}

	//------------------------------------------------
	//script to run BEFORE simulation
	//------------------------------------------------
	std::vector<std::string> scriptBefore;
	std::vector< std::vector<std::string> > scriptParamListBefore;
	gotParameters->fillParameterIntoVector("scriptBeforeSim", tmp, ';', false);
	if(tmp.size()>0){
		//some could be vectors -> expand those!
		repeatAndExpandIndexes(tmp, scriptBefore);
		if(scriptBefore.size()!=simProgList.size()) throw "Unequal number of scripts to launch before simulations and simulation programs!";
		//read parameters
		if(gotParameters->parameterExists("scriptBeforeSimArgs")){
			gotParameters->fillParameterIntoVector("scriptBeforeSimArgs", tmp, ';');
			//some could be vectors -> expand those!
			repeatAndExpandIndexesOfSubs(tmp, scriptParamListBefore, " ");
			if(scriptParamListBefore.size() !=  scriptBefore.size()) throw "Number of argument lists does not match number of scripts to launch before simulation program!";
		} else {
			for(unsigned int i = 0; i<scriptBefore.size(); ++i)
				scriptParamListBefore.push_back(std::vector<std::string>());
		}
		launchScriptBeforeSimulation=true;
	} else launchScriptBeforeSimulation=false;

	//------------------------------------------------
	//script to run AFTER simulation
	//------------------------------------------------
	std::vector<std::string> scriptAfter;
	std::vector< std::vector<std::string> > scriptParamListAfter;
	gotParameters->fillParameterIntoVector("scriptAfterSim", tmp, ';', false);

	if(tmp.size()>0){
		//some could be vectors -> expand those!
		repeatAndExpandIndexes(tmp, scriptAfter);
		if(scriptAfter.size()!=simProgList.size()) throw "Unequal number of scripts to launch after simulations and simulation programs!";
		//read parameters
		if(gotParameters->parameterExists("scriptAfterSimArgs")){
			gotParameters->fillParameterIntoVector("scriptAfterSimArgs", tmp, ';');
			//some could be vectors -> expand those!
			repeatAndExpandIndexesOfSubs(tmp, scriptParamListAfter, " ");
			if(scriptParamListAfter.size() !=  scriptAfter.size()) throw "Number of argument lists does not match number of scripts to launch after simulation program!";
		} else {
			for(unsigned int i = 0; i<scriptAfter.size(); ++i)
				scriptParamListAfter.push_back(std::vector<std::string>());
		}
		launchScriptAfterSimulation=true;
	} else launchScriptAfterSimulation=false;

	//-------------------------------------------------
	//create objects and initialize simulation programs
	//do at end for neat output
	//-------------------------------------------------
	int i = 0;
	int index = 1;
	int simnum = -1;
	TInputFile* lastInputFile;
	for(std::vector< std::vector<std::string> >::iterator it=inputFileVec.begin(); it!=inputFileVec.end(); ++it, ++i, ++index){
		logFile->number("Initializing '" + simProgList[i] + "':");
		logFile->addIndent();
		//input files
		vecInputFileObjects.push_back(new TInputFile(*it, priors, logFile, tempSuffix, randomGenerator));
		lastInputFile = vecInputFileObjects.back();
		lastInputFile->writeNamesToLogfile();
		//script before
		if(launchScriptBeforeSimulation){
			if(scriptBefore[i]=="-" || scriptBefore[i]=="") logFile->list("No script is launched before simulation program.");
			else lastInputFile->initializeScriptBeforeSimulation(scriptBefore[i], scriptParamListBefore[i]);
		}
		//sim program
		lastInputFile->initializeSimulationProgramm(simProgList[i], simParamList[i]);
		if(redirectionNames[i]!="") lastInputFile->setSimulationProgramRedirection(redirectionNames[i]);
		//script after
		if(launchScriptAfterSimulation){
			if(scriptAfter[i]=="-" || scriptAfter[i]=="") logFile->list("No script is launched after simulation program.");
			else lastInputFile->initializeScriptAfterSimulation(scriptAfter[i], scriptParamListAfter[i]);
		}
		lastInputFile->setExternalErrorhandling(externalErrorHandling);
		if(childOutputfileDescriptor>0) lastInputFile->setOutputFile(childOutputfileDescriptor);
		//launch test simulation
		logFile->startIndent("Launching a test simulation (with simulation number -1):");
		logFile->listFlush("Creating first input file(s)  ...");
		lastInputFile->writeNewInputFiles();
		logFile->write(" done!");
		lastInputFile->writeCommandLines(simnum, index);
		logFile->listFlush("Performing simulation ...");
		lastInputFile->performSimulation(simnum, index);
		logFile->write(" done!");
		logFile->removeIndent(2);
	}
	logFile->endNumbering();
	logFile->endIndent();
}
//---------------------------------------------------------------------------
void TInputFileVector::setOutputFile(int OutputFileDescriptor){
	curInputFileObject=vecInputFileObjects.begin();
    for(;curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject){
    	(*curInputFileObject)->setOutputFile(OutputFileDescriptor);
	}
}
//---------------------------------------------------------------------------
void TInputFileVector::writeNewInputFiles(int simnum){
   for(curInputFileObject=vecInputFileObjects.begin(); curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject)
	   (*curInputFileObject)->createNewInputFiles();
}
//---------------------------------------------------------------------------
void TInputFileVector::getNewValueMcmc(const int& priorNumber){
   priors->getNewValuesMcmc(priorNumber);
}
//---------------------------------------------------------------------------
void TInputFileVector::createNewInputFiles(int simnum){
   priors->getNewValues();
   writeNewInputFiles(simnum);
}
void TInputFileVector::createNewInputFilesMcmc(int simnum){
   priors->getNewValuesMcmc();
   writeNewInputFiles(simnum);
}
void TInputFileVector::createNewInputFilesMcmcUpdateOnePriorOnly(int simnum){
  priors->getNewValuesMcmcUpdateOnePriorOnly();
  writeNewInputFiles(simnum);
}
bool TInputFileVector::creatNewInputFilesPMC(double* newParams, int simnum){
	if(priors->getNewValuesPMC(newParams)){
		writeNewInputFiles(-1);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------
void TInputFileVector::performSimulations(int simnum){
   int index = 1;
   for(curInputFileObject=vecInputFileObjects.begin();curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject, ++index){
	   if((*curInputFileObject)->hasChanged || alsoLaunchedWhenNoParamChanged || simnum < 0){
		   (*curInputFileObject)->performSimulation(simnum, index);
	   }
   }
}
//---------------------------------------------------------------------------
void TInputFileVector::writeUsedCommandLines(int simnum){
	int index = 1;
	logFile->startNumbering("Command lines to be used:");
	for(curInputFileObject=vecInputFileObjects.begin();curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject, ++index){
		(*curInputFileObject)->writeCommandLines(simnum, index);
	}
	logFile->endNumbering();
}
//---------------------------------------------------------------------------
std::vector<std::string> TInputFileVector::getVectorOfInputFileNames(){
	//return a vector with all inputFileNames that are used to execute
	//the simulation program (-> temporary file names!)
	//if several files are given per Inputfile only the first one is returned....
	std::vector<std::string> filenames;
	curInputFileObject=vecInputFileObjects.begin();
	for(;curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject){
		filenames.push_back((*curInputFileObject)->getFirstSimulationProgramInputFile());
	}
	return filenames;
}
//---------------------------------------------------------------------------
std::string TInputFileVector::getSimplePriorName(int num){
	return priors->getSimplePriorName(num);
}
//---------------------------------------------------------------------------
double TInputFileVector::getLogPriorDensity(){
   return priors->getLogPriorDensity();
}
double TInputFileVector::getLogPriorDensity(double* values){
	return priors->getLogPriorDensity(values);
}
//---------------------------------------------------------------------------
int TInputFileVector::getNumberOfSimplePriorFromName(std::string name){
   return priors->getNumberOfSimplePriorFromName(name);
}
//---------------------------------------------------------------------------
void TInputFileVector::setPriorValue(int priorNumber, double value){
   priors->setSimplePriorValue(priorNumber, value);
}
//---------------------------------------------------------------------------
void TInputFileVector::updateCombinedParameters(){
   priors->updateCombinedParameters();
}
//---------------------------------------------------------------------------
TProgram* TInputFileVector::getLinkToSimulationProgramForDataObject(){
	TProgram* prog;
	for(curInputFileObject=vecInputFileObjects.begin();curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject){
		prog = (*curInputFileObject)->getPointerToSimulationProgram();
		if(prog->canBeLinkedToDataObject()){
			prog->linkToDataObject();
			return prog;
		}
	}
	throw "No simulation program to link with a data object!";
}
