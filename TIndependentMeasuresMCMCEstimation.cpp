//---------------------------------------------------------------------------


#include "TIndependentMeasuresMCMCEstimation.h"
//---------------------------------------------------------------------------
/*
TIndependentMeasuresMCMCEstimation::TIndependentMeasuresMCMCEstimation(TParameters* parameters, TRandomGenerator* randomGenerator, TLog* logFile):TBaseEstimation(parameters, randomGenerator, logFile){
	//read .est file! --> need a log file...
	my_string estName=parameters->getParameter("estName");
	myPriors=new TPriorVector(estName, logFile, randomGenerator);
	//check if all parameters, for which stimates are desired, are present in the est file
	for(int i=0; i<mySimData->numParams;++i){
		if(!myPriors->isPrior(mySimData->paramNames[i])) throw "No prior distribution defined for parameter '" + mySimData->paramNames[i] + "')!";
	}
}
//---------------------------------------------------------------------------
void TIndependentMeasuresMCMCEstimation::performEstimations(){

	logfile->write("Independent Realizations MCMC Estimation:");
	logfile->write("-----------------------------------------");
   //standardize
   standardize();
   //create data set objects and make rejection in each
   logfile->flush("Make Rejection in each data set....");
   myDataSets=new TIMDataSet*[myObsData->numObsDataSets];
   for(int i=0; i<myObsData->numObsDataSets;++i){
	   myDataSets[i]=new TIMDataSet(myObsData, mySimData, i);
	   if(distFromDifferentStats) myDataSets[i]->fillRetainedMatrices(myObsDataForDist, mySimDataForDistForDist, thresholdDefined, retainedDefined, threshold, numToRetain, writeRetainedSimulations, outputPrefix);
	   else myDataSets[i]->fillRetainedMatrices(myObsData, mySimData, thresholdDefined, retainedDefined, threshold, numToRetain, writeRetainedSimulations, outputPrefix);
   }
   logfile->write(" done!");
   //perform linear Regression in each data set
   prepareDiracPeaks();
   logfile->flush("Performing local linear regression in each data set...");
   for(int i=0; i<myObsData->numObsDataSets;++i){
   	   myDataSets[i]->performLinearRegression();
   }
   logfile->write(" done!");

   //prepare data sets for the mcmc
   logfile->flush("Prepare the data sets for the MCMC run...");
   prepareDiracPeaks();
   for(int i=0; i<myObsData->numObsDataSets;++i){
   	   myDataSets[i]->prepareLikelihoodCalculation(SigmaThetaInv);
   }
   //store pointers to priors
   TSimplePrior** pointersToSimplePriors;
   pointersToSimplePriors = new TSimplePrior*[mySimData->paramsStandardized];
   for(int i=0; i<mySimData->numParams;++i){
	   pointersToSimplePriors[i]=myPriors->getSimplePriorFromName(mySimData->paramNames[i]);

   }
   //and scale them as is done for the simData
   if(mySimData->paramsStandardized){
	   for(int i=0; i<mySimData->numParams;++i){
		   myPriors->getSimplePriorFromName(mySimData->paramNames[i])->scale(mySimData->paramMaxima[i], mySimData->paramMinima[i]);
	   }
   }
   logfile->write(" done!");

   //prepare array to save MCMC results....
   //TODO make these parameters readable from input file
   int MCMCLength=10000;
   int thinning=10;
   Matrix savedMCMCSteps(floor(MCMCLength/thinning),mySimData->numParams);
   ColumnVector theta(mySimData->numParams);

   clock_t starttime=clock();
   int prog, oldProg;
   oldProg=0;
   logfile->flush((my_string) "- Performing an MCMC with " + MCMCLength + " steps ... (0%)");
   //choose a position from the retained sims of data set one....
   for(int i=0; i<mySimData->numParams; ++i) myPriors->setSimplePriorValue(mySimData->paramNames[i], myDataSets[i]->paramMatrix(1,i+2));
   double oldHastingTerm=getCurrentHastingTerm();
   oldTheta=curTheta;
   myPriors->saveOldValues();
   double curHastingTerm;
   //set proposal range
   for(int i=0; i<myPriors->numSimplePrior; ++i) myPriors->setSimplePriorMCMCStep(i, 0.1);
   //run MCMC
   long lidum=1;
   ofstream mcmc;
   mcmc.open("mcmc.txt");
   for(int i=0; i<MCMCLength;++i){
	   //propose new values
	   myPriors->getNewValuesMcmc();
	   curHastingTerm=getCurrentHastingTerm();
	   if(curHastingTerm/oldHastingTerm>randomGenerator->getRand()){
		   myPriors->saveOldValues();
		   oldHastingTerm=curHastingTerm;
		   oldTheta=curTheta;
	   } else {
		   myPriors->resetOldValues();
	   }
	   if(i%thinning==0){
		   int pos=1+i/thinning;
		   savedMCMCSteps.row(pos)=oldTheta.as_row();
		   mcmc << oldTheta.as_row();
	   }
	   //report progress
	   prog=floor((100*(float)i/(float)MCMCLength));
	   if(prog>oldProg){
		   oldProg=prog;
		   logfile->overFlush((my_string) "- Performing an MCMC with " + MCMCLength + " steps ... ("+ prog +"%)");
	   }
   }
   float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
   logfile->overWrite((my_string) "- Performing an MCMC with " + MCMCLength + " steps ... done in "+ runtime+"min!    ");


   //calculate posterior
   float bandwidthSquare=0.01;
   logfile->flush((my_string)"   - calculating posterior densities for " + mySimData->numParams + " parameters ...");
   preparePosteriorDensityPoints();
   posteriorMatrix=Matrix(posteriorDensityPoints, mySimData->numParams);
   posteriorMatrix=0.0;
   for(int p=1; p<=mySimData->numParams;++p){
	   for(int i=1; i<=floor(MCMCLength/thinning);++i){
		   for(int k=1; k<=posteriorDensityPoints; ++k){
			   posteriorMatrix(k,p)+=exp(-0.5*(savedMCMCSteps(i,p)-theta(k))*(savedMCMCSteps(i,p)-theta(k))/bandwidthSquare);
		   }
	   }
    }
   logfile->write(" done!");

   //write posterior
   writePosteriorFiles();
   writePosteriorCharacteristics();
   //calculate marginal density
   //printMarginalDensity(0);

   logfile->write("Finished estimations!");
}
//---------------------------------------------------------------------------
double TIndependentMeasuresMCMCEstimation::getCurrentHastingTerm(){
	curTheta=ColumnVector(mySimData->numParams);
	if(mySimData->paramsStandardized){
		for(int i=0; i<mySimData->numParams;++i){
			curTheta(i+1)=(myPriors->getValue(mySimData->paramNames[i])-mySimData->paramMinima[i])/(mySimData->paramMaxima[i]-mySimData->paramMinima[i]);
		}
	} else{
		for(int i=0; i<mySimData->numParams;++i)
			curTheta(i+1)=myPriors->getValue(mySimData->paramNames[i]);
	}

	double term=1;
	//get likelihood product
	for(int i=0; i<myObsData->numObsDataSets;++i){
		term*=myDataSets[i]->getLikelihood(curTheta);
	}
	//get prior density
	term*=myPriors->getPriorDensity();
	return term;
}

//---------------------------------------------------------------------------
void TIndependentMeasuresMCMCEstimation::printMarginalDensity(int thisObsDataSet){
   double f_M=0.0;
   Matrix D = SigmaS+CHat*SigmaTheta*CHat.t();
   Matrix D_inv=D.i();
   ColumnVector theta_j, m_j;
   //get mean of s_obs
   ColumnVector meanObs;

   if(calcObsPValue) calculateMarginalDensitiesOfRetained(D, D_inv);

   logfile->write("   - marginal density: ");
   for(int z=0;z<myObsData->numObsDataSets;++z){
	   myObsData->getObsValuesIntoColumnVector(z, meanObs);
        //loop over all dirac peaks
   		f_M=0.0;
   		for(int d=1; d<=mySimData->numUsedSims;++d){
   			theta_j=mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
   			m_j=c_zero+CHat*theta_j;
   			Matrix temp=-0.5*(meanObs-m_j).t()*D_inv*(meanObs-m_j);
   			f_M+=exp(temp(1,1));
   		}
   		f_M=f_M/(mySimData->numUsedSims*sqrt((2*3.14159265358979323846*D).determinant()));
   		f_M=f_M*((double)mySimData->numUsedSims/(double)mySimData->numReadSims);
   		if(calcObsPValue){
   			float p=0;
   			for(int i=0;i<calcObsPValue;++i) if(fmFromRetained[i]<=f_M) ++p;
   			logfile->write((my_string)"        - Obs_" + z + "\t" + (my_string) f_M + "\t(p-value " + (my_string) (p/(float)calcObsPValue) + ")");
   			mdFile << thisObsDataSet << "\t" << f_M << "\t" << p/(float)calcObsPValue << endl;
   		} else {
   			logfile->write((my_string)"        - Obs_" + z + "\t" + (my_string) f_M);
   			mdFile << thisObsDataSet << "\t" << f_M << endl;
   		}
   }
   mdFile.close();
}
//---------------------------------------------------------------------------
*/
