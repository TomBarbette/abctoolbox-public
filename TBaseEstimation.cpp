//---------------------------------------------------------------------------


#include "TBaseEstimation.h"
//---------------------------------------------------------------------------
TJointPosteriorSettings::TJointPosteriorSettings(TParameters* parameters, TLog* LogFile){
	//TODO: write summary to logfile
	//compute joint posteriors?
	if(parameters->parameterExists("jointPosteriors")){
		doJointPosterior=true;
		jointPosteriorDensityPoints=parameters->getParameterDouble("jointPosteriorDensityPoints");
		//how to get the points at which the densities will be computed?
		jointPosteriorDensityPointsFromMarginals=(parameters->getParameterDouble("jointPosteriorDensityPointsFromMarginals", false) != 0);
		jointPosteriorDensityFromMCMC=(parameters->getParameterDouble("jointPosteriorDensityFromMCMC", false) != 0);
	} else doJointPosterior=false;

	//generate posterior samples?
	if(doJointPosterior){
		numApproxJointSamples=parameters->getParameterDouble("jointSamplesApprox", false);
		//sample via MCMC?
		numMCMCJointSamples=parameters->getParameterDouble("jointSamplesMCMC", false);
		if(jointPosteriorDensityFromMCMC && numMCMCJointSamples<100) throw "At last 100 MCMC samples have to be generated to compute joint posterior densities via MCMC!";
		if(numMCMCJointSamples>0){
			//how many samples?
			numMCMCJointSamples=parameters->getParameterDouble("jointSamplesMCMC", false);
			//burnin given or default?
			if(parameters->parameterExists("sampleMCMCBurnin")){
			   sampleMCMCBurnin=parameters->getParameterDouble("sampleMCMCBurnin", false);
			} else sampleMCMCBurnin=100;
			//rangeprop given or default?
			if(parameters->parameterExists("sampleMCMCRangeProp")){
			   sampleMCMCRangeProp=parameters->getParameterDouble("sampleMCMCRangeProp", false);
			} else sampleMCMCRangeProp=0.1;
			//sampling given or default?
			if(parameters->parameterExists("sampleMCMCSampling")){
			   sampleMCMCSampling=parameters->getParameterDouble("sampleMCMCSampling", false);
			} else sampleMCMCSampling=10;
			//where to start?
			if(parameters->parameterExists("sampleMCMCStart")){
			   std::string temp=parameters->getParameterString("sampleMCMCStart");
			   if(temp=="random") sampleMCMCStart=randomStart;
			   else if(temp=="jointmode") sampleMCMCStart=jointmodeStart;
			   else if(temp=="jointposterior") sampleMCMCStart=jointposteriorStart;
			   else if(temp=="marginal") sampleMCMCStart=marginalStart;
			   else throw "'"+temp+"' is an unknown tag used for 'sampleMCMCStart'!";
		   }
		} else sampleMCMCStart=jointmodeStart;
	}
}

//---------------------------------------------------------------------------
//TRootEstimation: an object from which the base estimation is derived, but also TModelaveragingEstimation
//---------------------------------------------------------------------------
TPosteriorMatrix::TPosteriorMatrix(ColumnVector* Theta, TParamRanges* ParamRanges){
	theta=Theta;
	paramRanges=ParamRanges;
	posteriorDensityPoints=theta->nrows();
	step=1.0/(posteriorDensityPoints-1.0);
	posteriorMatrix=Matrix(posteriorDensityPoints, paramRanges->numParams);
	initToZero();
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::calcMarginalAreas(){
	marginalAreas.ReSize(paramRanges->numParams);
	marginalAreasParamScale.ReSize(paramRanges->numParams);
	for(int param=1; param<=paramRanges->numParams; ++param){
		marginalAreas(param) = calcArea(posteriorMatrix.column(param));
		marginalAreasParamScale(param) = marginalAreas(param) * paramRanges->getMinMaxDiff(param);
	}
}

void TPosteriorMatrix::calcAreasParamScale(const Matrix & densities, ColumnVector & areas){
	areas.ReSize(paramRanges->numParams);
	for(int param=1; param<=paramRanges->numParams; ++param){
		areas(param) = calcAreaParamScale(densities, param);
	}
}
double TPosteriorMatrix::calcAreaParamScale(const Matrix & densities, const int & param){
	return calcArea(densities.column(param)) * paramRanges->getMinMaxDiff(param);
}

double TPosteriorMatrix::calcArea(const ColumnVector densities){
	return calcAreaByReference(densities);
}
double TPosteriorMatrix::calcAreaByReference(const ColumnVector & densities){
	//area i s calculated by assuming the density is always given in the middle of a bar from (pos - step/2 , pos + step/2)
	//Hence: all densities have a weight of 1, expect the first and the last
	double area = 0;
	for(int i=2; i<densities.size();++i) area += densities(i);
	area += 0.5 * (densities(1) + densities(densities.size()));
    return step * area;
}

//---------------------------------------------------------------------------
void TPosteriorMatrix::writePosteriorFiles(std::ofstream & output){
   //write values for each parameter: norm area!
   writeDensities(output, posteriorMatrix, marginalAreasParamScale);
}
void TPosteriorMatrix::writeDensities(std::ofstream & output, Matrix & densities, ColumnVector & areas){
	//write Header
	output << "number";
	for(int p=1; p<=paramRanges->numParams;++p){
		output << "\t" << paramRanges->getParamName(p) << "\t" << paramRanges->getParamName(p) << ".density";
	}
	output << std::endl;
	//write values for each parameter: norm area!
	for(int k=1;k<=posteriorDensityPoints;++k){
		  output << k;
		  for(int p=1; p<=paramRanges->numParams;++p){
			 output << "\t" << paramRanges->toParamScale(p, (*theta)(k)) << "\t" << densities(k,p)/areas(p);
		  }
		  output << std::endl;
	}
}
double TPosteriorMatrix::toParameterScale(const int & param, const double & pos){
	return paramRanges->toParamScale(param, pos);
}
double TPosteriorMatrix::toInternalScale(const int & param, const double & val){
	return paramRanges->toInternalScale(param, val);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::thetaParameterScale(const int & param, const int & k){
	return paramRanges->toParamScale(param, (*theta)(k));
}
//double TPosteriorMatrix::theatParameterScale(const int & param, const double & pos){
//	return paramRanges->toParamScale(param, pos);
//}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPositionOfMode(const int & param){
	double max=0;
	int pos=1;
	for(int k=1;k<=posteriorDensityPoints;++k){
		if(posteriorMatrix(k,param)>max){
			max = posteriorMatrix(k,param);
			pos = k;
		}
	}
	return thetaParameterScale(param, pos);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosteriorMean(const int & param){
	double mean=0;
	double w=0;
	for(int k=1;k<=posteriorDensityPoints;++k){
			mean+=posteriorMatrix(k,param)*thetaParameterScale(param, k);
			w+=posteriorMatrix(k,param);
	}
	return mean/w;
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosteriorquantile(const int & param, double q){
	double neededArea = q * marginalAreas(param);

	double a = 0.0;
	double pos = 0.0;
	double area = 0.0;

	//is first bar enough?
	pos = step * 0.5;
	area = posteriorMatrix(1,param) * step * 0.5;
	if(area >= neededArea){
		pos = neededArea / posteriorMatrix(1,param);
		return paramRanges->toParamScale(param, pos);
	}

	//else: sum bars
	for(int k=2;k<=posteriorDensityPoints;++k){
		if(k==1 || k == posteriorDensityPoints) a = step * posteriorMatrix(k,param) * 0.5;
		else a = step * posteriorMatrix(k,param);
		if((area+a) >= neededArea){
			/*
			double add = step*(-0.5+(neededArea-area)/a);
			add = add*paramRanges->getMinMaxDiff(param)/(posteriorDensityPoints-1);
			return thetaParameterScale(param, k)+add;
			*/

			pos += (neededArea - area)/posteriorMatrix(k,param);
			return paramRanges->toParamScale(param, pos);
		}
		else {
			area += a;

			pos += step;
		}
	}

	return 0;
}
void TPosteriorMatrix::writeQuantile(const int & param, double quantile, std::ofstream & output){
	quantile = (1.0 - quantile) / 2.0;
	output << "\t" << getPosteriorquantile(param, quantile) << "\t" << getPosteriorquantile(param, 1.0 - quantile);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosInCumulative(const int & param, double & val){
	//Note: assumes uniform posterior density points from 1 to posteriorDensityPoints
	if(val < 0.0) return 0;
	if(val > 1.0) return 1;
	//else compute position among posterior density points
	int upper = floor(val / step) + 1;
	double area = 0.0;
	//add all posterior densities below pos
	for(int k=1;k<upper;++k) area += posteriorMatrix(k, param);
	double tot = area;
	for(int k=upper;k<=posteriorDensityPoints;++k) tot += posteriorMatrix(k, param);
	//add fraction of last
	area += posteriorMatrix(upper, param) * (val - (upper - 1)*step);
	return area / tot;
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::getPosteriorHDIScaled(const int & param, const double & q, double & lower, double & upper){
	//get shortest interval of fraction q
	//since two values have to be returned, the results are written to a passed variable
	//go through each posterior point and find HDI. return smallest interval.
	//Note: equal spacing between points implied here!
	lower = 0;
	upper = 1;
	double neededArea = q * marginalAreas(param);
	double pos=0, max=0.0;

	//find density at mode
	for(int k=1;k<=posteriorDensityPoints;++k){
		if(posteriorMatrix(k,param) > max){
			max = posteriorMatrix(k,param);
			pos = k;
		}
	}

	//check if bar with mode is enough -> take care of boundaries!
	if(pos==1 && step * max / 2.0 >= neededArea){
		//take left of bar = 1 + required width
		double w = (neededArea/(step*max/2.0));
		lower = 0;
		upper = w * step;
	} else if(pos == posteriorDensityPoints && step * max / 2.0 >= neededArea){
		//take right of bar = posteriorDensityPoints - required width
		double w = (neededArea/(step*max/2.0));
		lower = (posteriorDensityPoints - w - 1.0) * step;
		upper = (posteriorDensityPoints - 1.0) * step;
	} else if(pos>1 && pos < posteriorDensityPoints && step * max >= neededArea){
		//take center of bar +- half of the width
		double w = (neededArea/(step*max));
		lower = (pos - (w/2.0) - 1.0) * step;
		upper = (pos + (w/2.0) - 1.0) * step;
	} else {
		//find it as combination of bars...
		double smallestHdi=1;
		int kl = 1;
		int ku = 1;
		double templ = 0.0;
		double tempu = 0.0;
		double tempArea = 0.0;
		double tempd = 0.0;
		double upperBar, lowerBar;
		while(kl < posteriorDensityPoints){
			//find interval for this lower bound
			upperBar = step * posteriorMatrix(ku,param);
			if(ku == 1 || ku == posteriorDensityPoints) upperBar = upperBar / 2.0; //only step/2 at border...
			if(tempArea + upperBar > neededArea){
				lowerBar = step * posteriorMatrix(kl,param);
				if(kl == 1) lowerBar = lowerBar / 2.0;
				//which density is higher? -> keep higher total and add from smaller...
				if(posteriorMatrix(ku,param) > posteriorMatrix(kl,param)){
					//ku density is higher -> add partial kl density
					//if Area minus lower bar too big -> there is a better solution for sure later on
					if(tempArea + upperBar - lowerBar < neededArea){
						//there is a solution with a partial lower bar
						if(ku == posteriorDensityPoints) tempu = (ku - 1.0) * step;
						else tempu = (ku - 0.5)*step; //complete upper bar, we are 1 of with enumerating (+0.5 -1 = -0.5)
						if(kl == 1) templ = 0.0;
						else templ = (kl-0.5 - (neededArea-(tempArea+upperBar-lowerBar))/lowerBar)*step;
						tempd = tempu-templ;
						if(smallestHdi >= tempd){
							smallestHdi = tempd;
							lower = templ;
							upper = tempu;
						}
					}
					//move one forward, but do not add upper bar, may be not needed in full
				} else {
					//kl density is higher-> add partial ku density
					if(kl == 1) templ = 0.0;
					else templ = (kl - 1.5)*step;
					if(ku == posteriorDensityPoints) tempu = (ku - 1.5 + (neededArea - tempArea)/2.0/upperBar )*step;
					else tempu = (ku - 1.5 + (neededArea - tempArea)/upperBar )*step;
					tempd = tempu-templ;
					if(smallestHdi >= tempd){
						smallestHdi = tempd;
						lower = templ;
						upper = tempu;
					}
					//move one forward and accept whole new bar
					tempArea += upperBar; //add upper bar total, since we remove lower bar which is larger
					++ku;
				}
				//move
				tempArea -= lowerBar; //remove lower bar
				++kl;
			} else {
				tempArea += upperBar;
				++ku;
			}
			if(ku > posteriorDensityPoints) break; //we are through!
		}
	}
}

void TPosteriorMatrix::getPosteriorHDI(const int & param, const double & q, double & lower, double & upper){
	getPosteriorHDIScaled(param, q, lower, upper);
	upper = paramRanges->toParamScale(param, upper);
	lower = paramRanges->toParamScale(param, lower);
}

void TPosteriorMatrix::writeHDI(const int & param, const double & HDI, std::ofstream & output){
	double lower, upper;
	getPosteriorHDI(param, HDI, lower, upper);
	output << "\t" << lower << "\t" << upper;
}
double TPosteriorMatrix::getPosInPosteriorHDI(int param, double trueValue, int steps){
	//this function return the HPDI of the true parameter value
	//the procedure is kind of crude (and could be improved):
	//I implemented a binary search and for comparison always call the HDI function, which seems a bit slow.
	//But for now....
	double hdi = 0.5;
	double lower, upper;
	double binaryStep=0.25;
	for(int i=0; i<steps; ++i){
		getPosteriorHDIScaled(param, hdi, lower, upper);
		if(trueValue==lower || trueValue==upper) return hdi;
		if(trueValue>lower && trueValue<upper){
			//hdi is too large
			hdi -= binaryStep;
		} else {
			//hdi is too small
			hdi += binaryStep;
		}
		binaryStep /= 2.0;
	}
	return hdi;
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::writePosteriorCharacteristicsHeader(std::ofstream & posteriorCharacteristicsFile, bool addL1PriorPosterior){
	posteriorCharacteristicsFile << "dataSet";
    for(int p=1; p<=paramRanges->numParams;++p){
    	std::string pName = paramRanges->getParamName(p);
    	posteriorCharacteristicsFile << "\t" << pName << "_mode";
    	posteriorCharacteristicsFile << "\t" << pName << "_mean";
    	posteriorCharacteristicsFile << "\t" << pName << "_median";

    	posteriorCharacteristicsFile << "\t" << pName << "_q50_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q50_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q90_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q90_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q95_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q95_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q99_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q99_upper";

    	posteriorCharacteristicsFile << "\t" << pName << "_HDI50_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI50_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI90_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI90_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI95_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI95_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI99_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI99_upper";

    	if(addL1PriorPosterior) posteriorCharacteristicsFile << "\t" << pName << "_L1";
   }
    posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristics(std::ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum, Matrix & priorDensities){
	posteriorCharacteristicsFile << obsDataSetNum;
	for(int p=1; p<=paramRanges->numParams;++p){
		writePosteriorCharacteristicsOneParam(posteriorCharacteristicsFile, p);
		//calc and write L1
		posteriorCharacteristicsFile << "\t" << getLOnedistwithPosterior(p, priorDensities);
	}
	posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristics(std::ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum){
	posteriorCharacteristicsFile << obsDataSetNum;
	for(int p=1; p<=paramRanges->numParams;++p){
		writePosteriorCharacteristicsOneParam(posteriorCharacteristicsFile, p);
	}
	posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristicsOneParam(std::ofstream & posteriorCharacteristicsFile, const int & param){
	posteriorCharacteristicsFile << "\t" << getPositionOfMode(param);
	posteriorCharacteristicsFile << "\t" << getPosteriorMean(param);
	posteriorCharacteristicsFile << "\t" << getPosteriorquantile(param, 0.5);

	writeQuantile(param, 0.50, posteriorCharacteristicsFile);
	writeQuantile(param, 0.90, posteriorCharacteristicsFile);
	writeQuantile(param, 0.95, posteriorCharacteristicsFile);
	writeQuantile(param, 0.99, posteriorCharacteristicsFile);
	//TODO: Check HDI!
	writeHDI(param, 0.50, posteriorCharacteristicsFile);
	writeHDI(param, 0.90, posteriorCharacteristicsFile);
	writeHDI(param, 0.95, posteriorCharacteristicsFile);
	writeHDI(param, 0.99, posteriorCharacteristicsFile);
}
void TPosteriorMatrix::writeValidationHeader(std::ofstream & output, std::string & paramName){
	output << "\t" << paramName << "\t" << paramName << "_mode\t" << paramName << "_mean\t" << paramName << "_quantile\t" << paramName << "_HDI";
}
void TPosteriorMatrix::writeValidationStats(std::ofstream & output, const int & param, double val){
	output << "\t" << toParameterScale(param, val);
	output << "\t" << getPositionOfMode(param);
	output << "\t" << getPosteriorMean(param);
	output << "\t" << getPosInCumulative(param, val);
	output << "\t" << getPosInPosteriorHDI(param, val, 20);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getLOnedistwithPosterior(const int & param, Matrix & densities){
	//calculate common surface: norm area
	double LOneDist=0;
	double area = calcAreaParamScale(densities, param);

	for(int k=1;k<=posteriorDensityPoints;++k){
		LOneDist+=abs(densities(k,param)/area - posteriorMatrix(k,param)/marginalAreasParamScale(param));
	}
	return LOneDist;
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getRandomParameterAccordingToMarginal(const int & param, double randomVal){
	//we assume uniform distribution of posterior density points!
	double sum=0;
	double r=randomVal*posteriorMatrix.Column(param).Sum();
	int k=0;
	while(sum<r && k<posteriorDensityPoints){
		++k;
		sum+=posteriorMatrix(k,param);
	}
	double diff=abs(posteriorMatrix(k,param)-posteriorMatrix(k-1,param));
	return (double) k-(sum-r)/diff / (double) (posteriorDensityPoints-1);
}
double TPosteriorMatrix::getPosteriorScaledSD(const int & param){
	//get mean
	double m = 0;
	double w = 0;
	for(int k = 0; k < posteriorDensityPoints; ++k){
		m += posteriorMatrix.element(k,param) * k;
		w += posteriorMatrix.element(k,param);
	}
	m = m * step / w;
	//get std
	double sd = 0;
	for(int k=0; k<posteriorDensityPoints; ++k)
		sd += posteriorMatrix.element(k,param)*(k*step-m)*(k*step-m);
	sd = sd / w;
	return sqrt(sd);
}
void TPosteriorMatrix::setParameterRange(const std::string & param, std::pair<double,double> minmax){
	paramRanges->setMinMax(param, minmax);
}
//---------------------------------------------------------------------------
//TBaseEstimation
//---------------------------------------------------------------------------
TBaseEstimation::TBaseEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* LogFile){
	mySimData=simData;
	outputPrefix=OutputPrefix;
	myJointPosteriorSettings=JointPosteriorSettings;
	marDens=0;
	marDensPValue=0;
	tukeyDepth=0;
	tukeyDepthPValue=0;
	logfile=LogFile;
	randomGenerator = RandomGenerator;

	//initialize status
    exponentsInitialized = false;
    exponentsSize = 0;
    trueParamValidationFileOpen=false;
    computeSmoothedTruncatedPrior=ComputeTruncatedPrior;

    //prepare estimation
    posteriorMatrix=new TPosteriorMatrix(Theta, simData->paramRanges);
    diracPeakWidth=DiracPeakWidth;
    prepareDiracPeaks(); //also used for writing the prior!

    //open output files used for all observed data sets
    openPosteriorCharacteristicsFile();
}
//---------------------------------------------------------------------------
bool TBaseEstimation::calculatePosterior(bool verbose){
	if(verbose)  logfile->listFlush("Preparing posterior density calculation ...");
	//prepare posterior Matrix
	posteriorMatrix->initToZero();
	if(!calculate_T_j(T_j)) return false;
	ColumnVector theta_j, v_j;

	//now loop over all parameters
	//first calculate all Qj in order to shift them -> otherwise maybe too large numbers...
	//the discrepancy is constant and therefore integrated out by normArea
	//loop over all dirac peaks
	if(exponentsInitialized && mySimData->numUsedSims != exponentsSize){
		delete[] exponents;
		exponentsInitialized = false;
	}

	if(!exponentsInitialized){
		exponents = new double[mySimData->numUsedSims];
		exponentsSize = mySimData->numUsedSims;
		exponentsInitialized = true;
	}

	meanExponent=0;
	for(int d=1; d<=mySimData->numUsedSims;++d){
		//theta_j is a vector with the parameters of one simulation / from the uniform matrix
		theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		v_j = calculate_v_j(theta_j);
		//removed (obsValues-c_zero).t()*SigmaSInv*(obsValues-c_zero) from the exponent since it is constant -> corrected by normArea
		Matrix exponent = theta_j.t()*SigmaThetaInv*theta_j-v_j.t()*T_j*v_j;
		exponents[d-1] = -0.5*exponent(1,1);
		meanExponent += exponents[d-1];
	}

	//shift the Qj
	meanExponent=meanExponent/mySimData->numUsedSims;
	for(int d=0; d<mySimData->numUsedSims;++d) exponents[d]=exponents[d]-meanExponent;
	if(verbose) logfile->write(" done!");

	if(verbose) logfile->listFlush("Calculating marginal posterior densities for ", mySimData->numParams, " parameters ...");
	//now calculate the marginal posterior densities
	double c_j;
	double tmpDens;
	for(int p=1; p<=mySimData->numParams;++p){
		double tau=T_j(p,p);
		//loop over all dirac peaks
		for(int d=1; d<=mySimData->numUsedSims;++d){
			//theta_j is a vector with the parameters of one simulation / from the uniform matrix
			theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
			v_j = calculate_v_j(theta_j);
			c_j = exp(exponents[d-1]);
			ColumnVector t_j = T_j * v_j;
			for(int k=1;k<=posteriorMatrix->posteriorDensityPoints;++k){

				tmpDens = c_j * exp(-pow((*posteriorMatrix->theta)(k)-t_j(p), 2.) / (2.*tau));

				posteriorMatrix->addToPosteriorMatrixDensity(k, p, tmpDens);
			}
		}
	}
	posteriorMatrix->calcMarginalAreas();
	if(verbose) logfile->write(" done!");
	return true;
}
//---------------------------------------------------------------------------
bool TBaseEstimation::calculate_T_j(SymmetricMatrix & m){
	m << CHat.t() * SigmaSInv * CHat + SigmaThetaInv;
	makeInvertable(m);
	double det=m.Determinant();
	if(det==0){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	try{
		m=m.i();
	} catch(...){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	return true;
}
//---------------------------------------------------------------------------
ColumnVector TBaseEstimation::calculate_v_j(ColumnVector theta_j){
	return CHat.t()*SigmaSInv*(obsValues-c_zero)+SigmaThetaInv*theta_j;
}
//---------------------------------------------------------------------------
bool TBaseEstimation::performLinearRegression(bool verbose){
	//perform regression
	if(verbose) logfile->listFlush("Performing local linear regression ...");
	C = mySimData->paramMatrix.t() * mySimData->paramMatrix;
	try {
		C = C.i();
	} catch(...){
		logfile->warning("Problems computing inverse of C!");
		std::cout << C << std::endl;
		return false;
	}

	C = mySimData->paramMatrix * C;
	C = mySimData->statMatrix * C;
	c_zero = C.column(1);
	CHat = C.columns(2,C.ncols());
	RHat = mySimData->statMatrix.t()-mySimData->paramMatrix*C.t();
	Matrix SigmaS_temp=RHat.t()*RHat;
	SigmaS_temp=1.0/(mySimData->numUsedSims-mySimData->numParams)*RHat.t()*RHat;
	fillInvertable(SigmaS_temp, SigmaS);

	double det=0;
	try {
		det=SigmaS.Determinant();
	} catch(...){
		logfile->warning("Problems computing determinant of Sigma_S!");
		return false;
	}
	if(det<=0.0){
		logfile->warning("Problems inverting Sigma_S: matrix seems singular!");
		return false;
	}
	try {
		SigmaSInv=SigmaS.i();
	} catch(...){
		logfile->warning("Problems inverting Sigma_S: matrix seems singular!");
		return false;
	}
	if(verbose) logfile->write(" done!");
	return true;
}
//---------------------------------------------------------------------------
void TBaseEstimation::prepareDiracPeaks(){
	//approximation of priors with Dirac peaks
	DiagonalMatrix diracPeakWidthMatrix(mySimData->numParams);
	diracPeakWidthMatrix=diracPeakWidth;
	//SigmaTheta=pow(1./numToRetain, 2./mySimData->numParams)*diracPeakWidthMatrix;
	SigmaTheta=diracPeakWidthMatrix;
	SigmaThetaInv=SigmaTheta.i();
}
//---------------------------------------------------------------------------
void TBaseEstimation::openPosteriorCharacteristicsFile(){
	std::string filename=outputPrefix+"MarginalPosteriorCharacteristics.txt";
	posteriorCharacteristicsFile.open(filename.c_str());
	posteriorMatrix->writePosteriorCharacteristicsHeader(posteriorCharacteristicsFile, computeSmoothedTruncatedPrior);
}

void TBaseEstimation::writePosteriorCharacteristics(const int & obsDataSetNum){
	//write  posterior characteristics, such as mean, mode etc.
	logfile->listFlush("Writing posterior characteristics ...");
	if(computeSmoothedTruncatedPrior) posteriorMatrix->writePosteriorCharacteristics(posteriorCharacteristicsFile, obsDataSetNum, retainedMatrix);
	else posteriorMatrix->writePosteriorCharacteristics(posteriorCharacteristicsFile, obsDataSetNum);
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
Matrix* TBaseEstimation::getPointerToMarginalPosteriorDensityMatrix(){
	return posteriorMatrix->getPointerToMarginalPosteriorDensityMatrix();
}
int TBaseEstimation::getParamColumnFromName(std::string name){
	return mySimData->getParamNumberFromName(name);
}
//---------------------------------------------------------------------------
void TBaseEstimation::writeTruncatedPrior(std::string filenameTag){
	logfile->listFlush("Writing file with marginal densities of the truncated prior ...");
	//calculate normalization area
	ColumnVector areas;
	posteriorMatrix->calcAreasParamScale(retainedMatrix, areas);
	//write to file
	std::ofstream output;
	std::string filename=outputPrefix + "TruncatedPrior";
	filename+= filenameTag;
	filename+=".txt";
	output.open(filename.c_str());
	posteriorMatrix->writeDensities(output, retainedMatrix, areas);
	output.close();
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
void TBaseEstimation::calculateSmoothedRetainedMatrix(){
	//prepare retained Matrix -> same points as for the posterior!
	retainedMatrix=Matrix(posteriorMatrix->posteriorDensityPoints, mySimData->numParams);
	retainedMatrix=0.0;
	//calculate the retained densities
	ColumnVector theta_j;
	for(int p=1; p<=mySimData->numParams;++p){
		//loop over all dirac peaks
		for(int d=1; d<=mySimData->numUsedSims;++d){
			//theta_j is a vector with the parameters of one simulation / from the uniform matrix
			theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
			for(int k=1;k<=posteriorMatrix->posteriorDensityPoints;++k){
				retainedMatrix(k,p) = retainedMatrix(k,p) + exp(-pow((*posteriorMatrix->theta)(k)-theta_j(p),2.) / (2.*diracPeakWidth));
			}
		}
	}
}
//---------------------------------------------------------------------------
double TBaseEstimation::marginalDensity(Matrix D, Matrix D_inv, ColumnVector stats){
	ColumnVector theta_j, m_j;
	double f_M = 0.0;
	D = 6.28318530717958623*D;
	double det = D.determinant(); //6.28318530717958623 is 2*Pi
	if(det<=0){
		logfile->warning("Problems calculating the marginal density: determinant is zero!");
		return 0.0;
	}

	//loop over all dirac peaks
	for(int d=1; d<=mySimData->numUsedSims;++d){
		theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		m_j = c_zero+CHat*theta_j;
		Matrix temp = -0.5*(stats-m_j).t()*D_inv*(stats-m_j);
		f_M += exp(temp(1,1));
	}
	f_M = f_M / (mySimData->numUsedSims*sqrt(det));
	f_M = f_M * ((double)mySimData->numUsedSims/(double)mySimData->numReadSims);
	return f_M;
}
//---------------------------------------------------------------------------
void TBaseEstimation::marginalDensitiesOfRetained(int calcObsPValue, Matrix D, Matrix D_inv){
	if(calcObsPValue>mySimData->numUsedSims) calcObsPValue=mySimData->numUsedSims;
	logfile->listFlush("Calculating the marginal density of ", calcObsPValue, " retained simulations ... (0%)");
	fmFromRetained.clear();

	int prog;
	int oldProg=0;
	for(int s=1; s<=calcObsPValue;++s){
		prog=100*(double)s/(double)calcObsPValue;
		if(prog>oldProg){
			oldProg=prog;
 			logfile->listOverFlush("Calculating the marginal density of ", calcObsPValue, " retained simulations ... (", prog, "%)");
		}
	   fmFromRetained.push_back(marginalDensity(D, D_inv, mySimData->statMatrix.column(s)));
	}
	logfile->overList("Calculating the marginal density of ", calcObsPValue, " retained simulations ... done!   ");
}
//---------------------------------------------------------------------------
//JOINT POSTERIOR FUNCTIONS
//---------------------------------------------------------------------------
void TBaseEstimation::performJointPosteriorEstimation(const int & obsDataSetNum){
	if(mySimData->doJointPosteriors){
		logfile->startIndent("Computing joint posterior distribution(s):");
		calculateJointPosterior();
		std::ostringstream tos; tos << obsDataSetNum;
		std::string filenameTag="_Obs"+ tos.str();
		generateSamplesFromJointPosteriors(filenameTag);
		writeJointPosteriorFiles(filenameTag);
		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
void TBaseEstimation::calculateJointPosterior(){
	//compute joint posterior densities
	//calculatePosterior has to be called first!!
	ColumnVector theta_j, v_j;
	ColumnVector* t_j_sub;
	ColumnVector* theta_sub;
	SymmetricMatrix* T_j_sub;
	ColumnVector t_j;
	int z=0;
	int prog=0;
	int oldProg=0;
	//TODO: output is confusing as the parameter numbers are different form the col numbers.
	//        -> also print col numbers and maybe param names??
	for(std::vector<TJointPosteriorStorage*>::iterator a=mySimData->jointPosteriorStorage.begin(); a!=mySimData->jointPosteriorStorage.end(); ++a, ++z){
		logfile->startIndent("Computing joint posterior densities for parameters " + (*a)->getParameterStringOfNames(", ", " and ") + ":");
		prog=0;
		oldProg=0;
		//prepare object
		if(myJointPosteriorSettings->jointPosteriorDensityPointsFromMarginals) (*a)->prepareSamplingVectors(myJointPosteriorSettings->jointPosteriorDensityPoints, posteriorMatrix->posteriorMatrix);
		else (*a)->prepareSamplingVectors(myJointPosteriorSettings->jointPosteriorDensityPoints);
		//compute densities. Use iteration offered by the object
		if(myJointPosteriorSettings->jointPosteriorDensityFromMCMC){
			logfile->list("density will be computed from MCMC run generating joint posterior samples.");
		} else {
			logfile->listFlush("Computing density at ",(*a)->getNumSamplingVecs(), " positions ... (0%)");
			T_j_sub=(*a)->get_T_j_sub(T_j);
			for(int d=1; d<=mySimData->numUsedSims;++d){
				(*a)->restart();
				theta_j=mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
				v_j=calculate_v_j(theta_j);
				double c_j=exp(exponents[d-1]);
				for(int i=0; i<(*a)->getNumSamplingVecs(); ++i){
					theta_sub=(*a)->next();
					t_j_sub=(*a)->get_t_j_sub(T_j*v_j);
					(*a)->addDensity(c_j*exp(-0.5 * (((*theta_sub)-(*t_j_sub)).t() * (*T_j_sub).i() * ((*theta_sub)-(*t_j_sub))).AsScalar()));
				}
				//show progress
				prog=100*(double) d/(double)mySimData->numUsedSims;
				if(prog>oldProg){
					oldProg=prog;
					logfile->listOverFlush("Computing density at ", (*a)->getNumSamplingVecs(), " positions ... (", prog, "%)");
				}
			}
			logfile->overList("Computing density at ", (*a)->getNumSamplingVecs(), " positions ... done!        ");
		}
		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
void TBaseEstimation::writeJointPosteriorFiles(std::string & filenameTag){
	logfile->listFlush("Writing file(s) with joint posterior estimates ...");
	int i=0;
	std::string filename;
	for(std::vector<TJointPosteriorStorage*>::iterator a=mySimData->jointPosteriorStorage.begin(); a!=mySimData->jointPosteriorStorage.end(); ++a, ++i){
	   filename=outputPrefix + "jointPosterior_";
	   filename+=(*a)->getParameterString('_');
	   filename+= filenameTag;
	   filename+=".txt";
	   (*a)->computeHDI();
	   (*a)->writeToFile(filename, posteriorMatrix->paramRanges);
	}
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
void TBaseEstimation::generateSamplesFromJointPosteriors(std::string & filenameTag){
	if(myJointPosteriorSettings->numApproxJointSamples>0) generateApproxmiateSamplesFromJointPosteriors(filenameTag);
	if(myJointPosteriorSettings->numMCMCJointSamples>0) generateMCMCSamplesFromJointPosteriors(filenameTag);
}

void TBaseEstimation::generateApproxmiateSamplesFromJointPosteriors(std::string & filenameTag){
	std::string filename;
   	for(std::vector<TJointPosteriorStorage*>::iterator a=mySimData->jointPosteriorStorage.begin(); a!=mySimData->jointPosteriorStorage.end(); ++a){
 	   filename=outputPrefix + "jointPosteriorSamples_";
   	   filename+=(*a)->getParameterString('_');
   	   filename+= filenameTag;
   	   filename+=".txt";
   	   //open file and write header...
   	   std::ofstream out(filename.c_str());
	   out << "number";
	   for(std::vector<int>::iterator p=(*a)->params.begin(); p!=(*a)->params.end(); ++p){
		   out << "\t" << mySimData->paramNames[(*p)-1];
	   }
   	   out << std::endl;
	   //get approximate samples
   	   (*a)->writeApproximatelyRandomSamples(myJointPosteriorSettings->numApproxJointSamples, &out, posteriorMatrix->paramRanges, randomGenerator);
   	   out.close();
   	}
}

void TBaseEstimation::generateMCMCSamplesFromJointPosteriors(std::string & filenameTag){
	std::string filename;
   	for(std::vector<TJointPosteriorStorage*>::iterator a=mySimData->jointPosteriorStorage.begin(); a!=mySimData->jointPosteriorStorage.end(); ++a){
 	   filename=outputPrefix + "jointPosteriorSamples_";
   	   filename+=(*a)->getParameterString('_');
   	   filename+= filenameTag;
   	   filename+=".txt";
   	   //open file and write header...
   	   std::ofstream out(filename.c_str());
	   out << "number";
	   for(std::vector<int>::iterator p=(*a)->params.begin(); p!=(*a)->params.end(); ++p){
		   out << "\t" << mySimData->paramNames[(*p)-1];
	   }
   	   out << std::endl;
   	   //run MCMC
	   runJointSampleMCMC(&out, (*a));
   	   out.close();
   	}
}
//---------------------------------------------------------------------------
ColumnVector TBaseEstimation::getSampleMCMCStart(TJointPosteriorStorage* a){
	ColumnVector oldPos;
	if(myJointPosteriorSettings->sampleMCMCStart==jointmodeStart){
		logfile->list("Starting chain at joint posterior mode");
		oldPos=a->getJointMode(); //get mode from
	} else {
		if(myJointPosteriorSettings->sampleMCMCStart==jointposteriorStart){
			logfile->list("Starting chain at random location from the approximate joint posterior");
			oldPos=a->getJointapproxRandomSample(randomGenerator);
		} else {
			oldPos.ReSize(a->params.size());
			if(myJointPosteriorSettings->sampleMCMCStart==marginalStart){
				logfile->list("Starting chain at random location from to the marginal posteriors");
				//we assume uniform distribution of posterior density points!
				for(unsigned int n=0; n<a->params.size(); ++n){
					oldPos.element(n)=posteriorMatrix->getRandomParameterAccordingToMarginal(n, randomGenerator->getRand());
				}
			} else {
				logfile->list("Starting chain at random location within the parameter range");
				for(unsigned int n=0; n<a->params.size(); ++n){
					oldPos.element(n)=randomGenerator->getRand();
				}
			}
		}
	}
	return oldPos;
}
//---------------------------------------------------------------------------
void TBaseEstimation::runJointSampleMCMC(std::ofstream* out, TJointPosteriorStorage* a){
	logfile->startIndent("Generating ", myJointPosteriorSettings->numMCMCJointSamples, " random samples from the joint posterior for parameters " + a->getParameterStringOfNames(", ", " and ") + ":");
	logfile->list("Starting an MCMC run with range proportion ", myJointPosteriorSettings->sampleMCMCRangeProp, ".");

	ColumnVector theta_j, v_j;
	ColumnVector t_j;

	SymmetricMatrix* T_j_sub;
	ColumnVector oldPos, newPos;

	//get starting position
	oldPos=getSampleMCMCStart(a);

	//get ranges
	double* ranges;
	ranges=new double[a->params.size()];
	for(unsigned int n=0; n<a->params.size(); ++n){
		ranges[n]=posteriorMatrix->getPosteriorScaledSD(n)*myJointPosteriorSettings->sampleMCMCRangeProp;
	}

	//a->fillMCMCranges(ranges, sampleMCMCRangeProp);
	//for(unsigned int n=0; n<a->params.size(); ++n){ cout << "RANGE " << n << ": " << ranges[n] << endl;}

	//prepare run
	T_j_sub=a->get_T_j_sub(T_j);
	double r;
	double oldDens, newDens;
	oldDens=getJointPosteriorDensity(T_j_sub, &oldPos, a);

	//run burnin
	logfile->listFlush("Starting burnin of ", myJointPosteriorSettings->sampleMCMCBurnin, " ...");
	for(int i=0; i<myJointPosteriorSettings->sampleMCMCBurnin; ++i){
	   	//propose new position
		newPos=oldPos;
		for(unsigned int n=0; n<a->params.size(); ++n){
			r=randomGenerator->getRand(0.0, ranges[n]);
			newPos.element(n)+=r-ranges[n]/2.0;
		}
		//get new posterior density
		newDens=getJointPosteriorDensity(T_j_sub, &newPos, a);
		//accept?
		if(randomGenerator->getRand() < (newDens/oldDens)){
			oldPos=newPos;
			oldDens=newDens;
		}
	}
	logfile->write(" done!");

	//run chain...
	clock_t starttime=clock();
	int prog, oldProg;
	oldProg=0;
	int accepted=0;
	int length=myJointPosteriorSettings->sampleMCMCSampling*(myJointPosteriorSettings->numMCMCJointSamples-1)+1;
	logfile->listFlush("Starting chain of ", length, " and sampling every ", myJointPosteriorSettings->sampleMCMCSampling, "th step ...");
	for(int i=0; i<length; ++i){
		//propose new position
		newPos=oldPos;
		for(unsigned int n=0; n<a->params.size(); ++n){
			r=randomGenerator->getRand(0.0, ranges[n]);
			newPos.element(n)+=r-ranges[n]/2.0;
		}
		//get new posterior density
		newDens=getJointPosteriorDensity(T_j_sub, &newPos, a);
		//accept?
		if(randomGenerator->getRand() < (newDens/oldDens)){
			oldPos=newPos;
			oldDens=newDens;
			++accepted;
		}

		if(i % myJointPosteriorSettings->sampleMCMCSampling == 0){
			*out << i;
			for(unsigned int n=0; n<a->params.size(); ++n){
				*out << "\t" << posteriorMatrix->toParameterScale(a->params.at(n), oldPos.element(n));
			}
			if(myJointPosteriorSettings->jointPosteriorDensityFromMCMC){
				//save to bin
				a->addSampleToDensity(1.0, &oldPos);
			}

			*out << std::endl;
			//report progress
			prog=(100*(double)i/(double)length);
			if(prog>oldProg){
				oldProg=prog;
				double acc=(double)accepted/(double)i;
				logfile->listOverFlush("Starting chain of ", length,  " and sampling every " + toString(myJointPosteriorSettings->sampleMCMCSampling) + "th step ... (" + toString(prog) + "%, acceptance " + toString(acc) + ")         ");
			}
		}
	}
	double runtime=(double) (clock()-starttime)/CLOCKS_PER_SEC/60;
	logfile->overList("Starting chain of ", length, " and sampling every " + toString(myJointPosteriorSettings->sampleMCMCSampling) + "th step ... done in ", runtime , " min!                         ");
	logfile->conclude("Acceptance rate was ", ((double)accepted / (double) length ));
	logfile->endIndent();
}
//---------------------------------------------------------------------------
double TBaseEstimation::getJointPosteriorDensity(SymmetricMatrix* T_j_sub, ColumnVector* theta_sub, TJointPosteriorStorage* a){
	double dens=0;
	ColumnVector theta_j, v_j;
	ColumnVector* t_j_sub;
	if(theta_sub->Minimum()<0 || theta_sub->Maximum()>1) return 0;
	for(int d=1; d<=mySimData->numUsedSims;++d){
		theta_j=mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		v_j=calculate_v_j(theta_j);
		double c_j=exp(exponents[d-1]);
		t_j_sub=a->get_t_j_sub(T_j*v_j);
		dens+=c_j*exp(-0.5 * (((*theta_sub)-(*t_j_sub)).t() * (*T_j_sub).i() * ((*theta_sub)-(*t_j_sub))).AsScalar());
	}
	return dens;
}


