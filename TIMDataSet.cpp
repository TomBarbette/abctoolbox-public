/*
 * TIndependentMeasuresDataSet.cpp
 *
 *  Created on: May 6, 2009
 *      Author: wegmannd
 */

/*
#include "TIMDataSet.h"
#pragma package(smart_init)

TIMDataSet::TIMDataSet(TObsData* ObsData, TSimData* SimData, int obsNum){
	  myObsData=ObsData;
	  mySimData=SimData;
	  myObsNum=obsNum;
	  myObsData->getObsValuesIntoColumnVector(myObsNum, obsValues);
}
//---------------------------------------------------------------------------
void TIMDataSet::fillRetainedMatrices(TObsData* ObsData, TSimData* SimData, bool thresholdDefined, bool retainedDefined, float threshold, int numToRetain, bool writeRetainedSimulations, my_string outputPrefix){
	//prepare distance array
	float* distances=new float[mySimData->numReadSims];
	SimData->calculateDistances(ObsData->getObservedValues(myObsNum), distances);
	//keep only retained -> check how they are defined
	if(thresholdDefined){
		if(retainedDefined) numSimsUsed=mySimData->fillRetainedFromThreshold(distances, threshold, numToRetain);
		else numSimsUsed=mySimData->fillRetainedFromThreshold(distances, threshold);
	} else {
		if(retainedDefined) numSimsUsed=mySimData->fillRetainedVectorFromNumRetained(distances, numToRetain);
		else numSimsUsed=mySimData->fillRetainedVectorKeepAll();
	}
	//check if at least 10 simulations have been retained
	if(mySimData->numUsedSims<10) throw "Given the current specified tolerance / numRetained, less than 10 (only "+ (my_string) mySimData->numUsedSims + ")simulation are retained!";
	//check if a statistic is monomorphic
	if(mySimData->someStatsAreMonomorphic()) throw "Some statistics are monomorphic among the retained simulations!";
	//write retained, if requested
	if(writeRetainedSimulations) mySimData->writeRetained(outputPrefix, distances, myObsNum);
	//store internally....
	paramMatrix=mySimData->paramMatrix;
	statMatrix=mySimData->statMatrix;
}
//---------------------------------------------------------------------------
void TIMDataSet::performLinearRegression(){
   //perform regression
   C=paramMatrix.t()*paramMatrix;
   C=C.i();
   C=paramMatrix*C;
   C=statMatrix*C;
   c_zero=C.column(1);
   CHat = C.columns(2,C.ncols());
   RHat = statMatrix.t()-paramMatrix*C.t();
   SigmaS=1.0/(numSimsUsed-mySimData->numParams)*RHat.t()*RHat;
   SigmaSInv=SigmaS.i();
}
//---------------------------------------------------------------------------
void TIMDataSet::prepareLikelihoodCalculation(Matrix& SigmaThetaInv){
	Matrix T_j=(CHat.t()*SigmaSInv*CHat+SigmaThetaInv).i();
	T_jInv=T_j.i();
	//first calculate all Qj in order to shift them -> otherwise maybe too large numbers...
	//the discrepancy is constant and therefore integrated out by normArea
	//loop over all dirac peaks
	double* exponents=new double[mySimData->numUsedSims];
	double meanExponent=0;
	c_j=new double[mySimData->numUsedSims];
	t_j=new ColumnVector[mySimData->numUsedSims];
	ColumnVector theta_j;
	ColumnVector* v_j=new ColumnVector[mySimData->numUsedSims];
	for(int d=1; d<=mySimData->numUsedSims;++d){
		//theta_j is a vector with the parameters of one simulation / from the uniform matrix
		theta_j=paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		v_j[d-1]=CHat.t()*SigmaSInv*(obsValues-c_zero)+SigmaThetaInv*theta_j;
		Matrix exponent=theta_j.t()*SigmaThetaInv*theta_j-v_j[d-1].t()*T_j*v_j[d-1];
		exponents[d-1]=-0.5*exponent(1,1);
		meanExponent+=exponents[d-1];
	}
	//shift the Qj and calculate the c_j
	meanExponent=meanExponent/numSimsUsed;
	for(int d=0; d<numSimsUsed;++d){
		c_j[d]=exp(exponents[d]-meanExponent);
		theta_j=paramMatrix.submatrix(d+1,d+1,2,mySimData->numParams+1).as_column();
		t_j[d]=T_j*v_j[d];
	}

	delete[] exponents;
	delete[] v_j;

	//get mean exponent from retained sims in order to level the exponents. Otherwise we might get 0 or Inv due to very large /small values.
	meanExpLikelihood=0;
	for(int d=1; d<=mySimData->numUsedSims;++d){
		theta_j=paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		Matrix exponent=-0.5*(theta_j-t_j[d-1]).t()*T_jInv*(theta_j-t_j[d-1]);
		meanExpLikelihood+=exponent(1,1);
	}
	meanExpLikelihood=meanExpLikelihood/mySimData->numUsedSims;
}

//---------------------------------------------------------------------------
double TIMDataSet::getLikelihood(ColumnVector& theta){
	//since the sample was uniform, just compute the posterior density according to equation 10 in Leuenberger and Wegmann
	double posteriorDensity=0;
	for(int d=0; d<numSimsUsed;++d){
		Matrix exponent=-0.5*(theta-t_j[d]).t()*T_jInv*(theta-t_j[d]);
		posteriorDensity+=c_j[d]*exp(exponent(1,1)-meanExpLikelihood);
	}
	return posteriorDensity;
}
*/


