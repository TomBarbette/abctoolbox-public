//---------------------------------------------------------------------------

#ifndef TBaseEstimationH
#define TBaseEstimationH
#include "TObsData.h"
#include "TSimData.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include <stdlib.h>
#include <time.h>
#include "TLog.h"
#include <algorithm>
#include "stringFunctions.h"
#include "matrixFunctions.h"
//---------------------------------------------------------------------------
enum MCMCStartType { randomStart, jointmodeStart, jointposteriorStart, marginalStart };

class TJointPosteriorSettings{
public:
	bool doJointPosterior;
	int jointPosteriorDensityPoints;
	bool jointPosteriorDensityPointsFromMarginals;
	bool jointPosteriorDensityFromMCMC;
	vector<std::string> jointParameters;

	//joint posterior samples
	long numApproxJointSamples;
	long numMCMCJointSamples;
	MCMCStartType sampleMCMCStart;
	double sampleMCMCRangeProp;
	int sampleMCMCBurnin;
	int sampleMCMCSampling;

	TJointPosteriorSettings(){
		doJointPosterior=false;
		jointPosteriorDensityPoints=0;
		jointPosteriorDensityPointsFromMarginals=false;
		jointPosteriorDensityFromMCMC=false;
		numApproxJointSamples=0;
		numMCMCJointSamples=0;
		sampleMCMCStart=randomStart;
		sampleMCMCRangeProp=0.0;
		sampleMCMCBurnin=0;
		sampleMCMCSampling=0;
	};
	TJointPosteriorSettings(TParameters* parameters, TLog* LogFile);
};

//---------------------------------------------------------------------------
class TPosteriorMatrix{
public:
	//pointers
	TParamRanges* paramRanges;

	//estimation
	double step; //step between posterior density points
	int posteriorDensityPoints;
	int obsDataSetNum;

	//storage
	ColumnVector marginalAreasParamScale, marginalAreas;
	Matrix posteriorMatrix;
	ColumnVector* theta;

	TPosteriorMatrix(ColumnVector* Theta, TParamRanges* ParamRanges);
	~TPosteriorMatrix(){};
	void initToZero(){posteriorMatrix=0;};
	void setPosteriorMatrixDensity(const int & pos, const int & param, const double & value){posteriorMatrix(pos,param)=value;};
	void addToPosteriorMatrixDensity(const int & pos, const int & param, const double & value){posteriorMatrix(pos,param)=posteriorMatrix(pos,param)+value;};
	void calcMarginalAreas();
	double getMarginalArea(const int & param){return marginalAreasParamScale(param);};
	void calcAreasParamScale(const Matrix & densities, ColumnVector & areas);
	double calcAreaParamScale(const Matrix & densities, const int & param);
	double calcArea(const ColumnVector densities);
	double calcAreaByReference(const ColumnVector & densities);
	void writePosteriorFiles(ofstream & output);
	void writeDensities(ofstream & output, Matrix & densities, ColumnVector & areas);
	double toParameterScale(const int & param, const double & pos);
	double toInternalScale(const int & param, const double & val);
	double thetaParameterScale(const int & param, const int & k);
	//double theatParameterScale(const int & param, const double & pos);
	double getPositionOfMode(const int & param);
	double getPosteriorMean(const int & param);
	double getPosteriorquantile(const int & param, double q);
	double getPosInCumulative(const int & param, double & val);
	void writeQuantile(const int & param, double quantile, ofstream & output);
	void getPosteriorHDIScaled(const int & param, const double & q, double & lower, double & upper);
	void getPosteriorHDI(const int & param, const double & q, double & lower, double & upper);
	void writeHDI(const int & param, const double & HDI, ofstream & output);
	double getPosInPosteriorHDI(int param, double trueValue, int steps);
	void writePosteriorCharacteristicsHeader(ofstream & posteriorCharacteristicsFile, bool addL1PriorPosterior=false);
	void writePosteriorCharacteristics(ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum);
	void writePosteriorCharacteristics(ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum, Matrix & priorDensities);
	void writePosteriorCharacteristicsOneParam(ofstream & posteriorCharacteristicsFile, const int & param);
	void writeValidationHeader(std::ofstream & output, std::string & paramName);
	void writeValidationStats(ofstream & output, const int & param, double val);
	double getLOnedistwithPosterior(const int & param, Matrix & densities);
	double getRandomParameterAccordingToMarginal(const int & param, double randomVal);
	double getPosteriorScaledSD(const int & param);
	void setParameterRange(const std::string & param, pair<double,double> minmax);
	Matrix* getPointerToMarginalPosteriorDensityMatrix(){return &posteriorMatrix;};
};

//---------------------------------------------------------------------------
class TBaseEstimation {
   public:
	  //pointers
	  TRandomGenerator* randomGenerator;
	  TSimData* mySimData;
	  TJointPosteriorSettings* myJointPosteriorSettings;
	  TPosteriorMatrix* posteriorMatrix;
	  TLog* logfile;

	  //estimation
	  double diracPeakWidth;
	  double marDens;
	  double marDensPValue;
	  double tukeyDepth;
	  double tukeyDepthPValue;
	  double posteriorProbability;

	  //storage
	  ColumnVector obsValues;
	  Matrix C;
	  ColumnVector c_zero;
	  Matrix CHat;
	  Matrix RHat;
	  SymmetricMatrix SigmaS;
	  Matrix SigmaSInv;
	  Matrix SigmaTheta;
	  Matrix SigmaThetaInv;
	  Matrix retainedMatrix;
	  SymmetricMatrix T_j;
	  double* exponents;
	  bool exponentsInitialized;
	  int exponentsSize;
	  double meanExponent;
	  vector<double> fmFromRetained;

	  //output
	  ofstream trueParamCharacteristicsFile;
	  ofstream propCommonSurfacePriorPosterior;
	  ofstream trueParamValidationFile;
	  ofstream posteriorCharacteristicsFile;
	  std::string outputPrefix;

	  bool trueParamValidationFileOpen;
	  bool computeSmoothedTruncatedPrior;

	  TBaseEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* LogFile);
	  virtual ~TBaseEstimation(){
		  if(exponentsInitialized) delete[] exponents;
		  if(trueParamValidationFileOpen) trueParamValidationFile.close();
		  delete posteriorMatrix;
	  };
	  double* getPointerToStatValues(long & simNum){return mySimData->getPointerToStatValues(simNum);};
	  void preparePrior(bool verbose=false);
	  virtual bool calculatePosterior(bool verbose=false);
	  virtual bool calculate_T_j(SymmetricMatrix & m);
	  virtual ColumnVector calculate_v_j(ColumnVector theta_j);
	  bool performLinearRegression(bool verbose=false);
	  void prepareDiracPeaks();
	  void writeTruncatedPrior(std::string filenameTag="");
	  Matrix* getPointerToMarginalPosteriorDensityMatrix();
	  int getParamColumnFromName(std::string name);
	  void calculateSmoothedRetainedMatrix();
	  double marginalDensity(Matrix D, Matrix D_inv, ColumnVector stats);
	  void marginalDensitiesOfRetained(int calcObsPValue, Matrix D, Matrix D_inv);
	  void openPosteriorCharacteristicsFile();
	  void writePosteriorCharacteristics(const int & obsDataSetNum);
	  //joint posterior / samples from joint posteriors
	  void performJointPosteriorEstimation(const int & obsDataSetNum);
	  void calculateJointPosterior();
	  void writeJointPosteriorFiles(std::string & filenameTag);
	  void generateSamplesFromJointPosteriors(std::string & filenameTag);
	  void generateApproxmiateSamplesFromJointPosteriors(std::string & filenameTag);
	  void generateMCMCSamplesFromJointPosteriors(std::string & filenameTag);
	  ColumnVector getSampleMCMCStart(TJointPosteriorStorage* a);
	  void runJointSampleMCMC(ofstream* out, TJointPosteriorStorage* a);
	  double getJointPosteriorDensity(SymmetricMatrix* T_j_sub, ColumnVector* theta_sub, TJointPosteriorStorage* a);

};


#endif
