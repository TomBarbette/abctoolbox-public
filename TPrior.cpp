//---------------------------------------------------------------------------
#include "TPrior.h"
//------------------------------------------------------------------------------
 //TPrior
TPrior::TPrior(TRandomGenerator* RandomGenerator, std::string Name){
	randomGenerator=RandomGenerator;
	name=Name;
	curValue=0;
	oldValue=0;
	isInt = false;
	output = false;
	changed = true;
	isConstant = false;
}
void TPrior::initialize(std::vector<std::string> & line){
	//check if it is int or not in first column
	isInt=stringToInt(line[0]);
	if(isInt!=0 && isInt !=1) throw "Unknown isInt value in the .est file for parameter '" + name + "'!";

	//check if prior will be output in last column
	if(line[line.size()-1]=="output") output=true;
	else {
	   if(line[line.size()-1]!="hide") throw "output or hide tag is missing in the .est file for parameter '" + name + "'!";
	   output=false;
	}
}
void TPrior::makeCurValueInt(){
	curValue = round(curValue);
}
void TPrior::writeValue(const double& value, std::ofstream& file){
   //attention about scientific notations (integers can't read them)
   if(isInt) file << (long) value;
   else file << value;
}
void TPrior::writeCurValue(std::ofstream& file){
   writeValue(curValue, file);
}
/*
void TPrior::writeHyperPriorGamma(const double& arg, std::ofstream& file){
   writeValue(randomGenerator->getGammaRand(arg, arg/curValue), file);
}
void TPrior::writeHyperPriorBeta(std::ofstream& file){
	if(curValue<0.001) writeValue(0.0, file);
	else {
	   if(curValue>1) throw "Hyperprior '"+name+"': mean of the beta distribution > 1!";
	   double a = 0.5 + 199 * curValue;
	   writeValue(randomGenerator->getBetaRandom(a, a*(1-curValue)/curValue), file);
	}
}
void TPrior::writeHyperPriorNormal(const double& stdev, std::ofstream& file){
   writeValue(randomGenerator->getNormalRandom(curValue, stdev), file);
}
void TPrior::writeHyperPriorNormalPositive(const double& stdev, std::ofstream& file){
	float val=randomGenerator->getNormalRandom(curValue, stdev);
	while(val<=0) val=randomGenerator->getNormalRandom(curValue, stdev);
    writeValue(val, file);
}
void TPrior::writeHyperPriorLognormal(const double& stdev, std::ofstream& file){
	   if(stdev<=0.) throw "Hyperprior '"+name+"': stdev of the log normal distribution <=0!";
	   double mean=log(curValue) - 0.5 * log(1 + ( (stdev*stdev)/(curValue*curValue) ));
	   double sigma=sqrt( log( ((stdev*stdev)/(curValue*curValue)) +1 ) );
	   writeValue(exp(randomGenerator->getNormalRandom(mean, sigma)), file);
}
void TPrior::writeHyperPriorLognormalParametersInLog10Scale(const double& stdev, std::ofstream& file){ //base 10!!!!
	if(stdev<=0.) throw "Hyperprior '"+name+"': stdev of the log normal distribution with parameters in log scale <=0!";
	writeValue(pow((double) 10.0, randomGenerator->getNormalRandom(curValue, stdev)), file);
}
*/
void TPrior::saveOldValue(){
   oldValue = curValue;
   changed = false;
}
void TPrior::resetOldValue(){
	curValue = oldValue;
	changed = false;
}
void TPrior::setCurValue(const double & newValue){
   curValue = newValue;
   if (isInt) makeCurValueInt();
   oldValue = curValue;
   changed = true;
}

//------------------------------------------------------------------------------
//TCombinedPrior
TCombinedPrior::TCombinedPrior(TRandomGenerator* RandomGenerator, std::string Name, std::string Equation, std::vector<std::string> & line, TPriorVector* priorVec):TPrior(RandomGenerator, Name){
	TPrior::initialize(line);
	//replace all - by (-1)*
	Equation=stringReplace('-', "+(-1)*", Equation);
	equation = new TEquation_base(Equation, priorVec);
	curValue = equation->getValue();
	isConstant = equation->isConstant;
	//equation->what();
}
//------------------------------------------------------------------------------
//TSimplePiror
TSimplePrior::TSimplePrior(TRandomGenerator* RandomGenerator, std::string & Name, std::vector<std::string> & line):TPrior(RandomGenerator, Name){
   TPrior::initialize(line);
   lowerLimit=0;
   upperLimit=0;
   mcmcStep=-1;
   isHyperPrior = false;
}
TSimplePrior::TSimplePrior(TRandomGenerator* RandomGenerator, std::string & Name):TPrior(RandomGenerator, Name){
	lowerLimit = 0;
	upperLimit = 0;
	mcmcStep = -1;
	isHyperPrior = false;
}
void TSimplePrior::setLimits(double min, double max){
	lowerLimit = min;
	upperLimit = max;
	if(isInt){
	   lowerLimit-=0.5;
	   upperLimit+=0.5;
	}
	if(lowerLimit>upperLimit) throw "Parameter '" + name + "' initialized with min > max!";
}
void TSimplePrior::setMCMCStep(double McmcStep){
	//if(McmcStep > (upperLimit - lowerLimit)) throw "MCMC step is larger than the range (max-min) of parameter '" + name + "'";
	if(McmcStep > 0.25 * (upperLimit - lowerLimit)) mcmcStep = 0.25 * (upperLimit - lowerLimit);
	mcmcStep = McmcStep;
}
void TSimplePrior::setMCMCStepRelativeToRange(double McmcStep){
	mcmcStep = McmcStep * (upperLimit - lowerLimit);
	if(mcmcStep > 0.25 * (upperLimit - lowerLimit)) mcmcStep = 0.25 * (upperLimit - lowerLimit);
}
void TSimplePrior::changeCurrentValue(){
	changed = true;
	_changeCurrentValue();
	if(isInt) makeCurValueInt();
	if(isHyperPrior){
		for(std::vector<THierarchicalPrior*>::iterator it=hypoPriors.begin(); it!=hypoPriors.end(); ++it)
			(*it)->hyperPriorChanged();
	}
};
double TSimplePrior::getLogPriorDensity(){
   return getLogPriorDensityFromValue(curValue);
}
double TSimplePrior::getLogPriorDensityFromValue(const double& value){
   return 0.0;
}
void TSimplePrior::getNewValuesMcmc(){
   curValue += randomGenerator->getNormalRandom(0, mcmcStep);

   //reflect, if necessary
   if(curValue < lowerLimit) curValue=lowerLimit+(lowerLimit-curValue);
   if(curValue > upperLimit) curValue=upperLimit+(upperLimit-curValue);
   changed = true;
}
//------------------------------------------------------------------------------
//uniform prior
TUniformPrior::TUniformPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=6) throw "Wrong number of entries for uniform prior on '" + Name + "'!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	logDensity = - log((upperLimit-lowerLimit));
	changeCurrentValue();
}
//standard function with no arguments
void TUniformPrior::_changeCurrentValue(){
   curValue=randomGenerator->getRand(lowerLimit, upperLimit);
}
double TUniformPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;
   return logDensity;
}
//------------------------------------------------------------------------------
//fixed prior (always the same value)
TFixedPrior::TFixedPrior(TRandomGenerator* RandomGenerator, std::string & Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=5) throw "Wrong number of entries for fixed prior on '" + Name + "'!";
	//value is in 4th column
	double val = stringToDoubleCheck(line[3]);
	setLimits(val, val);
	curValue=val;
	if (isInt) makeCurValueInt();
	isConstant = true;
}

TFixedPrior::TFixedPrior(TRandomGenerator* RandomGenerator, std::string Name, double val):TSimplePrior(RandomGenerator, Name){
	setLimits(val, val);
	curValue=val;
}
//standard function with no arguments
double TFixedPrior::getLogPriorDensity(){
	return 0.0;
}
double TFixedPrior::getLogPriorDensityFromValue(const double& value){
   if(value == curValue) return 0.0;
   return log_zero_density;
}
//------------------------------------------------------------------------------
//loguniform prior
TLogUniformPrior::TLogUniformPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=6) throw "Wrong number of entries for log-uniform prior on '" + Name + "'!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	if( lowerLimit < 1e-15) throw "Can not initialize log-uniform distribution with lower limit <= 0.0!";

	logLowerLimit = log(lowerLimit);
	logUpperLimit = log(upperLimit);
	logLimitDiff = logUpperLimit - logLowerLimit;
	changeCurrentValue();
}
void TLogUniformPrior::_changeCurrentValue(){
   curValue=exp(randomGenerator->getRand(logLowerLimit, logUpperLimit));
}
double TLogUniformPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 0.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;
    return log(value) - logLimitDiff;
}
//------------------------------------------------------------------------------
//normal prior
TNormalPrior::TNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=8) throw "Wrong number of entries for normal prior on '" + Name + "'!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize
	//mean and sigma are in 6th and 7th column
	mean = stringToDoubleCheck(line[5]);
	sigma = stringToDoubleCheck(line[6]);
	if(sigma<=0.) throw "Normal prior on '" + name + "' initialized with stdev <= 0!";

	//initialize calc of density
	logSigmaSqrtTwoPi = log(sigma  * sqrt(2.0 * M_PI));
	twoSigmaSquared = 2.0 * sigma * sigma;
	logAreaAfterTruncation = log(randomGenerator->normalCumulativeDistributionFunction(upperLimit, mean, sigma) - randomGenerator->normalCumulativeDistributionFunction(lowerLimit, mean, sigma));

	//get first value
	changeCurrentValue();
}
void TNormalPrior::_changeCurrentValue(){
   do{
	  curValue=randomGenerator->getNormalRandom(mean, sigma);
   }while(curValue<lowerLimit || curValue>upperLimit);
}
double TNormalPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 1.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;
   //standardize value
   double y = ( value - mean );
   //now calculate density according to standard formulae, where 0.3989...=1/sqrt(2*PI)
   //also take care of truncation
   return -logSigmaSqrtTwoPi - y * y / twoSigmaSquared - logAreaAfterTruncation;
}
//------------------------------------------------------------------------------
//lognormal prior
TLogNormalPrior::TLogNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=8) throw "Wrong number of entries for log-normal prior on '" + Name + "'!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	if(lowerLimit<0.0) throw "Log normal prior on '" + name + "' initialized with min < 0!";

	//mean and sigma are in 6th and 7th column
	mean = stringToDoubleCheck(line[5]);
	sigma = stringToDoubleCheck(line[6]);
	if(sigma<=0.) throw "Log-normal prior on '" + name + "' initialized with stdev <= 0!";

	meanInLog = 2.0 * log(mean) - 0.5 * log(sigma*sigma + mean*mean);
	sigmaInLog = sqrt( log(1.0 + sigma*sigma / mean / mean));

	//initialize variables for density calculation
	logAreaAfterTruncation = log(randomGenerator->normalCumulativeDistributionFunction(upperLimit, meanInLog, sigmaInLog) - randomGenerator->normalCumulativeDistributionFunction(lowerLimit, meanInLog, sigmaInLog));
	logSqrtTwoPiSigma = log(sqrt(2.0 * M_PI) * sigmaInLog);
	twoSigmaSquared = 2.0 * sigmaInLog * sigmaInLog;

	//get first value
	changeCurrentValue();
}

void TLogNormalPrior::_changeCurrentValue(){
   do{
	  curValue = exp(randomGenerator->getNormalRandom(meanInLog, sigmaInLog));
   }while(curValue<lowerLimit || curValue>upperLimit);
}

double TLogNormalPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 1.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;
	double y = log(value) - meanInLog;
	return (-log(value) - logSqrtTwoPiSigma - y*y/twoSigmaSquared) - logAreaAfterTruncation;
}
//------------------------------------------------------------------------------
//gamma prior
//constructor for child class
TGammaPrior::TGammaPrior(TRandomGenerator* RandomGenerator, std::string Name):TSimplePrior(RandomGenerator, Name){
	alpha = 0.0;
	beta = 0.0;
	alphaLogBetaMinusLogGamma = 0.0;
	alphaMinusOne = 0.0;
	logAreaAfterTruncation = 0.0;
}

//constructor to be used for this class
TGammaPrior::TGammaPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=8) throw "Wrong number of entries for gamma prior on '" + Name + "'!";

	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	if(lowerLimit < 0.0) throw "Gamma distribution only defined >= 0.0. Change lower limit!";

	//shape and scale are 6th and 7th
	alpha = stringToDoubleCheck(line[5]);
	if(alpha <= 0.0) throw "Problem initializing gamma prior on '" + name + "': alpha (shape)  <= 0.0!";
	beta = stringToDoubleCheck(line[6]);
	if(beta <= 0.0) throw "Problem initializing gamma prior on '" + name + "': beta (rate) <= 0.0!";

	//initialize variables for density calculation
	initializeDensityCalculation();
}

void TGammaPrior::initializeDensityCalculation(){
	//initialize variables for density calculation
	alphaLogBetaMinusLogGamma = alpha * log(beta) - randomGenerator->gammaln(alpha);
	alphaMinusOne = alpha - 1.0;

	logAreaAfterTruncation = (randomGenerator->gammaCumulativeDistributionFunction(upperLimit, alpha, beta) - randomGenerator->gammaCumulativeDistributionFunction(lowerLimit, alpha, beta));

	//get first value
	changeCurrentValue();
}

void TGammaPrior::_changeCurrentValue(){
   do{
	  curValue = randomGenerator->getGammaRand(alpha, beta);
   }while(curValue<lowerLimit || curValue>upperLimit);
}
double TGammaPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;
   return alphaLogBetaMinusLogGamma + alphaMinusOne*log(value) - beta*value - logAreaAfterTruncation;
}
//------------------------------------------------------------------------------
//gamma prior
TGammaMeanPrior::TGammaMeanPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TGammaPrior(RandomGenerator, Name){
	if(line.size()!=8) throw "Wrong number of entries for gamma prior on '" + Name + "'!";
	initialize(line);

	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	if(lowerLimit < 0.0) throw "Gamma distribution only defined >= 0.0. Change lower limit!";

	//shape and scale are 6th and 7th
	alpha = stringToDoubleCheck(line[5]);
	if(alpha <= 0.0) throw "Problem initializing gamma (mean) prior on '" + name + "': alpha (shape)  <= 0.0!";
	mean = stringToDoubleCheck(line[6]);
	if(mean <= 0.0) throw "Problem initializing gamma (mean) prior on '" + name + "': mean <= 0.0!";
	beta = alpha / mean;

	//initialize variables for density calculation
	initializeDensityCalculation();
}
//------------------------------------------------------------------------------
//exponential prior
TExponentialPrior::TExponentialPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=7) throw "Wrong number of entries for exponential prior on '" + Name + "'!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));
	if(lowerLimit < 0.0) throw "Exponential distribution only defined >= 0.0. Change lower limit!";

	//lambda is 6th
	lambda = stringToDoubleCheck(line[5]);
	if(lambda <= 0.0) throw "Can not initialize exponential distribution with rate (lambda) parameter " + toString(lambda) + ", shape must be > 0.0!";

	//initialize variables for density calculation
	logLambda = log(lambda);
	minInCumul = exp(-lowerLimit * lambda);
	maxInCumul = exp(-upperLimit * lambda);

	logAreaAfterTruncation = log(randomGenerator->exponentialCumulativeFunction(upperLimit, lambda) - randomGenerator->exponentialCumulativeFunction(lowerLimit, lambda));

	//get first value
	changeCurrentValue();
}

void TExponentialPrior::_changeCurrentValue(){
	curValue = -log(randomGenerator->getRand(minInCumul, maxInCumul)) / lambda;
}
double TExponentialPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;

   return logLambda - lambda * value - logAreaAfterTruncation;
}

//------------------------------------------------------------------------------
//Generalized Pareto prior
TGeneralizedParetoPrior::TGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=9) throw "Wrong number of entries for generalized pareto prior on '" + Name + "': need 9 not " +  toString(line.size()) + "!";
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//other parameters
	locationMu = stringToDoubleCheck(line[5]);
	scaleSigma = stringToDoubleCheck(line[6]);
	shapeXi = stringToDoubleCheck(line[7]);

	//check parameters
	if(lowerLimit < locationMu) throw "Can not initialize generalized Pareto distribution with mu (location) = " + toString(locationMu) + ", mu must be >= lower limit (" + toString(lowerLimit) + ")!";
	if(locationMu < 0.0 && upperLimit >= (shapeXi - scaleSigma / locationMu)) throw "Can not initialize generalized Pareto distribution with upper limit " + toString(upperLimit) + ", upper limit must be < (threshold - scale / shape) if shape < 0!";


	//initialize variables for density calculation
	logSigma = log(scaleSigma);

	logAreaAfterTruncation = log(randomGenerator->GeneralizedParetoCumulativeFunction(upperLimit, locationMu, scaleSigma, shapeXi) - randomGenerator->GeneralizedParetoCumulativeFunction(lowerLimit, locationMu, scaleSigma, shapeXi));

	//get first value
	changeCurrentValue();
}

void TGeneralizedParetoPrior::_changeCurrentValue(){
   do{
	  curValue = randomGenerator->getGeneralizedParetoRand(locationMu, scaleSigma, shapeXi);
   }while(curValue<lowerLimit || curValue>upperLimit);
}
double TGeneralizedParetoPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;

   if(shapeXi == 0.0){
	   return -logSigma - (value - locationMu) / scaleSigma - logAreaAfterTruncation;
	} else {
	   return -logSigma + (-1.0-1.0/shapeXi) * log(1.0 + shapeXi * (value - locationMu )/scaleSigma) - logAreaAfterTruncation;
	}

}

//------------------------------------------------------------------------------
//Mirrored Generalized Pareto Distribution MGPD prior
/*
 * This is a self-designed distribution consisting of two Generalized Pareto distributions GPD. The idea is to cover
 * the negative range by having MGPD = (GPD(-x + mu, 0, sigma, xiLow) + GPD(x - mu, 0, sigma, xiHigh)) / 2.
 * Since GPD(x<0, 0, sigma, xi) = 0, the first is defined for all x <=0 and the second for all x >= 0.
 * We assume sigam to be teh same, but allow for two different shape parameters xi on botth sides.
 * The location mu is defined as the location of the peak.
 */
TMirroredGeneralizedParetoPrior::TMirroredGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	if(line.size()!=11) throw "Wrong number of entries for generalized pareto prior on '" + Name + "': need 11 not " +  toString(line.size()) + "!";
	//the order is: min max locationMu scaleSigma shapXiLow shapeXiHigh pAboveMu
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//other parameters
	locationMu = stringToDoubleCheck(line[5]);
	scaleSigma = stringToDoubleCheck(line[6]);
	shapeXiLow = stringToDoubleCheck(line[7]);
	shapeXiHigh = stringToDoubleCheck(line[8]);
	probAboveMu = stringToDoubleCheck(line[9]);

	//check parameters
	if(scaleSigma <= 0.0) throw "Can not initialize Mirrored Generalized Pareto distribution with sigma (scale) = " + toString(scaleSigma) + ", sigma must be > 0.0!";
	if(shapeXiLow <= 0.0) throw "Can not initialize Mirrored Generalized Pareto distribution with xi (shape) = " + toString(shapeXiLow) + ", xi must be > 0.0!";
	if(shapeXiHigh <= 0.0) throw "Can not initialize Mirrored Generalized Pareto distribution with xi (shape) = " + toString(shapeXiHigh) + ", xi must be > 0.0!";
	if(probAboveMu == 0.0 && lowerLimit > locationMu) throw "Can not initialize Mirrored Generalized Pareto distribution with pAboveMu = 0.0 if lower limit > mu (location)!";
	if(probAboveMu == 1.0 && upperLimit < locationMu) throw "Can not initialize Mirrored Generalized Pareto distribution with pAboveMu = 1.0 if upper limit < mu (location)!";

	//assess symmetry: how much of the dist is > mu, how much < mu?
	zero = 0.0;
	double areaAboveMu, areaBelowMu;
	if(lowerLimit > locationMu || probAboveMu == 1.0){
		logAreaAfterTruncationBelowMu = -1.0; // not used!
		areaAboveMu = randomGenerator->GeneralizedParetoCumulativeFunction(upperLimit - locationMu, zero, scaleSigma, shapeXiHigh);
		areaAboveMu -= randomGenerator->GeneralizedParetoCumulativeFunction(lowerLimit - locationMu, zero, scaleSigma, shapeXiHigh);
		logAreaAfterTruncationAboveMu = log(areaAboveMu);
		probAboveMu = 1.0;
	} else if(upperLimit < locationMu || probAboveMu == 0.0){
		logAreaAfterTruncationAboveMu = -1.0; // not used!
		areaBelowMu = randomGenerator->GeneralizedParetoCumulativeFunction(locationMu-lowerLimit, zero, scaleSigma, shapeXiLow);
		areaBelowMu -= randomGenerator->GeneralizedParetoCumulativeFunction(locationMu - upperLimit, zero, scaleSigma, shapeXiLow);
		logAreaAfterTruncationBelowMu = log(areaBelowMu);
		probAboveMu = 0.0;
	} else {
		logAreaAfterTruncationBelowMu = log(randomGenerator->GeneralizedParetoCumulativeFunction(locationMu-lowerLimit, zero, scaleSigma, shapeXiLow) / (1.0 - probAboveMu));
		logAreaAfterTruncationAboveMu = log(randomGenerator->GeneralizedParetoCumulativeFunction(upperLimit - locationMu, zero, scaleSigma, shapeXiHigh) / probAboveMu);
	}

	logSigma = log(scaleSigma);

	//get first value
	changeCurrentValue();
}

void TMirroredGeneralizedParetoPrior::_changeCurrentValue(){
   if(randomGenerator->getRand() < probAboveMu){
	  //sample from positive (above mu) range
	  do {
		  curValue = randomGenerator->getGeneralizedParetoRand(zero, scaleSigma, shapeXiHigh) + locationMu;
	  } while(curValue<lowerLimit || curValue > upperLimit);
   } else {
	   //sample from negative (below mu) range
	   do {
		   curValue = - randomGenerator->getGeneralizedParetoRand(zero, scaleSigma, shapeXiLow) + locationMu;
	   } while(curValue<lowerLimit || curValue > upperLimit);
   }
}

double TMirroredGeneralizedParetoPrior::getLogPriorDensityFromValue(const double& value){
	//Note: xi > 0.0 in all cases!
   if(value>upperLimit || value<lowerLimit) return log_zero_density;
   if(probAboveMu == 1.0 && value < locationMu) return log_zero_density;
   if(probAboveMu == 0.0 && value > locationMu) return log_zero_density;

   double tmp = value - locationMu;
   if(tmp < zero)
	   return -logSigma + (-1.0-1.0/shapeXiLow) * log(1.0 + shapeXiLow * (-tmp)/scaleSigma) - logAreaAfterTruncationBelowMu;
   else if(tmp > zero)
	   return -logSigma + (-1.0-1.0/shapeXiHigh) * log(1.0 + shapeXiHigh * (tmp)/scaleSigma) - logAreaAfterTruncationAboveMu;
   //in case value == mu, return average
   else return -logSigma + ((-1.0-1.0/shapeXiLow) * log(1.0 + shapeXiLow * (-tmp)/scaleSigma) - logAreaAfterTruncationBelowMu + (-1.0-1.0/shapeXiHigh) * log(1.0 + shapeXiHigh * (tmp)/scaleSigma) - logAreaAfterTruncationAboveMu) / 2.0;
}

//------------------------------------------------------------------------------
//THierarchicalPrior
//------------------------------------------------------------------------------

THierarchicalPrior::THierarchicalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line):TSimplePrior(RandomGenerator, Name, line){
	updateTruncatedArea = true;
	logAreaAfterTruncation = 0.0;
}

//--------------------------------
//Normal
THierarchicalNormalPrior::THierarchicalNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior):THierarchicalPrior(RandomGenerator, Name, line){
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize mean
	bool isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[5]){
			isPrior = true;
			mean = it->second;
			it->second->registerHypoPrior(this);
		}
	}
	if(!isPrior){
		mean = new TFixedPrior(RandomGenerator, "Mean", stringToDoubleCheck(line[5]));
		fixedPriors.push_back(mean);
	}

	//initialize sigma
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[6]){
			isPrior = true;
			sigma = it->second;
			it->second->registerHypoPrior(this);
			if(sigma->lowerLimit <= 0.) throw "Problem initializing hierarchical normal prior on '" + name + "': lower limit of hyper prior on stdev '" + sigma->name + "' <= 0!";
		}
	}
	if(!isPrior){
		sigma = new TFixedPrior(RandomGenerator, "Sigma", stringToDoubleCheck(line[6]));
		fixedPriors.push_back(sigma);
		if(sigma->curValue <= 0.) throw "Hierarchical normal prior on '" + name + "' initialized with stdev <= 0!";
	}

	//initialize variables for density calculation
	logSqrtTwoPi = log(sqrt(2.0 * M_PI));

	//get first value
	changeCurrentValue();
}

void THierarchicalNormalPrior::_changeCurrentValue(){
  if(sigma->curValue <= 0.) throw "Stdev <=0 in normal prior '" + name + "'!";
   do{
	  curValue = randomGenerator->getNormalRandom(mean->curValue, sigma->curValue);
   }while(curValue<lowerLimit || curValue>upperLimit);
   if (isInt) makeCurValueInt();
   changed = true;
}
void THierarchicalNormalPrior::calculateTruncatedArea(){
	if(updateTruncatedArea){
		logAreaAfterTruncation = randomGenerator->normalCumulativeDistributionFunction(upperLimit, mean->curValue, sigma->curValue);
		logAreaAfterTruncation -= randomGenerator->normalCumulativeDistributionFunction(lowerLimit, mean->curValue, sigma->curValue);
		logAreaAfterTruncation = log(logAreaAfterTruncation);
		updateTruncatedArea = false;
	}
}
double THierarchicalNormalPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 0.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;
   //standardize value
   double y = ( value - mean->curValue ) / sigma->curValue;
   //now calculate density according to standard formulae
   y = -logSqrtTwoPi - log(sigma->curValue) - y * y / 2.0;

   //take care of truncation, as this may change during MCMC
   calculateTruncatedArea();
   return y - logAreaAfterTruncation;
}
//--------------------------------
//Gamma (alpha and beta)
THierarchicalGammaPrior::THierarchicalGammaPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior):THierarchicalPrior(RandomGenerator, Name, line){
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize alpha
	bool isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[5]){
			isPrior = true;
			alpha = it->second;
			it->second->registerHypoPrior(this);
			if(alpha->lowerLimit <= 0.) throw "Problem initializing hierarchical gamma prior on '" + name + "': lower limit of hyper prior on alpha '" + alpha->name + "' <= 0.0!";
		}
	}
	if(!isPrior){
		alpha = new TFixedPrior(RandomGenerator, "alpha", stringToDoubleCheck(line[5]));
		fixedPriors.push_back(alpha);
		if(alpha->curValue <= 0.0) throw "Problem initializing hierarchical gamma prior on '" + name + "': alpha  <= 0.0!";
	}

	//initialize beta
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[6]){
			isPrior = true;
			beta = it->second;
			it->second->registerHypoPrior(this);
			if(beta->lowerLimit <= 0.) throw "Problem initializing hierarchical gamma prior on '" + name + "': lower limit of hyper prior on beta '" + beta->name + "' <= 0.0!";
		}
	}
	if(!isPrior){
		beta = new TFixedPrior(RandomGenerator, "beta", stringToDoubleCheck(line[6]));
		fixedPriors.push_back(beta);
		if(beta->curValue <= 0.0) throw "Problem initializing hierarchical gamma prior on '" + name + "': beta  <= 0.0!";
	}

	//get first value
	changeCurrentValue();
}

void THierarchicalGammaPrior::_changeCurrentValue(){
	do{
		curValue = randomGenerator->getGammaRand(alpha->curValue, beta->curValue);
	}while(curValue<lowerLimit || curValue>upperLimit);
	if (isInt) makeCurValueInt();


	changed = true;
}
void THierarchicalGammaPrior::calculateTruncatedArea(){
	if(updateTruncatedArea){
		logAreaAfterTruncation = (randomGenerator->gammaCumulativeDistributionFunction(upperLimit, alpha->curValue, beta->curValue) - randomGenerator->gammaCumulativeDistributionFunction(lowerLimit, alpha->curValue, beta->curValue));
		updateTruncatedArea = false;
	}
}
double THierarchicalGammaPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 0.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;

	//calculate truncated area
	calculateTruncatedArea();
    return alpha->curValue * log(beta->curValue) - randomGenerator->gammaln(alpha->curValue) + (alpha->curValue - 1.0) * log(value) - beta->curValue*value - logAreaAfterTruncation;
}

//--------------------------------
//Gamma (alpha and mean)
THierarchicalGammaMeanPrior::THierarchicalGammaMeanPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior):THierarchicalPrior(RandomGenerator, Name, line){
	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize alpha
	bool isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[5]){
			isPrior = true;
			alpha = it->second;
			it->second->registerHypoPrior(this);
			if(alpha->lowerLimit <= 0.) throw "Problem initializing hierarchical gamma (mean) prior on '" + name + "': lower limit of hyper prior on alpha '" + alpha->name + "' <= 0.0!";
		}
	}
	if(!isPrior){
		alpha = new TFixedPrior(RandomGenerator, "alpha", stringToDoubleCheck(line[5]));
		fixedPriors.push_back(alpha);
		if(alpha->curValue <= 0.0) throw "Problem initializing hierarchical gamma (mean) prior on '" + name + "': alpha  <= 0.0!";
	}

	//initialize mean
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[6]){
			isPrior = true;
			mean = it->second;
			it->second->registerHypoPrior(this);
			if(mean->lowerLimit <= 0.) throw "Problem initializing hierarchical gamma (mean) prior on '" + name + "': lower limit of hyper prior on beta '" + mean->name + "' <= 0.0!";
		}
	}
	if(!isPrior){
		mean = new TFixedPrior(RandomGenerator, "beta", stringToDoubleCheck(line[6]));
		fixedPriors.push_back(mean);
		if(mean->curValue <= 0.0) throw "Problem initializing hierarchical gamma prior on '" + name + "': mean  <= 0.0!";
	}

	//get first value
	changeCurrentValue();
}

void THierarchicalGammaMeanPrior::_changeCurrentValue(){
	do{
		double beta = alpha->curValue / mean->curValue;
		curValue = randomGenerator->getGammaRand(alpha->curValue, beta);
	}while(curValue<lowerLimit || curValue>upperLimit);
	if (isInt) makeCurValueInt();


	changed = true;
}
void THierarchicalGammaMeanPrior::calculateTruncatedArea(){
	if(updateTruncatedArea){
		double beta = alpha->curValue / mean->curValue;
		logAreaAfterTruncation = (randomGenerator->gammaCumulativeDistributionFunction(upperLimit, alpha->curValue, beta) - randomGenerator->gammaCumulativeDistributionFunction(lowerLimit, alpha->curValue, beta));
		updateTruncatedArea = false;
	}
}
double THierarchicalGammaMeanPrior::getLogPriorDensityFromValue(const double& value){
	if(upperLimit==lowerLimit) return 0.0;
	if(value>upperLimit || value<lowerLimit) return log_zero_density;

	//calculate truncated area
	calculateTruncatedArea();
	double beta = alpha->curValue / mean->curValue;
    return alpha->curValue * log(beta) - randomGenerator->gammaln(alpha->curValue) + (alpha->curValue - 1.0) * log(value) - beta*value - logAreaAfterTruncation;
}

//----------------------------------
//Generalized pareto
THierarchicalGeneralizedParetoPrior::THierarchicalGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior):THierarchicalPrior(RandomGenerator, Name, line){
	if(line.size()!=9) throw "Wrong number of entries for hierarchical mirrored generalized pareto prior on '" + Name + "': need 10 not " +  toString(line.size()) + "!";

	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize location mu
	bool isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){

		if(it->first == line[5]){
			isPrior = true;
			locationMu = it->second;
			it->second->registerHypoPrior(this);
		}
	}
	if(!isPrior){
		locationMu = new TFixedPrior(RandomGenerator, "locationMu", stringToDoubleCheck(line[5]));
		fixedPriors.push_back(locationMu);
	}

	//initialize scale sigma
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[6]){
			isPrior = true;
			scaleSigma = it->second;
			it->second->registerHypoPrior(this);
			if(scaleSigma->lowerLimit <= 0.) throw "Problem initializing hierarchical mirrored pareto prior on '" + name + "': lower limit of hyper prior on sigma (scale) '" + scaleSigma->name + "' <= 0!";
		}
	}
	if(!isPrior){
		scaleSigma = new TFixedPrior(RandomGenerator, "scaleSigma", stringToDoubleCheck(line[6]));
		fixedPriors.push_back(scaleSigma);
		if(scaleSigma->curValue <= 0.) throw "Hierarchical mirrored pareto prior on '" + name + "' initialized with sigma (scale) <= 0!";
	}

	//initialize lower xi
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[7]){
			isPrior = true;
			shapeXi = it->second;
			it->second->registerHypoPrior(this);
		}
	}
	if(!isPrior){
		shapeXi = new TFixedPrior(RandomGenerator, "shapeXi", stringToDoubleCheck(line[7]));
		fixedPriors.push_back(shapeXi);
		if(shapeXi->curValue <= 0.) throw "Hierarchical mirrored pareto prior on '" + name + "' initialized with xi (shape) <= 0!";
	}

	//initialize others
	zero = 0.0;
	logAreaAfterTruncation = 0.0;
	probPositive = 0.0;

	//get first value
	changeCurrentValue();
}

void THierarchicalGeneralizedParetoPrior::_changeCurrentValue(){
   calculateTruncatedArea();
   do {
		  curValue = randomGenerator->getGeneralizedParetoRand(zero, scaleSigma->curValue, shapeXi->curValue) + locationMu->curValue;
   } while(curValue<lowerLimit || curValue > upperLimit);
}
void THierarchicalGeneralizedParetoPrior::calculateTruncatedArea(){
	if(updateTruncatedArea){
		double end = upperLimit;
		if(shapeXi->curValue < 0.0 && upperLimit > (locationMu->curValue - scaleSigma->curValue / shapeXi->curValue)) end = (locationMu->curValue - scaleSigma->curValue / shapeXi->curValue);
		double start = lowerLimit;
		if(lowerLimit < locationMu->curValue) start = locationMu->curValue;
		logAreaAfterTruncation = randomGenerator->GeneralizedParetoCumulativeFunction(end, locationMu->curValue, scaleSigma->curValue, shapeXi->curValue);
		logAreaAfterTruncation -= randomGenerator->GeneralizedParetoCumulativeFunction(start, locationMu->curValue, scaleSigma->curValue, shapeXi->curValue);
		logAreaAfterTruncation = log(logAreaAfterTruncation);
		updateTruncatedArea = false;
	}
}
double THierarchicalGeneralizedParetoPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;
   //also check other conditions of generalized pareto
   if(value < locationMu->curValue) return log_zero_density;
   if(shapeXi->curValue < 0.0 && value > (locationMu->curValue - scaleSigma->curValue / shapeXi->curValue)) return log_zero_density;

   double logSigma = log(scaleSigma->curValue);
   calculateTruncatedArea();

   if(shapeXi->curValue == 0.0){
	   return -logSigma - (value - locationMu->curValue) / scaleSigma->curValue - logAreaAfterTruncation;
	} else {
	   return -logSigma + (-1.0-1.0/shapeXi->curValue) * log(1.0 + shapeXi->curValue * (value - locationMu->curValue )/scaleSigma->curValue) - logAreaAfterTruncation;
	}
}

//----------------------------------
//Mirrored generalized pareto
THierarchicalMirroredGeneralizedParetoPrior::THierarchicalMirroredGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior):THierarchicalPrior(RandomGenerator, Name, line){
	if(line.size()!=11) throw "Wrong number of entries for hierarchical mirrored generalized pareto prior on '" + Name + "': need 11 not " +  toString(line.size()) + "!";

	//min and max are in 4th and 5th column
	setLimits(stringToDoubleCheck(line[3]), stringToDoubleCheck(line[4]));

	//initialize location mu
	bool isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){

		if(it->first == line[5]){
			isPrior = true;
			locationMu = it->second;
			it->second->registerHypoPrior(this);
		}
	}
	if(!isPrior){
		locationMu = new TFixedPrior(RandomGenerator, "locationMu", stringToDoubleCheck(line[5]));
		fixedPriors.push_back(locationMu);
	}

	//initialize scale sigma
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[6]){
			isPrior = true;
			scaleSigma = it->second;
			it->second->registerHypoPrior(this);
			if(scaleSigma->lowerLimit <= 0.) throw "Problem initializing hierarchical mirrored pareto prior on '" + name + "': lower limit of hyper prior on sigma (scale) '" + scaleSigma->name + "' <= 0!";
		}
	}
	if(!isPrior){
		scaleSigma = new TFixedPrior(RandomGenerator, "scaleSigma", stringToDoubleCheck(line[6]));
		fixedPriors.push_back(scaleSigma);
		if(scaleSigma->curValue <= 0.) throw "Hierarchical mirrored pareto prior on '" + name + "' initialized with sigma (scale) <= 0!";
	}

	//initialize lower xi
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[7]){
			isPrior = true;
			shapeXiLow = it->second;
			it->second->registerHypoPrior(this);
			//if(shapeXiLow->lowerLimit <= 0.) throw "Problem initializing hierarchical mirrored pareto prior on '" + name + "': lower limit of hyper prior on lower Xi (shape) '" + shapeXiLow->name + "' <= 0!";
		}
	}
	if(!isPrior){
		shapeXiLow = new TFixedPrior(RandomGenerator, "shapeXiLow", stringToDoubleCheck(line[7]));
		fixedPriors.push_back(shapeXiLow);
		//if(shapeXiLow->curValue <= 0.) throw "Hierarchical mirrored pareto prior on '" + name + "' initialized with xi low (shape) <= 0!";
	}

	//initialize higher xi
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[8]){
			isPrior = true;
			shapeXiHigh = it->second;
			it->second->registerHypoPrior(this);
			//if(shapeXiHigh->lowerLimit <= 0.) throw "Problem initializing hierarchical mirrored pareto prior on '" + name + "': lower limit of hyper prior on higher Xi (shape) '" + shapeXiHigh->name + "' <= 0!";
		}
	}
	if(!isPrior){
		shapeXiHigh = new TFixedPrior(RandomGenerator, "shapeXiHigh", stringToDoubleCheck(line[8]));
		fixedPriors.push_back(shapeXiHigh);
		//if(shapeXiHigh->curValue <= 0.) throw "Hierarchical mirrored pareto prior on '" + name + "' initialized with xi high (shape) <= 0!";
	}

	//prob above mu (mixing probability)
	isPrior = false;
	for(std::map<std::string,TSimplePrior*>::iterator it=mapSimplePrior.begin(); it!=mapSimplePrior.end(); ++it){
		if(it->first == line[9]){
			isPrior = true;
			probAboveMu = it->second;
			it->second->registerHypoPrior(this);
		}
	}
	if(!isPrior){
		probAboveMu = new TFixedPrior(RandomGenerator, "probAboveMu", stringToDoubleCheck(line[9]));
		fixedPriors.push_back(probAboveMu);
	}

	//initialize others
	zero = 0.0;
	logAreaAfterTruncationBelowMu = 0.0;
	logAreaAfterTruncationAboveMu = 0.0;
	probAboveMuInternal = 0.0;

	//get first value
	changeCurrentValue();
}

void THierarchicalMirroredGeneralizedParetoPrior::_changeCurrentValue(){
	calculateTruncatedArea();
   if(randomGenerator->getRand() < probAboveMuInternal){
	  //sample from positive (above mu) range
	  do {
		  curValue = randomGenerator->getGeneralizedParetoRand(zero, scaleSigma->curValue, shapeXiHigh->curValue) + locationMu->curValue;
	  } while(curValue<lowerLimit || curValue > upperLimit);
   } else {
	   //sample from negative (below mu) range
	   do {
		   curValue = - randomGenerator->getGeneralizedParetoRand(zero, scaleSigma->curValue, shapeXiLow->curValue) + locationMu->curValue;
	   } while(curValue<lowerLimit || curValue > upperLimit);
   }
}
void THierarchicalMirroredGeneralizedParetoPrior::calculateTruncatedArea(){
	if(updateTruncatedArea){
		double areaAboveMu, areaBelowMu;
		if(lowerLimit > locationMu->curValue || probAboveMu->curValue == 1.0){
			logAreaAfterTruncationBelowMu = -1.0; // not used!
			areaAboveMu = randomGenerator->GeneralizedParetoCumulativeFunction(upperLimit - locationMu->curValue, zero, scaleSigma->curValue, shapeXiHigh->curValue);
			areaAboveMu -= randomGenerator->GeneralizedParetoCumulativeFunction(lowerLimit - locationMu->curValue, zero, scaleSigma->curValue, shapeXiHigh->curValue);
			logAreaAfterTruncationAboveMu = log(areaAboveMu);
			probAboveMuInternal = 1.0;
		} else if(upperLimit < locationMu->curValue  || probAboveMu->curValue == 0.0){
			logAreaAfterTruncationAboveMu = -1.0; // not used!
			areaBelowMu = randomGenerator->GeneralizedParetoCumulativeFunction(locationMu->curValue - lowerLimit, zero, scaleSigma->curValue, shapeXiLow->curValue);
			areaBelowMu -= randomGenerator->GeneralizedParetoCumulativeFunction(locationMu->curValue - upperLimit, zero, scaleSigma->curValue, shapeXiLow->curValue);
			logAreaAfterTruncationBelowMu = log(areaBelowMu);
			probAboveMuInternal = 0.0;
		} else {
			logAreaAfterTruncationBelowMu = log(randomGenerator->GeneralizedParetoCumulativeFunction(locationMu->curValue - lowerLimit, zero, scaleSigma->curValue, shapeXiLow->curValue) / (1.0 - probAboveMu->curValue));
			logAreaAfterTruncationAboveMu = log(randomGenerator->GeneralizedParetoCumulativeFunction(upperLimit - locationMu->curValue, zero, scaleSigma->curValue, shapeXiHigh->curValue) / probAboveMu->curValue);
			probAboveMuInternal = probAboveMu->curValue;
		}
		updateTruncatedArea = false;
	}
}
double THierarchicalMirroredGeneralizedParetoPrior::getLogPriorDensityFromValue(const double& value){
   if(value>upperLimit || value<lowerLimit) return log_zero_density;
   if(probAboveMu->curValue == 1.0 && value < locationMu->curValue) return log_zero_density;
   if(probAboveMu->curValue == 0.0 && value > locationMu->curValue) return log_zero_density;
   if(shapeXiHigh->curValue < 0.0 && upperLimit - locationMu->curValue > - scaleSigma->curValue / shapeXiHigh->curValue) return log_zero_density;
   if(shapeXiLow->curValue < 0.0 && locationMu->curValue - lowerLimit > - scaleSigma->curValue / shapeXiLow->curValue) return log_zero_density;


   calculateTruncatedArea();
   double logSigma = log(scaleSigma->curValue);

   double tmp = value - locationMu->curValue;

   if(tmp < zero)
	   return -logSigma + (-1.0-1.0/shapeXiLow->curValue) * log(1.0 + shapeXiLow->curValue * (-tmp)/scaleSigma->curValue) - logAreaAfterTruncationBelowMu;
   else if(tmp > zero)
	   return -logSigma + (-1.0-1.0/shapeXiHigh->curValue) * log(1.0 + shapeXiHigh->curValue * (tmp)/scaleSigma->curValue) - logAreaAfterTruncationAboveMu;
   //in case value == mu, return average
   else return -logSigma + ((-1.0-1.0/shapeXiLow->curValue) * log(1.0 + shapeXiLow->curValue * (-tmp)/scaleSigma->curValue) - logAreaAfterTruncationBelowMu
		                  + (-1.0-1.0/shapeXiHigh->curValue) * log(1.0 + shapeXiHigh->curValue * (tmp)/scaleSigma->curValue) - logAreaAfterTruncationAboveMu) / 2.0;
}

//------------------------------------------------------------------------------
//TEquation
//------------------------------------------------------------------------------
TEquation_base::TEquation_base(std::string & s, TPriorVector* priorVec){
	originalEuqation=s;
	init(s, priorVec, &originalEuqation);
}
TEquation_base::TEquation_base(std::string equat, TPriorVector* priorVec, std::string* Original){
	init(equat, priorVec, Original);
}
void TEquation_base::init(std::string & equat, TPriorVector* priorVec, std::string* Original){
	original=Original;
	myString=equat;
	eraseAllWhiteSpaces(myString);
	equat_object=NULL;
	if(equat.empty()) throw "Missing equation!";
	if(!generateAppropriateObject(myString, &equat_object, priorVec)) throw "Problems reading equation '" + (*original) + "'!";
	isConstant = equat_object->isConstant;
}

bool TEquation_base::generateAppropriateObject(std::string equat, TEquation_base** equatObj, TPriorVector* priorVec){
	//variables
	std::string first, second;
	char sign;
	double tmp;
	//find first part of equation
	std::string::size_type pos=equat.find_first_of("+-()");
	if(pos!=std::string::npos && equat.substr(pos,1)==")") throw "Unbalanced parenthesis in equation'" + (*original) + "'!";

	//no such sign found? -> only contains point operations or no operations
	if(pos==std::string::npos){
		if(stringContainsAny(equat, "*/")){
			//no +, - or ( but still an equation
			first=extractBeforeAnyOf(equat, "*/");
			sign=equat.substr(0,1).c_str()[0];
			equat.erase(0,1);
			if(equat.empty() || first.empty()) throw "Problems reading equation '" + (*original) + "'!";
			(*equatObj)=new TEquation_operator(first, equat, sign, priorVec, original);
			return true;
		} else {
			//must be a parameter or a value
			if(priorVec->priorExists(equat)){
				TPrior* prior=priorVec->getPriorFromName(equat);
				(*equatObj)=new TEquation_prior(equat, prior, original);
				return true;
			} else {
				if((!stringContainsOnly(equat, "0123456789.E")) || (!stringContainsNumbers(equat)))
					throw "Problems reading equation '" + (*original) + "': the string '"+equat+"' is unknown!";
				tmp=atof(equat.c_str());
				if(tmp==HUGE_VAL || tmp==-HUGE_VAL) throw "Problems reading equation '" + (*original) + "': the value '"+equat+"' is out of range!";
				(*equatObj)=new TEquation_value(equat, tmp, original);
			}
			return true;
		}
	} else {
		//check if pos is a parenthesis
		while(pos<(equat.size()-1) && equat.substr(pos,1)!="+" && equat.substr(pos,1)!="-" && equat.substr(pos,1)!="*" && equat.substr(pos,1)!="/"){
			if(equat.substr(pos,1)=="("){
				//find closing parenthesis
				int numParenthesis=1;
				std::string::size_type oldPos=pos;
				while(numParenthesis>0){
					pos=equat.find_first_of("()", pos+1);
					if(pos==std::string::npos) throw "Unbalanced parenthesis in equation'" + (*original) + "'!";
					if(equat.substr(pos,1)=="(") ++numParenthesis;
					else --numParenthesis;
				}
				if(oldPos==0 && pos==(equat.size()-1)){
					//string is surrounded by brackets -> remove them!
					equat.erase(0,1);
					equat.erase(pos-1,1);
					return generateAppropriateObject(equat, equatObj, priorVec);
				} else {
					//search + or - operator after parenthesis
					oldPos=equat.find_first_of("+-()", pos+1);
					if(oldPos==std::string::npos || equat.substr(oldPos,1)=="("){
						//does it contain a * or / before or after the equation?
						//before
						oldPos=equat.find_first_of("*/()");
						if(oldPos==std::string::npos || equat.substr(oldPos,1)=="("){
							//after?
							pos=equat.find_first_of("*/()", pos+1);
							if(pos==std::string::npos  || equat.substr(pos,1)=="(" || equat.substr(pos,1)==")"){
								//throw "XXX Missing operator in '" + equat + "' in equation'" + (*original) + "'!";
							//else {
								//is probably a function. Get name before first bracket!
								pos=equat.find_first_of("(");
								if(pos==std::string::npos || pos==0)
									throw "Problems reading equation '" + (*original) + "'!";
								//find closing bracket
								oldPos=pos; int numOpen=1;
								while(numOpen>0){
									oldPos=equat.find_first_of(")(", oldPos+1);
									if(oldPos==std::string::npos) throw "Unbalanced parenthesis in equation'" + (*original) + "'!";
									if(equat.at(oldPos)=='(') ++numOpen;
									else --numOpen;
								}

								//check if there is something AFTER the function
								if(oldPos!=(equat.size()-1))
									throw "Problems reading equation '" + (*original) + "', unexpected characters after closing bracket!";
								//TODO: add function with two arguments! -> simply search for comma!
								(*equatObj)=new TEquation_function(equat.substr(0, pos), equat.substr(pos+1, oldPos-pos-1), priorVec, original);
								return true;
							}
						} else {
							if(equat.substr(oldPos,1)==")") throw "Unbalanced parenthesis in equation'" + (*original) + "'!";
							pos=oldPos;
						}
					} else {
						if(equat.substr(oldPos,1)==")") throw "Unbalanced parenthesis in equation'" + (*original) + "'!";
						pos=oldPos;
					}
				}
			}
		}

		//split and record sign
		first=equat.substr(0, pos);
		sign=equat.substr(pos,1).c_str()[0];
		second=equat.substr(pos+1);

		if(second.empty()) throw "Problems reading equation '" + (*original) + "'!";
		if(first.empty()){
			if(sign=='+' || sign=='-') first="0";
			else throw "Problems reading equation '" + (*original) + "'!";
		}
		TEquation_base* tmpObj=new TEquation_operator(first, second, sign, priorVec, original);
		//(*equatObj)=new TEquation_operator(first, second, sign, priorVec, original);
		if(tmpObj->isConstant){
			(*equatObj)=new TEquation_value(equat, tmpObj->getValue(), original);
			delete tmpObj;
		} else {
			(*equatObj)=tmpObj;
		}
		return true;
	}
	return false;
}

TEquation_value::TEquation_value(std::string s, double Val, std::string* Original){
	myString=s;
	val=Val;
	original=Original;
	isConstant=true;
}
TEquation_prior::TEquation_prior(std::string s, TPrior* Prior, std::string* Original){
	myString=s;
	prior=Prior;
	original=Original;
	isConstant = prior->isConstant;
};

TEquation_operator::TEquation_operator(std::string firstString, std::string secondString, char Sign, TPriorVector* priorVec, std::string* Original){
	myString=firstString+Sign+secondString;
	sign=Sign;
	original=Original;
	first=NULL;
	second=NULL;
	if(sign!='+' && sign!='-' && sign!='*' && sign!='/')
		throw (std::string) "Unknown operator '"+ sign + "' in equation '" + (*original) +"'!";

	if(!generateAppropriateObject(firstString, &first, priorVec)) throw "Problems reading equation '" + (*original) + "'!";
	if(!generateAppropriateObject(secondString, &second, priorVec)) throw "Problems reading equation '" + (*original) + "'!";
	isConstant=first->isConstant && second->isConstant;
};

double TEquation_operator::getValue(){
	double tmp;
	switch(sign){
		case '*': tmp=first->getValue() * second->getValue(); break;
		case '/': tmp=second->getValue();
			  	  if(tmp==0.0){
			  		  throw "Problem evaluating equation: division by zero in '" + myString +"'!";
			  	  }
			  	  tmp=first->getValue() / tmp;
			  	  break;
		case '+': tmp=first->getValue() + second->getValue(); break;
		case '-': tmp=first->getValue() - second->getValue(); break;
	}
	return tmp;
};
TEquation_function::TEquation_function(std::string function, std::string Equat, TPriorVector* priorVec, std::string* Original){
	myString=function+"("+Equat+")";
	original=Original;
	//find appropriate function
	trimString(function);

	//log and sqrt
	if(function=="exp") pt2Function=&TEquation_function::func_exp;
	else if(function=="pow10") pt2Function=&TEquation_function::func_pow10;
	else if(function=="pow2") pt2Function=&TEquation_function::func_pow2;
	else if(function=="log") pt2Function=&TEquation_function::func_log;
	else if(function=="log10") pt2Function=&TEquation_function::func_log10;
	else if(function=="log2") pt2Function=&TEquation_function::func_log2;
	else if(function=="sqrt") pt2Function=&TEquation_function::func_sqrt;

	//rounding
	else if(function=="round") pt2Function=&TEquation_function::func_round;
	else if(function=="abs") pt2Function=&TEquation_function::func_abs;
	else if(function=="floor") pt2Function=&TEquation_function::func_floor;
	else if(function=="ceil") pt2Function=&TEquation_function::func_ceil;
	else throw "Unknown function '"+function+"' in equation '" + (*original) +"'!";

	//generate equation object of function content
	if(!generateAppropriateObject(Equat, &equat_object, priorVec)) throw "Problems reading equation '" + (*original) + "'!";
	isConstant=equat_object->isConstant;
}

double TEquation_function::func_exp(double val){
	return exp(val);
}
double TEquation_function::func_pow10(double val){
	return pow(10.0, val);
}
double TEquation_function::func_pow2(double val){
	return pow(2.0, val);
}
double TEquation_function::func_log(double val){
	if(val<=0) throw "Problems evaluating equation '" + (*original) + "', can not take logarithm of '" + toString(val) + "'!";
	return log(val);
}
double TEquation_function::func_log10(double val){
	if(val<=0) throw "Problems evaluating equation '" + (*original) + "', can not take logarithm of '" + toString(val) + "'!";
	return log10(val);
}
double TEquation_function::func_log2(double val){
	if(val<=0) throw "Problems evaluating equation '" + (*original) + "', can not take logarithm of '" + toString(val) + "'!";
	return log2(val);
}

double TEquation_function::func_sqrt(double val){
	if(val<0) throw "Problems evaluating equation '" + (*original) + "', can not take square root of '" + toString(val) + "'!";
	return log(val);
}
double TEquation_function::func_round(double val){
	return round(val);
}
double TEquation_function::func_abs(double val){
	return fabs(val);
}
double TEquation_function::func_ceil(double val){
	return ceil(val);
}
double TEquation_function::func_floor(double val){
	return floor(val);
}
//------------------------------------------------------------------------------
//TRule
//------------------------------------------------------------------------------
TRule::TRule(std::string firstString, char Sign, std::string secondString, TPriorVector* priorVec){
	sign=Sign;
	if(sign!='<' && sign !='>') throw "Cannot initialize rule '" + firstString + " " + Sign + " " + secondString + "': unknown sign '"+Sign+"'!";
	first=new TEquation_base(firstString, priorVec);
	second=new TEquation_base(secondString, priorVec);
}
bool TRule::passed(){
	if(sign=='>'){
		if(first->getValue() > second->getValue()) return true;
		else return false;
	} else {
		if(first->getValue() < second->getValue()) return true;
		else return false;
	}
}

//------------------------------------------------------------------------------
//TPriorVector
//------------------------------------------------------------------------------
TPriorVector::TPriorVector(std::string fileName, TLog* gotLogFile, TRandomGenerator* RandomGenerator){
	randomGenerator=RandomGenerator;
	logFile=gotLogFile;
   numPrior=0;
   numSimplePrior=0;
   numCombinedPrior=0;
   mapSimplePrior.empty();
   mapCombinedPrior.empty();
   readPriorsAndRules(fileName);
   //initialize values
   getNewValues();
   saveOldValues();
}
//---------------------------------------------------------------------------
/** read the *est file and fill the array with the priors */
void TPriorVector::readPriorsAndRules(std::string fileName){
	logFile->listFlush("Reading priors and rules from '" + fileName + "' ...");
	// open file
	std::ifstream is (fileName.c_str()); // opening the file for reading
	if(!is) throw "The .est file '" + fileName + "' could not be opened!";

	std::vector<std::string>::iterator curName, endName;
	// Reading the file
	std::string myType, my_name, my_equation,my_sign;
	bool hasParamAsArg;
	std::vector<std::string> my_nameVec;
	std::vector<std::string> my_equationVec;
	std::string::size_type pos, endPos;
	int curSection=0; // 0: none, 1: Priors, 2:Rules

	// read line by line
	std::string line;
	std::vector<std::string> vecLine;
	std::vector<std::string>::iterator stringVecIt;
	int lineNum=0;
	while(is.good() && !is.eof()){
	  ++lineNum;
	  //------------------------
	  // go to the prior section
	  //------------------------
	  getline(is, line);
	  line=extractBeforeDoubleSlash(line);   // remove the commentated part
	  trimString(line);
	  if(!line.empty()){
		 if(line.find_first_of('[')==0 && line.find_first_of(']')==(line.size()-1)){
			 if(stringContains(line, "[PARAMETERS]")) { curSection=1; continue;}
			 else if(stringContains(line, "[RULES]")){
				 if(curSection==0) throw "Section '[PARAMETERS]]' is missing!";
				 curSection=2;
				 continue;
			 }
			 else if(stringContains(line, "[COMPLEX PARAMETERS]")){
				 if(curSection==0) throw "Section '[PARAMETERS]' is missing!";
				 curSection=3;
				 continue;
			 } else throw "Unknown section '"+line+"'!";
		}
		//------------------------
		//switch by section
		//------------------------
		switch (curSection){
		   case 1: //------------------------
			   	   //read the priors
			   	   //------------------------
			   	   fillVectorFromStringWhiteSpaceSkipEmpty(line, vecLine);
			   	   //if(vecLine.size()!=5 && vecLine.size()!=6 && vecLine.size()!=8) throw "Unexpected number of entries in the .est file '"+fileName+"' on line " + toString(lineNum) + "!";

			   	   //read name -> could indicate a vector of similar parameters
			   	   if(vecLine[1].empty()) throw "No name given on line " + toString(lineNum) + "!";
			   	   my_nameVec.clear();
			   	   addExpandIndex(vecLine[1], my_nameVec);

			   	   //read type
			   	   myType=vecLine[2];

			   	   //check if some of the parameters are names of existing priors
			   	   hasParamAsArg = false;
			   	   for(unsigned int p = 3; p < (vecLine.size()-1); ++p){
			   		   for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior){
			   			   if(curSimplePrior->first == vecLine[p]){
			   				   if(p < 5) throw "Using hyper pramaters not implemented for min and max!";
			   				   hasParamAsArg=true;
			   				   //only some types accept that
			   				   if(myType != "norm" && myType != "gamma" && myType != "mPareto" && myType != "gPareto" && myType != "gammaMean") throw "Using hyper parameters not implemented for type '" + myType + "'!";
			   			   }
			   		   }
			   	   }

			   	   //create objects
			   	   for(stringVecIt=my_nameVec.begin(); stringVecIt!=my_nameVec.end(); ++stringVecIt){
			   		   //check if name is already in use
			   		   for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior){
			   			   if(*stringVecIt == curSimplePrior->first) throw "Error on line " + toString(lineNum) + ": there already exists a parameter with name '" + *stringVecIt + "'!";
			   		   }
			   		   //switch by type
					   if(myType=="fixed") simplePriors.push_back(new TFixedPrior(randomGenerator, *stringVecIt, vecLine));
					   else if(myType=="unif") simplePriors.push_back(new TUniformPrior(randomGenerator, *stringVecIt, vecLine));
					   else if(myType=="logUnif") simplePriors.push_back(new TLogUniformPrior(randomGenerator, *stringVecIt, vecLine));
					   else if(myType=="norm"){
						   if(hasParamAsArg) simplePriors.push_back(new THierarchicalNormalPrior(randomGenerator, *stringVecIt, vecLine, mapSimplePrior));
						   else simplePriors.push_back(new TNormalPrior(randomGenerator, *stringVecIt, vecLine));
					   }
					   else if(myType=="logNorm") simplePriors.push_back(new TLogNormalPrior(randomGenerator, *stringVecIt, vecLine));
					   else if(myType=="gamma"){
						   if(hasParamAsArg) simplePriors.push_back(new THierarchicalGammaPrior(randomGenerator, *stringVecIt, vecLine, mapSimplePrior));
						   else simplePriors.push_back(new TGammaPrior(randomGenerator, *stringVecIt, vecLine));
					   }
					   else if(myType=="gammaMean"){
						   if(hasParamAsArg){
							   simplePriors.push_back(new THierarchicalGammaMeanPrior(randomGenerator, *stringVecIt, vecLine, mapSimplePrior));
						   } else
							   simplePriors.push_back(new TGammaMeanPrior(randomGenerator, *stringVecIt, vecLine));
					   }
					   else if(myType=="exp") simplePriors.push_back(new TExponentialPrior(randomGenerator, *stringVecIt, vecLine));
					   else if(myType=="gPareto"){
						   if(hasParamAsArg) simplePriors.push_back(new THierarchicalGeneralizedParetoPrior(randomGenerator, *stringVecIt, vecLine, mapSimplePrior));
						   else simplePriors.push_back(new TGeneralizedParetoPrior(randomGenerator, *stringVecIt, vecLine));
					   }
					   else if(myType=="mPareto"){
						   if(hasParamAsArg) simplePriors.push_back(new THierarchicalMirroredGeneralizedParetoPrior(randomGenerator, *stringVecIt, vecLine, mapSimplePrior));
						   else simplePriors.push_back(new TMirroredGeneralizedParetoPrior(randomGenerator, *stringVecIt, vecLine));
					   }
					   else throw "Unknown prior type '" + myType +"' on line " + toString(lineNum) + "!";

					   //save pointer in map
					   mapSimplePrior[*stringVecIt] = *simplePriors.rbegin();
			   	   }
			   	   break;
		   case	2: //------------------------
			   	   // read the rules
			   	   //------------------------
			   	   pos=line.find_first_of("<>");
			   	   if(pos==std::string::npos) throw "Rule '" + line + "' on line " + toString(lineNum) + " has no sign (either < or >)!";
			   	   rules.push_back(new TRule(line.substr(0,pos), line.substr(pos,1).c_str()[0], line.substr(pos+1), this));
			   	   break;
		   case 3: //------------------------
			   	   // read the complex parameters
			   	   //------------------------
			   	   fillVectorFromStringWhiteSpaceSkipEmpty(line, vecLine);
			   	   if(vecLine.size()<4) throw "Too few entries on line " + toString(lineNum) + "!";
			   	   //check if it is a vector of similar parameters
			   	   if(vecLine[1].empty()) throw "No name given on line " + toString(lineNum) + "!";
			   	   my_nameVec.clear();
			   	   addExpandIndex(vecLine[1], my_nameVec);
			   	   //equation
			   	   trimString(vecLine[2]);
			   	   if(vecLine[2]!="=") throw "Unexcpeted sign (instead of '=') on line " + toString(lineNum) + "!";
			   	   std::string output = vecLine[vecLine.size()-1];
				   vecLine.erase(vecLine.end()-1);
			   	   //get equation
			   	   concatenateString(vecLine, my_equation, 3);
			   	   vecLine.push_back(output);

			   	   // if the values are ok put save the set as combined paramter -> replace
		 	 	   std::string eq;
			   	   int i=1;
			   	   for(stringVecIt=my_nameVec.begin(); stringVecIt!=my_nameVec.end(); ++stringVecIt, ++i){
			   		   //check if name is already in use
			   		   for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior){
			   			   if(*stringVecIt == curSimplePrior->first) throw "Error on line " + toString(lineNum) + ": there already exists a parameter with name '" + *stringVecIt + "'!";
			   		   }
			   		   for(curCombinedPrior=mapCombinedPrior.begin(); curCombinedPrior!=mapCombinedPrior.end(); ++curCombinedPrior){
			   			   if(*stringVecIt == curCombinedPrior->first) throw "Error on line " + toString(lineNum) + ": there already exists a parameter with name '" + *stringVecIt + "'!";
			   		   }
			   		   //create object
			   		   eq = stringReplace("["+ toString(my_nameVec.size()) +"]", toString(i), my_equation);
			   		   if(eq.find_last_of('[') != std::string::npos) throw "Problem expanding indexes in equation '" + eq + "': only '["+ toString(my_nameVec.size()) +"]' is a valid tag!";
			   		   combinedPriors.push_back(new TCombinedPrior(randomGenerator, *stringVecIt, eq, vecLine, this));
			   	       mapCombinedPrior[*stringVecIt] = *combinedPriors.rbegin();
			   	   }
			   	   break;
			}
	  }
	}

	//create iterators
	curSimplePrior = mapSimplePrior.begin();
	curCombinedPrior = mapCombinedPrior.begin();
	numSimplePrior = simplePriors.size();
	numCombinedPrior = combinedPriors.size();
	nextMcmcUpdatePrior = simplePriors.end();

	//save list of simple priors
	simplePriorNamesConcatenated = simplePriors[0]->name;
	for(unsigned int i=1; i<numSimplePrior;++i) simplePriorNamesConcatenated+="," + simplePriors[i]->name;

	//write to log file
	logFile->write(" done!");
	logFile->conclude(mapSimplePrior.size(), " parameters");
	logFile->conclude(mapCombinedPrior.size(), " complex parameters");
	logFile->conclude(rules.size(), " rules");

}
//------------------------------------------------------------------------------
void TPriorVector::writeHeader(std::ofstream& ofs){
   writeHeaderSimplePriors(ofs);
   writeHeaderCombinedPriors(ofs);
}
void TPriorVector::writeHeaderSimplePriors(std::ofstream& ofs){
	for(vecSimplePriorIt=simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt){
		if((*vecSimplePriorIt)->output) ofs << "\t" << (*vecSimplePriorIt)->name;
	}
}
void TPriorVector::writeHeaderCombinedPriors(std::ofstream& ofs){
	for(vecCombinedPriorIt=combinedPriors.begin(); vecCombinedPriorIt!=combinedPriors.end(); ++vecCombinedPriorIt){
		if((*vecCombinedPriorIt)->output) ofs << "\t" << (*vecCombinedPriorIt)->name;
	}
}
//------------------------------------------------------------------------------
void TPriorVector::writeParameters(std::ofstream& ofs){
   writeParametersSimplePriors(ofs);
   writeParametersCombinedPriors(ofs);
}
void TPriorVector::writeParametersSimplePriors(std::ofstream& ofs){
	for(vecSimplePriorIt=simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt){
		if((*vecSimplePriorIt)->output){
			ofs << "\t";
			(*vecSimplePriorIt)->writeCurValue(ofs);
		}
	}
}
void TPriorVector::writeParametersCombinedPriors(std::ofstream& ofs){
	for(vecCombinedPriorIt=combinedPriors.begin(); vecCombinedPriorIt!=combinedPriors.end(); ++vecCombinedPriorIt){
		if((*vecCombinedPriorIt)->output){
			ofs << "\t";
			(*vecCombinedPriorIt)->writeCurValue(ofs);
		}
	}
}
//------------------------------------------------------------------------------
std::string TPriorVector::getSimplePriorName(const int & num){
	return simplePriors[num]->name;
}
TPrior* TPriorVector::getPriorFromName(const std::string& name){
	curSimplePrior = mapSimplePrior.find(name);
	if(curSimplePrior != mapSimplePrior.end()) return curSimplePrior->second;
	curCombinedPrior = mapCombinedPrior.find(name);
	if(curCombinedPrior != mapCombinedPrior.end()) return curCombinedPrior->second;
    throw "Model parameter '" + name + "' does not exist!";
}
TSimplePrior* TPriorVector::getSimplePriorFromName(const std::string& name){
	curSimplePrior = mapSimplePrior.find(name);
	if(curSimplePrior != mapSimplePrior.end()) return curSimplePrior->second;
	throw "Simple model parameter '" + name + "' does not exist!";
}
TCombinedPrior* TPriorVector::getCombinedPriorFromName(const std::string& name){
	curCombinedPrior = mapCombinedPrior.find(name);
	if(curCombinedPrior != mapCombinedPrior.end()) return curCombinedPrior->second;
    throw "Complex model parameter '" + name + "' does not exist!";
}
int TPriorVector::getNumberOfSimplePriorFromName(const std::string& name){
   for(unsigned int i=0; i<numSimplePrior;++i) if(simplePriors[i]->name==name) return i;
   return -1;
}
//------------------------------------------------------------------------------
//functions to search for priors
bool TPriorVector::isHyperPriorWithTag(const std::string& name){
	char key=name[0];
	switch (key){
	case '%': case '&': case '$': case '!': case '#': case '@':
		int pos = name.find_last_of(key);
		std::string param = name.substr(pos+1);
		if(priorExists(param)) return true;
		else return false;
		break;
	}
	return false;
}

bool TPriorVector::isPriorTag(const std::string& name){
	if(isHyperPriorWithTag(name)) return true;
	return priorExists(name);
}

bool TPriorVector::simplePriorExists(const std::string& name){
	if(mapSimplePrior.find(name)==mapSimplePrior.end()) return false;
	return true;
}
bool TPriorVector::combinedPriorExists(const std::string& name){
	if(mapCombinedPrior.find(name)==mapCombinedPrior.end()) return false;
	return true;
}
bool TPriorVector::priorExists(const std::string& name){
	if(simplePriorExists(name) || combinedPriorExists(name)) return true;
	return false;
}
bool TPriorVector::isHyperPrior(const int& priorNumber){
	return simplePriors[priorNumber]->isHyperPrior;
}
bool TPriorVector::simplePriorIsConstant(const int& priorNumber){
	return simplePriors[priorNumber]->isConstant;
}
bool TPriorVector::priorIsConstant(const std::string& name){
	curSimplePrior = mapSimplePrior.find(name);
	if(curSimplePrior != mapSimplePrior.end()) return curSimplePrior->second->isConstant;
	curCombinedPrior = mapCombinedPrior.find(name);
	if(curCombinedPrior != mapCombinedPrior.end()) return curCombinedPrior->second->isConstant;
	throw "Prior '" + name + "' does not exist!;";
}
//------------------------------------------------------------------------------
/*
bool TPriorVector::writeCurValueWithHyperprior(const std::string& name, std::ofstream& file){
   //check for hyperprior
	char key=name[0];
	std::string param=name;
	switch (key){
			case '%':{ // gamma_deviation
				int pos2 = param.find_first_of('%',1);
				std::string arg = param.substr(1, pos2-1);
				double value=getValue(arg);   // check if it is a prior name
				if(value==_nan) value=atof(arg.c_str());
				curPrior=getPriorFromName(param.substr(pos2+1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorGamma(value, file);
				return true;
                }
			case '&': // beta_deviation
				curPrior=getPriorFromName(param.substr(1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorBeta(file);
				return true;
			case '$': {// normal
				int pos2 = param.find_first_of('$',1);
				std::string arg = param.substr(1, pos2-1);
				double value=getValue(arg);   // check if it is a prior name
				if(value==_nan) value=atof(arg.c_str());
				curPrior=getPriorFromName(param.substr(pos2+1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorNormal(value, file);
				return true;}
			case '!': {// normal truncated at 0 --> always positive!
				int pos2 = param.find_first_of('!',1);
				std::string arg = param.substr(1, pos2-1);
				double value=getValue(arg);   // check if it is a prior name
				if(value==_nan) value=atof(arg.c_str());
				curPrior=getPriorFromName(param.substr(pos2+1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorNormalPositive(value, file);
				return true;}
			case '#': {//lognormal
				int pos2 = param.find_first_of('#',1);
				std::string arg = param.substr(1, pos2-1);
				double value=getValue(arg);   // check if it is a prior name
				if(value==_nan) value=atof(arg.c_str());
				curPrior=getPriorFromName(param.substr(pos2+1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorLognormal(value, file);
				return true;}
			case '@': { //lognorm base 10, but values given in logscale!
				int pos2 = param.find_first_of('@',1);
				std::string arg = param.substr(1, pos2-1);
				double value=getValue(arg);   // check if it is a prior name
				if(value==_nan) value=atof(arg.c_str());
				curPrior=getPriorFromName(param.substr(pos2+1));
				if(!curPrior) return false;
				curPrior->writeHyperPriorLognormalParametersInLog10Scale(value, file);
				return true;}
	}
	return false;
}
*/
//------------------------------------------------------------------------------
bool TPriorVector::writeCurValueToFileFromName(const std::string& name, std::ofstream& file){
   //if(writeCurValueWithHyperprior(name, file)) return true;
   curPrior=getPriorFromName(name);
   if(!curPrior) return false;
   curPrior->writeCurValue(file);
   return true;
}
//------------------------------------------------------------------------------
void TPriorVector::getNewValues(){
	bool checkRules=false;
	while(!checkRules){
		// change all simple prior values
		for(vecSimplePriorIt=simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt)
		   (*vecSimplePriorIt)->changeCurrentValue();
		//check the rules if the rules do not fit change ALL parameters until the rules fit
		checkRules=true;
		for(std::vector<TRule*>::iterator curRule = rules.begin(); curRule!=rules.end(); ++curRule){
			if(!(*curRule)->passed()) checkRules=false;
		}
	}
   // calc all combined parameters
   updateCombinedParameters();
}
//------------------------------------------------------------------------------
//functions to update parameters via MCMC
void TPriorVector::getNewValuesMcmc(){
	//first get new values
	bool rulesok=false;
	while(!rulesok){
		for(vecSimplePriorIt=simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt){
			double randVal = randomGenerator->getRand(0.0, (*vecSimplePriorIt)->mcmcStep);
			double s = (*vecSimplePriorIt)->mcmcStep/2.0;
			(*vecSimplePriorIt)->curValue += randVal - s;
			//reflect, if necessary
			if((*vecSimplePriorIt)->curValue < (*vecSimplePriorIt)->lowerLimit) (*vecSimplePriorIt)->curValue = (*vecSimplePriorIt)->lowerLimit+((*vecSimplePriorIt)->lowerLimit - (*vecSimplePriorIt)->curValue);
			if((*vecSimplePriorIt)->curValue > (*vecSimplePriorIt)->upperLimit) (*vecSimplePriorIt)->curValue = (*vecSimplePriorIt)->upperLimit+((*vecSimplePriorIt)->upperLimit - (*vecSimplePriorIt)->curValue);
			(*vecSimplePriorIt)->changed = true;
		}
		//check the rules if the rules do not fit change ALL parameters until the rules fit
		rulesok=true;
		for(std::vector<TRule*>::iterator curRule = rules.begin(); curRule!=rules.end(); ++curRule){
			if(!(*curRule)->passed()) rulesok=false;
		}
	}
	//calc all combined parameters
	updateCombinedParameters();
}
void TPriorVector::getNewValuesMcmcUpdateOnePriorOnly(){
	//select prior: next in row
	if(nextMcmcUpdatePrior == simplePriors.end()) nextMcmcUpdatePrior = simplePriors.begin();
    getNewValuesMcmc(*nextMcmcUpdatePrior);
    // calc all combined parameters
	updateCombinedParameters();
	++nextMcmcUpdatePrior;
}
void TPriorVector::getNewValuesMcmc(const int& priorNumber){
	  getNewValuesMcmc(simplePriors[priorNumber]);
	  // calc all combined parameters
	  updateCombinedParameters();
}
void TPriorVector::getNewValuesMcmc(TSimplePrior* thisSimplePrior){
   bool rulesok=false;
   while(!thisSimplePrior->isConstant && !rulesok){
	   thisSimplePrior->getNewValuesMcmc();
	   //check the rules if the rules do not fit change ALL parameters until the rules fit
	   rulesok=true;
	   for(std::vector<TRule*>::iterator curRule = rules.begin(); curRule!=rules.end(); ++curRule){
   			if(!(*curRule)->passed()) rulesok=false;
	   }
   }
}
//------------------------------------------------------------------------------
//functions to update parameters via PMC
bool TPriorVector::getNewValuesPMC(double* newParams){
	//save new params into prior objects...
	int i=0;
	for(vecSimplePriorIt=simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt, ++i)
		(*vecSimplePriorIt)->setCurValue(newParams[i]);

	for(std::vector<TRule*>::iterator curRule = rules.begin(); curRule!=rules.end(); ++curRule){
		if(!(*curRule)->passed()) return false;
	}

   // calc all combined parameters
   updateCombinedParameters();
   return true;
}

//------------------------------------------------------------------------------
void TPriorVector::resetOldValues(){
	for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior)
	   curSimplePrior->second->resetOldValue();
	for(curCombinedPrior=mapCombinedPrior.begin(); curCombinedPrior!=mapCombinedPrior.end(); ++curCombinedPrior)
	   curCombinedPrior->second->resetOldValue();
};
//------------------------------------------------------------------------------
void TPriorVector::saveOldValues(){
   for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior)
	   curSimplePrior->second->saveOldValue();
   for(curCombinedPrior=mapCombinedPrior.begin(); curCombinedPrior!=mapCombinedPrior.end(); ++curCombinedPrior)
	   curCombinedPrior->second->saveOldValue();
};
//------------------------------------------------------------------------------
void TPriorVector::updateCombinedParameters(){
	//same order as in the est file!!!
	for(unsigned int i=0; i<numCombinedPrior; ++i)
		combinedPriors[i]->update();
};
//------------------------------------------------------------------------------
double TPriorVector::getLogPriorDensity(){

   double density = 0.0;
   for(vecSimplePriorIt = simplePriors.begin(); vecSimplePriorIt!=simplePriors.end(); ++vecSimplePriorIt)
	  density += (*vecSimplePriorIt)->getLogPriorDensity();
   return density;
};
double TPriorVector::getLogPriorDensity(double* values){
   double density = simplePriors[0]->getLogPriorDensityFromValue(values[0]);
   for(unsigned int i=1; i<numSimplePrior; ++i){
	  density += simplePriors[i]->getLogPriorDensityFromValue(values[i]);
   }
   return density;
};
//------------------------------------------------------------------------------
double TPriorVector::getValue(const std::string& name){
   curPrior=getPriorFromName(name);
   if(!curPrior) return _nan;
   return curPrior->curValue;
}
//------------------------------------------------------------------------------
void TPriorVector::setSimplePriorMCMCStep(const int& priorNumber, const double& prop){
   simplePriors[priorNumber]->setMCMCStep(prop*(simplePriors[priorNumber]->upperLimit-simplePriors[priorNumber]->lowerLimit));
}
void TPriorVector::setSimplePriorValue(const int& priorNumber, const double& val){
	simplePriors[priorNumber]->setCurValue(val);
}
void TPriorVector::setSimplePriorValue(const std::string& name, const double& val){
	curPrior=getPriorFromName(name);
	curPrior->setCurValue(val);
}
//starting values
void TPriorVector::chooseStartingValueFromPrior(const int& priorNumber){
	simplePriors[priorNumber]->changeCurrentValue();
}
void TPriorVector::chooseStartingValueHyperPrior(const int& priorNumber){
	//choose a value from prior
	chooseStartingValueFromPrior(priorNumber);

	//now use climbing Algo to find a better start value, conditioning on the values of the hypopriors
	if(simplePriors[priorNumber]->isHyperPrior){
		double oldLogDens = getLogPriorDensity();
		double newLogDens;
		for(int i=0; i<100; ++i){
			simplePriors[priorNumber]->saveOldValue();
			getNewValuesMcmc(simplePriors[priorNumber]);
			newLogDens = getLogPriorDensity();
			if(newLogDens > oldLogDens) oldLogDens = newLogDens;
			else simplePriors[priorNumber]->resetOldValue();
		}
	}
}



