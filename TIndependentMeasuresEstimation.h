//---------------------------------------------------------------------------

#ifndef TIndependentMeasuresEstimationH
#define TIndependentMeasuresEstimationH
#include "TBaseEstimation.h"

//---------------------------------------------------------------------------
class TIndependentMeasuresEstimation:public TBaseEstimation {
   public:
	  bool v_jPrecomputed;
	  ColumnVector v_jPreComputedPart;
	  double* marDensPValueVec; //used to store the p-value of each individual data set
	  double* marDensVec; //used to store the marginal densities of each individual data set
	  bool marDensPValueVecFilled;
	  bool marDensVecFilled;

	  TIndependentMeasuresEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile);
	  ~TIndependentMeasuresEstimation(){
		  if(marDensPValueVecFilled) delete[] marDensPValueVec;
		  if(marDensVecFilled) delete[] marDensVec;
	  }
	  bool performPosteriorEstimation(bool writeRetainedSimulations, int numSimsForObsPValues);
	  bool calculate_T_j(SymmetricMatrix & m);
	  void makeRejection(bool writeRetainedSimulations);
	  ColumnVector calculate_v_j(ColumnVector theta_j);
	  void calculateMarginalDensity(int numSimsForObsPValue);
};


#endif
